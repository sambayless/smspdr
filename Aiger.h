/*
 * Aiger.cpp
 *
 *  Created on: 2012-10-18
 *      Author: sam
 */

//Utility functions for using AIG with Minisat
#ifndef AIGER_CPP_H
#define AIGER_CPP_H
extern "C" {
#include "aiger/aiger.h"
}
#include "core/Solver.h"
#include "core/SolverTypes.h"
#include "mtl/Vec.h"
Minisat::Lit unroll(Minisat::Solver& target,aiger * mgr, int iterations, Minisat::vec<Minisat::Var> & in_latches, Minisat::vec<Minisat::Var> & out_latches);

Minisat::Lit unroll(Minisat::Solver& target,aiger * mgr, Minisat::vec<Minisat::Var> & in_latches, Minisat::vec<Minisat::Var> & out_latches);
bool isReset(Minisat::vec<Minisat::Lit> & assignment);
void prepare(Minisat::Solver& target,aiger * mgr, Minisat::vec<Minisat::Var> & latches);
int zero(Minisat::Solver& target,aiger * mgr, Minisat::vec<Minisat::Var> & latches);

Minisat::Lit getLit(Minisat::Solver & S,const int startVar, aiger * mgr, unsigned lit, Minisat::vec<Minisat::Var> & in_latches);

#endif
