################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Aiger.cpp \
../Cone.cpp \
../Config.cpp \
../Generalize.cpp \
../ITPDebug.cpp \
../NaiveSolver.cpp \
../Ternary.cpp 

CC_SRCS += \
../Main.cc 

OBJS += \
./Aiger.o \
./Cone.o \
./Config.o \
./Generalize.o \
./ITPDebug.o \
./Main.o \
./NaiveSolver.o \
./Ternary.o 

CC_DEPS += \
./Main.d 

CPP_DEPS += \
./Aiger.d \
./Cone.d \
./Config.d \
./Generalize.d \
./ITPDebug.d \
./NaiveSolver.d \
./Ternary.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -D__STDC_LIMIT_MACROS -D__STDC_FORMAT_MACROS -DNDEBUG -I.././ -O3 -Wall -c -fmessage-length=0  -static  -static-libgcc   -static-libstdc++ -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

%.o: ../%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -D__STDC_LIMIT_MACROS -D__STDC_FORMAT_MACROS -DNDEBUG -I.././ -O3 -Wall -c -fmessage-length=0  -static  -static-libgcc   -static-libstdc++ -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


