/*
 * Bitset.h
 * Barebones bit-vector class.
 *  Created on: 2013-04-30
 *      Author: sam
 */

#ifndef BITSET_H_
#define BITSET_H_

#include "mtl/Vec.h"
#include <limits>
namespace Minisat {

//=================================================================================================
#define BITSET_ELEMENT_SIZE (sizeof(uint64_t))
class Bitset {
    vec<uint64_t>  buf;
    int sz;

public:
    Bitset():sz(0)  {}
    Bitset(int size):sz(size)  {buf.growTo(size/BITSET_ELEMENT_SIZE + 1);}
    Bitset(int size, bool default_value):sz(size)  {
    	buf.growTo(size/BITSET_ELEMENT_SIZE + 1);
    	if(default_value){
    		for(int i = 0;i<buf.size();i++){
    			buf[i]= std::numeric_limits<uint64_t>::max();
    		}
    	}
    }

    void memset(bool to){
    	if(to){
			for(int i = 0;i<buf.size();i++){
				buf[i]= std::numeric_limits<uint64_t>::max();
			}
		}else{
			for(int i = 0;i<buf.size();i++){
				buf[i]= 0;
			}
		}
    }

    void growTo(int size){
    	buf.growTo(size/BITSET_ELEMENT_SIZE + 1);
    	sz=size;
    }
    void clear (bool dealloc = false) { buf.clear(dealloc);sz=0; }
    void zero(){
    	for(int i = 0;i<buf.size();i++)
    		buf[i]=0;
    }
    int  size  () const { return sz; }
    void copyFrom(const Bitset & from){
    	from.buf.copyTo(buf);
    	sz=from.size();
    }
    void copyTo(Bitset & from) const{
       	buf.copyTo(from.buf);
       	from.sz=size();
       }
    const bool operator [] (int index) const  {
    	int i = index/BITSET_ELEMENT_SIZE ;
    	int rem = index % BITSET_ELEMENT_SIZE;
    	assert(i<buf.size());
    	return buf[i] & (1<<rem);
    }

    void set(int index){
    	assert(index<size());
    	int i = index/BITSET_ELEMENT_SIZE;
    	int r = index %BITSET_ELEMENT_SIZE;

    	buf[i]|=(1<<r);

    }
    void clear(int index){
    	assert(index<size());
    	int i = index/BITSET_ELEMENT_SIZE;
    	int r = index %BITSET_ELEMENT_SIZE;

    	buf[i]&= ~(1<<r);

    }
    void toggle(int index){
    	assert(index<size());
    	int i = index/BITSET_ELEMENT_SIZE;
    	int r = index %BITSET_ELEMENT_SIZE;

    	buf[i]^= (1<<r);

    }
    void Not(Bitset & out){
        	out.clear();
        	out.growTo(size());

        	for(int i = 0;i<buf.size();i++){
        		uint64_t a = buf[i];
        		out.buf[i]=~a;
        	}
        	out.sz=sz;
      }
    void And(const Bitset & with){

        	int max = size();
        	if(max>with.size()){
        		max= with.size();
        	}
        	int max_i=max/BITSET_ELEMENT_SIZE+1;
        	for(int i = 0;i<max_i;i++){

        		uint64_t b = with.buf[i];
        		buf[i]|=b;
        	}

        }

    void Or(const Bitset & with){

          	int max = size();
          	if(max>with.size()){
          		max= with.size();
          	}
          	int max_i=max/BITSET_ELEMENT_SIZE+1;
          	for(int i = 0;i<max_i;i++){

          		uint64_t b = with.buf[i];
          		buf[i]|=b;
          	}

          }
    void And(const Bitset & with, Bitset & out){
    	out.clear();
    	out.growTo(size());
    	int max = size();
    	if(max>with.size()){
    		max= with.size();
    	}
    	int max_i=max/BITSET_ELEMENT_SIZE+1;
    	for(int i = 0;i<max_i;i++){
    		uint64_t a = buf[i];
    		uint64_t b = with.buf[i];
    		out.buf[i]=a&b;
    	}
    	for(int i = max_i;i<buf.size();i++){
    		uint64_t a = buf[i];
    		out.buf[i]=a;
    	}
    	out.sz=sz;
    }

    void Or(const Bitset & with, Bitset & out){
        	out.clear();
        	out.growTo(size());
        	int max = size();
        	if(max>with.size()){
        		max= with.size();
        	}
        	int max_i=max/BITSET_ELEMENT_SIZE+1;
        	for(int i = 0;i<max_i;i++){
        		uint64_t a = buf[i];
        		uint64_t b = with.buf[i];
        		out.buf[i]=a|b;
        	}
        	for(int i = max_i;i<buf.size();i++){
        		uint64_t a = buf[i];
        		out.buf[i]=a;
        	}
        	out.sz=sz;
        }
};
//=================================================================================================
}

#endif /* BITSET_H_ */
