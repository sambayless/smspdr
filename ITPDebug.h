/*
 * ITPDebug.h
 *
 *  Created on: 2012-10-26
 *      Author: sam
 */

#include "Config.h"
#include "aiger/aiger.h"
#include "core/Solver.h"

bool dbg_checkApprox( aiger * aig,int steps, const Minisat::vec<Minisat::Lit>  & interpolant);
bool dbg_constraints( Minisat::Solver & S,Minisat::Lit safety);
bool dbg_checkApprox( aiger * aig, int k,const Minisat::Solver & S);
bool dbg_checkInterpolant( aiger * aig, int k,const Minisat::vec<Minisat::vec<Minisat::Lit> > & interpolant);
bool dbg_is_subset(const Minisat::Solver & currentApprox,const Minisat::Solver & previous_approx);
bool dbg_is_subset(const Minisat::vec<Minisat::vec<Minisat::Lit> > & interpolant,const Minisat::vec<Minisat::vec<Minisat::Lit> > & previous_interpolant, int n);
bool dbg_checkBMC( aiger * aig, int k);
bool 	dbg_checkBMC( aiger * aig, int k,const  Minisat::vec< Minisat::Var> & sOut,const Minisat::Solver & S);
bool 	dbg_checkBMC( aiger * aig, int k,const  Minisat::vec< Minisat::Var> & sOut,const Minisat::vec<Minisat::vec<Minisat::Lit> > & interpolant);
bool 	dbg_checkBMC(aiger * aig, int k,const  Minisat::vec< Minisat::Var> & sOut, const Minisat::vec<Minisat::Lit>  & interpolant);
void dbg_print(const Minisat::vec<Minisat::vec<Minisat::Lit> > & interpolant);
bool dbg_inductiveInterpolant(aiger * aig,int cur_interpolant,const Minisat::vec<Minisat::vec<Minisat::Lit> >& allInterpolants );
bool dbg_inductiveInterpolant(aiger * aig, const Minisat::vec<Minisat::Solver*> &solvers,const Minisat::vec<Minisat::vec<Minisat::LRef> >& allInterpolants );


#ifdef NDEBUG
inline bool dbg_checkInterpolant( aiger * aig, int k,const Minisat::vec<Minisat::vec<Minisat::Lit> > & interpolant){return true;}
inline bool dbg_is_subset(const Minisat::vec<Minisat::vec<Minisat::Lit> > & interpolant,const Minisat::vec<Minisat::vec<Minisat::Lit> > & previous_interpolant, int n){return true;}
inline bool dbg_checkBMC( aiger * aig, int k){return true;}
inline bool 	dbg_checkBMC( aiger * aig, int k,const Minisat::vec<Minisat::vec<Minisat::Lit> > & interpolant){return true;}
inline bool 	dbg_checkBMC(aiger * aig, int k,const  Minisat::vec< Minisat::Var> & sOut, const Minisat::vec<Minisat::Lit>  & interpolant){return true;}
inline bool dbg_constraints( Minisat::Solver & S,Minisat::Lit safety){return true;}
inline void dbg_print(const Minisat::vec<Minisat::vec<Minisat::Lit> > & interpolant){}
inline bool dbg_inductiveInterpolant(aiger * aig, const Minisat::vec<Minisat::Solver*> solvers,const Minisat::vec<Minisat::vec<Minisat::LRef> >& allInterpolants ){return true;}
#endif
