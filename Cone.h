/*
 * Cone.h
 *
 *  Created on: 2013-04-30
 *      Author: sam
 */

#ifndef CONE_H_
#define CONE_H_

#include "mtl/Vec.h"
#include "core/SolverTypes.h"

#include "mtl/Bitset.h"

extern "C" {
#include "aiger/aiger.h"
}

using namespace Minisat;

class ConeOfInfluence{

	aiger* mgr;
	vec<Var> & in_latches;
	vec<bool> seen;
public:
	vec<Bitset> latch_coi;

	ConeOfInfluence(aiger * _mgr,vec<Var> & _in_latches):mgr(_mgr),in_latches(_in_latches){

	}

	void markCone( int l);
	void collectMarkedLatches(vec<Var> & coi);
	void collectCOI(Var from, vec<Var> & coi);
	void collectCOI(const vec<Var>& from, vec<Var> & coi);
	void setupCOI();
};


#endif /* CONE_H_ */
