/*
 * Config.cpp
 *
 *  Created on: 2012-10-31
 *      Author: sam
 */


#include "Config.h"

using namespace Minisat;

#ifndef NDEBUG
int dbg_total_iterations=0;
#endif

static const char* _cat = "CORE";
static const char* _cat_sms = "SMS (SAT modulo SAT solver options)";
static const char* _cat_pdr = "PDR (WARNING: some of these options may lead to incorrect behaviour)";
IntOption     Minisat::opt_verb   ("MAIN", "verb",   "Verbosity level (0=silent, 1=some, 2=more).", 0, IntRange(0, 2));
 DoubleOption  Minisat::opt_var_decay         (_cat, "var-decay",   "The variable activity decay factor",            0.95,     DoubleRange(0, false, 1, false));
 DoubleOption  Minisat::opt_clause_decay      (_cat, "cla-decay",   "The clause activity decay factor",              0.999,    DoubleRange(0, false, 1, false));
 DoubleOption  Minisat::opt_random_var_freq   (_cat, "rnd-freq",    "The frequency with which the decision heuristic tries to choose a random variable", 0, DoubleRange(0, true, 1, true));
 DoubleOption  Minisat::opt_random_seed       (_cat, "rnd-seed",    "Used by the random variable selection",         91648253, DoubleRange(0, false, HUGE_VAL, false));
 IntOption     Minisat::opt_ccmin_mode        (_cat, "ccmin-mode",  "Controls conflict clause minimization (0=none, 1=basic, 2=deep)", 2, IntRange(0, 2));
 IntOption     Minisat::opt_phase_saving      (_cat, "phase-saving", "Controls the level of phase saving (0=none, 1=limited, 2=full)", 2, IntRange(0, 2));
 BoolOption    Minisat::opt_rnd_init_act      (_cat, "rnd-init",    "Randomize the initial activity", false);
 BoolOption    Minisat::opt_luby_restart      (_cat, "luby",        "Use the Luby restart sequence", true);
 IntOption     Minisat::opt_restart_first     (_cat, "rfirst",      "The base restart interval", 100, IntRange(1, INT32_MAX));
 DoubleOption  Minisat::opt_restart_inc       (_cat, "rinc",        "Restart interval increase factor", 2, DoubleRange(1, false, HUGE_VAL, false));
 DoubleOption  Minisat::opt_garbage_frac      (_cat, "gc-frac",     "The fraction of wasted memory allowed before a garbage collection is triggered",  0.20, DoubleRange(0, false, HUGE_VAL, false));
 BoolOption Minisat::opt_force_garbage_collect(_cat,"force-ga","Force garbage collection on solver recycle", true);

//Note: Updated defaults to match the final paper version; removed obsolete options.
//Many of these options have not been tested recently; changing others may lead to incorrect results from PDR (for example, disabling the interpolants option will break pdr).
//Options that _are_ well tested include enabling/disabling coi and ternary simulation, keep-cubes, fast-clause-prop, and enabling naive-modular-sat,

//SAT modulo SAT solver options
BoolOption Minisat::opt_interpolants(_cat_sms,"interpolants","Store the interpolants naturally produced by the SAT modulo SAT solver", true);

IntOption Minisat::opt_eager_prop(_cat_sms,"eager-prop","Controls whether unit propagation is allowed to cross subsolver boundaries. 0= Disable. 1= Enable. 2=Enable, but don't cross the last interpolant. 3= Enable, but don't cross the last interpolant, or any earlier solver. 4= Enable, but dont cross the last interpolant, or any earlier solver, unless they have already had their interpolants strengthened", 1, IntRange(0,5));
IntOption Minisat::opt_eager_prop_break(_cat_sms,"eager-prop-break","", 0, IntRange(0,INT32_MAX));

IntOption Minisat::opt_subsearch(_cat_sms,"subsearch","Control how the solver performs search on the subsolvers: 0=abort as soon as a conflict backtracks past the supersolvers decisionlevel. 1=Abort only once a conflict on the super-interface variables is found, allowing backtracks past those variables in the process. 2=Abort only when the the super-solvers assignment is proven to be in conflict. 3=Don't continue subsearach if the subsolver has backtracked past super-solver decisions. 4=Don't continue past the last interpolant level if any solver has backtracked past a super solver's decisions",2,IntRange(0,4));
BoolOption Minisat::opt_switch_eager(_cat_sms,"switch-eager"," ", false);

BoolOption Minisat::opt_semi_restart(_cat_sms,"semi-restart","",false);
BoolOption Minisat::opt_only_ternary_conflict(_cat_sms,"ternary-conflict","",false);

IntOption Minisat::opt_force_restart_subsolver(_cat_sms,"restart-sub","Force the subsolver to restart (only applies to subsearch=2)",1,IntRange(0,2));

BoolOption Minisat::opt_allow_super_units(_cat_sms,"super-units","Allow (where possible) subsolvers to learn unit clauses on interface variables from their super solvers",true);

BoolOption Minisat::opt_coi(_cat_sms,"coi","Apply Cone of Influence reduction to the assignments passed  down from one solver to the next",true);
BoolOption Minisat::opt_ternary(_cat_pdr,"ternary","Apply Ternary Simulation (as in PDR) to the assignments passed down from one solver to the next",true);
BoolOption Minisat::opt_separate_ca(_cat,"separate-ca","Give each solver a separate clause allocation space (increases memory requirements for large numbers of modules), instead of combining them.", false);


IntOption Minisat::opt_interface_clauses(_cat_sms,"interface-clauses",  "Conflict clauses from sub solvers are treated in the super solver as: 0=conflicts, 1=learnt clauses, 2=permanent clauses", 2, IntRange(1, 2));
IntOption Minisat::opt_learn_sub_reasons(_cat_sms,"interface-reasons",  "Reason vectors for propagations caused by the subsolver are treated in the super solver as: 0=temporaries (discarded after use), 1=learnt clauses, 2=permanent clauses", 2, IntRange(1, 2));
BoolOption Minisat::opt_subsearch_allow_after_interpolant(_cat_sms,"switch-subsearch"," ", false);

BoolOption Minisat::opt_subsume_interpolant(_cat_sms, "subsume-int",        "", true);
BoolOption Minisat::opt_allow_subsolver_redundant(_cat_sms,"allow-subsolver-redundant-check","Allow clauses from a subsolver to be constructed during the litRedundant check (very expensive)",false);
BoolOption Minisat::opt_use_naive_modular_sat_solver(_cat_sms,"naive-modular-sat","",false);
BoolOption Minisat::opt_generalize_during_sat(_cat_sms,"generalize-during-sat","Only applies to the naive solver right now",false);
BoolOption Minisat::opt_coi_during_sat(_cat_sms,"coi-during-sat","Only applies to the naive solver right now",false);


//Controls how restarts work in the SAT modulo SAT solver
BoolOption Minisat::opt_global_restarts(_cat_sms, "global-restarts", "Apply restarts between solvers in the SAT modulo SAT solver", false);
BoolOption Minisat::opt_local_restarts(_cat_sms, "local-restarts", "Apply  restarts within solvers", true);
BoolOption Minisat::opt_first_solver_local_restarts(_cat_sms, "first-local-restarts",        "", false);
BoolOption Minisat::opt_clear_global_restarts(_cat_sms, "clear-global-restarts", "", false);

BoolOption Minisat::opt_global_unit_prop_shortcut(_cat_sms,"prop-shortcut","",false);

//PDR Options

BoolOption Minisat::opt_add_negated_reset(_cat_pdr,"negate-reset","",true);


BoolOption Minisat::opt_selfloop(_cat_pdr,"selfloop","Insert a self-loop into the transition function (required for correct operation of this implementation of IC3)",true);
BoolOption Minisat::opt_early_clause_prop(_cat_pdr,"early-clause-prop","",true);
BoolOption Minisat::opt_check_blocked(_cat_pdr,"block-syntactic","Attempt to block cubes syntactically, as in PDR",true);
BoolOption Minisat::opt_queue_forward(_cat_pdr,"queue-forward","Push cubes forward to be blocked in the next time frame, as in IC3",true);

BoolOption Minisat::opt_keep_cubes(_cat_pdr,"keep-cubes","Keep cubes for the next iteration of modBlockCube, unless they are syntactically blocked. In our paper, this option is combined with \'queue-forward-blocked\'",true);
BoolOption Minisat::opt_forward_all(_cat_pdr,"queue-forward-blocked","Re-queue syntactically blocked cubes for future time frames",true);
BoolOption Minisat::opt_keep_all_cubes(_cat_pdr,"keep-all-cubes","Keep cubes even if they are syntactically blocked in the last tiem frame",false);

BoolOption Minisat::opt_push_forward_without_conflict(_cat_pdr,"push-forward-without-conflict","",false);

BoolOption Minisat::opt_use_queue(_cat_pdr,"use-queue","",true);
BoolOption Minisat::opt_list_interpolants(_cat_pdr,"list-ints","",false);

BoolOption Minisat::opt_generalize_all(_cat_pdr,"generalize-all-interpolants","",true);
IntOption Minisat::opt_sort_when_generalizing(_cat_pdr,"sort-generalize","0=No Sort, 1=Sort by Latch Frequency in Interpolants, 2=Inverse of 1, 3=sort by VSIDS, 4=inverse of 3",1,IntRange(0,5));

BoolOption Minisat::opt_add_global_unit_prop_clauses_to_q(_cat_pdr,"add-unit-prop-to-queue","",false);



BoolOption Minisat::opt_check_subsume_eager(_cat_pdr,"subsume-eager","",false);

BoolOption Minisat::opt_remove_subsumed_last(_cat_pdr,"subsume-last","",true);
BoolOption Minisat::opt_remove_subsumed_during_clause_prop(_cat_pdr,"subsume-during-prop","",true);
BoolOption Minisat::opt_fast_clause_propagation(_cat_pdr,"fast-clause-prop","Don't apply clause propagation to time frames that didn't require strengthening during the current iteration",true);

IntOption Minisat::opt_generalize_rounds(_cat_pdr,"generalize-rounds","Number of times to apply inductive generalization to each clause (0 to disable completely)",1);
BoolOption Minisat::opt_push_clauses_down(_cat_pdr,"push-clauses-down","",true);
IntOption Minisat::opt_recycle(_cat_sms,"recycle","",300,  IntRange(0,10000));
IntOption Minisat::opt_recycle_type(_cat_sms,"recycle-type","",0,IntRange(0,3));
IntOption Minisat::opt_max_generalization_failures(_cat_pdr,"max-fail","Maximum number of generalization failures before giving up",INT32_MAX,IntRange(0,INT32_MAX));
IntOption Minisat::opt_force_full_propagation_interval(_cat_pdr,"force-prop","Use -1 to never force full clause propagation",-1,IntRange(-1,INT32_MAX));


//This option reflects a suboptimality in the implementation from our paper, in which unit propagation started from the reset state instead of the first time frame.
//Selecting prop-zero=1 or 0 is almost certainly a better option, but we leave this option as it was in the paper for reproducing results.
IntOption Minisat::opt_prop_zero(_cat_pdr,"prop-zero","Propagate from the 0th time frame: 0=never, 1=first round only,2=always",2, IntRange(0,2));

//Print out options

BoolOption Minisat::opt_print_gen(_cat_pdr,"print-gen","",false);
BoolOption Minisat::opt_list_gen(_cat_pdr,"list-gen","",false);

BoolOption Minisat::opt_print_coi(_cat_pdr,"print-coi","Print Cone of Influence",false);
BoolOption Minisat::opt_verify(_cat_pdr,"verify","Double check invariant",false);

BoolOption Minisat::opt_prop_first(_cat_pdr,"prop-before-solve","Apply clause propagation before, instead of after, cube blocking",true);
BoolOption Minisat::opt_print_depth(_cat_pdr,"print-depth-lines","Print HWMCC format depth bounds (ie, 'u' lines)\n",false);
BoolOption Minisat::opt_print_ca(_cat,"print-ca","Print size of clause allocator", false);

BoolOption Minisat::opt_print_cubes(_cat_pdr,"print-cubes","",false);
BoolOption Minisat::opt_print_lowest_frame(_cat_pdr,"print-lowest-strengthened-frame","",false);
BoolOption Minisat::opt_print_interpolants(_cat_pdr, "print-interpolants","Print the interpolants at each iteration",false);



