/***************************************************************************************[Solver.cc]
Copyright (c) 2003-2006, Niklas Een, Niklas Sorensson
Copyright (c) 2007-2010, Niklas Sorensson
Copyright (c) 2007-2013, Sam Bayless
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************************************/

#include <math.h>

#include "mtl/Sort.h"
#include "core/Solver.h"

using namespace Minisat;

template<class T> static inline T min(T x, T y) { return (x < y) ? x : y; }
template<class T> static inline T max(T x, T y) { return (x > y) ? x : y; }



//=================================================================================================
// Options:

//This is a hack to keep track of global restarts across solvers, should not use static variables
int Solver::nof_global_conflicts=-1;
int Solver::global_conflictC=0;
int Solver::global_restarts=0;
bool Solver::globalRestartTriggered=false;
//=================================================================================================
// Constructor/Destructor:
#ifndef NDEBUG
int Solver::iteration=0;
#endif

Solver::Solver(ClauseAllocator * external_allocator) :

    // Parameters (user settable):
    //
    verbosity        (0)
  , var_decay        (opt_var_decay)
  , clause_decay     (opt_clause_decay)
  , random_var_freq  (opt_random_var_freq)
  , random_seed      (opt_random_seed)
  , luby_restart     (opt_luby_restart)
  , ccmin_mode       (opt_ccmin_mode)
  , phase_saving     (opt_phase_saving)
  , rnd_pol          (false)
  , rnd_init_act     (opt_rnd_init_act)
  , garbage_frac     (opt_garbage_frac)
  , restart_first    (opt_restart_first)
  , restart_inc      (opt_restart_inc)
	, int_clauses(opt_interface_clauses)
	,int_sub_reasons(opt_learn_sub_reasons)

    // Parameters (the rest):
    //
  , learntsize_factor((double)1/(double)3), learntsize_inc(1.1)

    // Parameters (experimental):
    //
  , learntsize_adjust_start_confl (100)
  , learntsize_adjust_inc         (1.5)

    // Statistics: (formerly in 'SolverStats')
    //
  , solves(0), starts(0), decisions(0), rnd_decisions(0), propagations(0), conflicts(0)
  , dec_vars(0), clauses_literals(0), learnts_literals(0), max_literals(0), tot_literals(0)

  , ok                 (true)
, allocated_index(-1)
,using_external_allocator(external_allocator!=NULL)
  ,ca(external_allocator ? ( *external_allocator):* new ClauseAllocator())
,track_min_trail(0),track_min_level(0)
  , cla_inc            (1)
  , var_inc            (1)
  , watches            (WatcherDeleted(ca))
  , qhead              (0)
  , simpDB_assigns     (-1)
  , simpDB_props       (0)
  , order_heap         (VarOrderLt(activity))
  , progress_estimate  (0)
  , remove_satisfied   (true)

    // Resource constraints:
    //
  , conflict_budget    (-1)
  , propagation_budget (-1)
  , asynch_interrupt   (false)
{

	ca.attachAllocated(this);
	ternary=NULL;
	num_ternary_blocked=0;
	allow_super_units=opt_allow_super_units;
	store_interpolant=opt_interpolants;
	super= NULL;
	freeVars=0;
external_interpolant=NULL;
 sub_offset=0;
 sub_ninterface=-1;
 super_qhead=0;
 super_offset=0;
 super_min_interface=0;
 super_max_interface=0;
 quit_eager=false;
 has_coi=false;
 has_ternary=false;
 max_tmp_confl=-1;tmp_confl=CRef_Undef;
 max_tmp_reason=-1;tmp_reason=CRef_Undef;
super_levels.push();
num_irrelevant=0;
interpolant_size=0;
int_sub_reasons=2;
int_clauses=2;
max_learnts=-1;
must_cancel=-1;

disableSubSolve=false;
disableSubProp=false;
level_q=0;
top_solver=false;
sub_needs_refresh=true;
forced_inputs=0;
forced_outputs=0;
num_coi_blocked=0;
total_assign_lits=0;
switch_eager=false;
disable_simplification=false;
store_interpolant=true;

attempted_solve=false;
succesful_solve=false;
had_unit_prop=false;
interpolant_increase=0;
eager_propagation=opt_eager_prop;
disableSubProp = opt_eager_prop==0;

initial_level=0;
allow_unforced_subsearch=false;
has_any_super_backtracked=false;
cubes=NULL;
max_frame=-1;
frame=-1;
allow_unforced_subsearch= (opt_subsearch==classic_search);
#ifndef NDEBUG
	dbg_sub_mini= new DbgMini();
    maxSubVar=0;
    computedDbgClauses=false;
  //  iteration=0;
    dbg_solverNum=0;
    dbg_super_int=-1;
    dbg_super_off=0;
    dbg_super=NULL;
    dbg_property=lit_Undef;
    dbg_exported=false;
#endif

}


Solver::~Solver()
{
	if(!using_external_allocator){
		delete(&ca);
	}else if (allocated_index>=0){
		ca.removeAllocated(this);
	}
}

void Solver::addClauses(const vec<Lit> & clauses){
	static vec<Lit> c;
	c.clear();
	for(int i = 0;i<clauses.size();i++){
			Lit l = clauses[i];
			if(l==lit_Undef){
				addClause(c);
				c.clear();
			}else{
				while(nVars()<=var(l))
					newVar();
				c.push(l);
			}
		}
	assert(c.size()==0);
}

//=================================================================================================
// Minor methods:


// Creates a new SAT variable in the solver. If 'decision' is cleared, variable will not be
// used as a decision variable (NOTE! This has effects on the meaning of a SATISFIABLE result).
//
Var Solver::newVar(bool sign, bool dvar)
{
    int v = nVars();
    if(freeVars>0){
    	freeVars--;
    	setDecisionVar(v, dvar);
    	return v;
    }else{

		watches  .init(mkLit(v, false));
		watches  .init(mkLit(v, true ));
		assigns  .push(l_Undef);
		vardata  .push(mkVarData(CRef_Undef, 0));

		activity .push(rnd_init_act ? drand(random_seed) * 0.00001 : 0);
		seen     .push(0);
		polarity .push(sign);
		decision .push();
		trail    .capacity(v+1);
		setDecisionVar(v, dvar);
		return v;
    }
}

void 	Solver::clearVar(Var v){
	assert(decisionLevel()==0);
	watches[ mkLit(v, false)].clear();
	watches[ mkLit(v, true)].clear();
    polarity[v]=true;
    activity[v]=(rnd_init_act ? drand(random_seed) * 0.00001 : 0);
    seen[v]=0;
    decision[v]=false;//make sure this variable isn't decided
    vardata[v]=mkVarData(CRef_Undef, 0);

	assigns[v]=l_Undef;
	freeVars++;
}

bool Solver::addClause_(vec<Lit>& ps, bool learnt)
{
	dbg_AddClause(ps);
    assert(decisionLevel() == 0);
    if (!ok) return false;

    // Check if clause is satisfied and remove false/duplicate literals:
    sort(ps);
    Lit p; int i, j;
    for (i = j = 0, p = lit_Undef; i < ps.size(); i++)
        if (value(ps[i]) == l_True || ps[i] == ~p)
            return true;
        else if (value(ps[i]) != l_False && ps[i] != p)
            ps[j++] = p = ps[i];
    ps.shrink(i - j);

    if (ps.size() == 0)
        return ok = false;
    else if (ps.size() == 1){
        uncheckedEnqueue(ps[0]);
        int dp = disableSubProp;
        int ds = disableSubSolve;
        disableSubProp=true;
        disableSubSolve=true;
        ok = (propagate() == CRef_Undef);
        disableSubProp=dp;
           disableSubSolve=ds;
           return ok;
       // return ok;// = (propagate() == CRef_Undef); //don't want to do this in the modular solver, because it will cause all the sub solvers to prop (and may learn interpolants, etc)
    }else{
        CRef cr = ca.alloc(ps, learnt);
        if(learnt)
        	learnts.push(cr);
        else
        	clauses.push(cr);
        attachClause(cr);
    }

    return true;
}


void Solver::attachClause(CRef cr) {
    const Clause& c = ca[cr];
    assert(c.size() > 1);
    watches[~c[0]].push(Watcher(cr, c[1]));
    watches[~c[1]].push(Watcher(cr, c[0]));
    if (c.learnt()) learnts_literals += c.size();
    else            clauses_literals += c.size(); }


void Solver::detachClause(CRef cr, bool strict) {
    const Clause& c = ca[cr];
    assert(c.size() > 1);
    
    if (strict){
        remove(watches[~c[0]], Watcher(cr, c[1]));
        remove(watches[~c[1]], Watcher(cr, c[0]));
    }else{
        // Lazy detaching: (NOTE! Must clean all watcher lists before garbage collecting this clause)
        watches.smudge(~c[0]);
        watches.smudge(~c[1]);
    }

    if (c.learnt()) learnts_literals -= c.size();
    else            clauses_literals -= c.size();
}


LRef Solver::buildInterpolantClause(vec<Lit> &c){
#ifndef NDEBUG
	if(opt_use_naive_modular_sat_solver)
		dbg_AddClause(c);
#endif
	if(c.size()==1){
		if(!inInterpolant(var(c[0]))){
			cancelUntil(0);
			enqueue(c[0]);
			interpolant_size++;
			interpolant_increase++;
		}
		return LRef (c[0]);
	}
#ifndef NDEBUG
	for(int i = 0;i<c.size();i++){
		assert(var(c[i])<nVars());
		assert(sub_interface(c[i]));
	}

#endif


	CRef cr = ca.alloc(c, false, opt_subsume_interpolant);
	LRef lr = LRef(cr);

 	attachClause(cr);
   	interpolant_size++;interpolant_increase++;

#ifndef NDEBUG
 	vec<Lit> t;
 	LRef(cr).copyTo(t, ca);
 	Solver * s = this;
 	assert( dbg_proveLearntClause(t));
 	while(s->subSolvers.size()){
 		assert(s->dbg_proveLearntClause(t));
 		s = s->subSolvers[0];
 	}
#endif
   	return lr;
}


bool Solver::addToInterpolant(Lit l){

	if(inInterpolant(var(l)))
		return false;


 	in_interpolant[var(l)]=true;
 	interpolant.push(LRef(l));
 	interpolant_size++;interpolant_increase++;
 	cancelUntil(0);
 	enqueue(l);
 	if(opt_add_global_unit_prop_clauses_to_q && this->cubes && subSolvers.size() && super){

 				TCube tc(this->frame+1);

 				for(int i = 1;i<=sub_ninterface;i++){
 					assert(sub_interface(i));
 					if(i!=var(l)){
 						tc.assignment->push(mkLit(i,true));//add lits from the reset state to the assignment. This is safe, because we know the assignment must already have one lit that is outside of the reset state already
 					}
 				}

 			}

#ifndef NDEBUG
 	vec<Lit> c;
 	LRef(l).copyTo(c, ca);
 	assert(dbg_proveLearntClause(c));
 	Solver * s = this;
 	/*while(s->subSolvers.size()){
 		assert(s->dbg_proveLearntClause(c));
 		s = s->subSolvers[0];
 	}*/
#endif
 	return true;

 }

void Solver::attachSubSolver(Solver * subSolver, int offset, int min_sub_interface, int max_sub_interface, bool re_attach){

		assert(!subSolver->super);
		assert(subSolver!=this);
    	subSolvers.push(subSolver);

    	subSolver->must_cancel=0;
    	subSolver->super_qhead=0;

    	solver_cref.push(ca.allocTheoryMarker(solver_cref.size()));
    	assert(ca[solver_cref.last()].isTheoryMarker());
    	int interface =  max_sub_interface - min_sub_interface;
    	sub_qhead.push(0);


		sub_offset=offset;
		sub_ninterface=interface;

		in_interpolant.growTo(sub_ninterface+1);


    	seen_subsume.clear();
    	seen_subsume.growTo(sub_ninterface*2+3);

    	subSolver->super_offset=offset;
    	subSolver->super_min_interface=min_sub_interface;
    	subSolver->super_max_interface=max_sub_interface;

    	subSolver->super= this;
    	track_min_trail=0;
    	track_min_level=0;
    	subSolver->track_min_trail=0;
    	subSolver->track_min_level=0;
    	subSolver->has_coi=false;
    	recursiveCancelUntil(0);


    	assert(decisionLevel()==0);
    	assert(subSolver->decisionLevel()==0);
    	subtrail.clear();
    	for(int i = 0;i<trail.size();i++){
    		if(sub_interface(trail[i])){
    			subtrail.push(trail[i]);
    		}
    	}

    	sub_needs_refresh=false;


#ifndef NDEBUG
        	if(!re_attach){
	int pos = 0;
    	subSolver->dbg_exportClauses(this, subSolvers.size()-1);

    	Solver * st = this;
    	while(st && st->super){
    		st->dbg_exported=false;
    		int n = 0;
    		for(int i = 0;i<st->super->subSolvers.size();i++){
    			if(st->super->subSolvers[i]==st){
    				n=i;
    				break;
    			}
    		}
    		st->dbg_exportClauses(st->super,n);
    		st= st->super;
    	}
        	}
#endif

    }

void Solver::detachSubSolvers(){
	while(subSolvers.size()){
		detachSubSolver(subSolvers.size()-1);
	}
}

void Solver::detachSubSolver(int solver){
if(solver>= subSolvers.size() || solver<0){
	fprintf(stderr, "No such solver to detatch: %d!\n", solver);
	exit(1);
}
	assert(solver<subSolvers.size());
			{
		Solver * subSolver = subSolvers[solver];

		for(int i = solver+1;i<subSolvers.size();i++){
			subSolvers[i-1]=subSolvers[i];
			solver_cref[i-1]=solver_cref[i];
			sub_qhead[i-1] = sub_qhead[i];
		}
		subSolvers.shrink(1);

		solver_cref.shrink(1);
		sub_qhead.shrink(1);
		subSolver->super_qhead=0;
		cancelUntil(0);
		subSolver->cancelUntil(0);
		subSolver->super_units.clear();
		assumptions.clear();
		subSolver->super= NULL;
		subSolver->super_offset=0;
		subSolver->super_min_interface=0;
		subSolver->super_max_interface=0;


#ifndef NDEBUG
		subSolver->dbg_exported=false;

			//if we left the subsolver attached during subset checking, then the super solver may have learnt additional facts that need to be
			//left in the local dbg solver
			for(int i = 0;i<learnts.size();i++){
				dbg_AddLocalClause(ca[learnts[i]]);
			}
			for(int i = 0;i<trail.size();i++){
				assert(level(var(trail[i]))==0);
				static vec<Lit> unit;
				unit.clear();
				unit.push(trail[i]);
				dbg_AddLocalClause(unit);
			}


		dbgclauses.clear();
		for(int i = 0;i<dbg_local_clauses.size();i++){
			dbgclauses.push();
			dbg_local_clauses[i].copyTo(dbgclauses[dbgclauses.size()-1]);
		}
		for(int i = 0;i<clauses.size();i++){
				assert(dbg_proveClause(ca[clauses[i]]));
			}
			for(int i = 0;i<learnts.size();i++){
					assert(dbg_proveClause(ca[learnts[i]]));
				}
			for(int i = 0;i<trail.size();i++){
				assert(level(var(trail[i]))==0);
				assert(dbg_proveUnit(trail[i]));
			}


#endif
	}
}

void Solver::removeClause(CRef cr) {
	assert(cr!=CRef_Undef);

    Clause& c = ca[cr];
    assert(!c.isTheoryMarker());
    detachClause(cr);
    // Don't leave pointers to free'd memory!
    if (locked(c)) vardata[var(c[0])].reason = CRef_Undef;
    c.mark(1); 
    ca.free(cr);
}


bool Solver::satisfied(const Clause& c) const {
    for (int i = 0; i < c.size(); i++)
        if (value(c[i]) == l_True)
            return true;
    return false; }


// Revert to the state at given level (keeping all assignment at 'level' but not beyond).
//
void Solver::cancelUntil(int level) {
	assert(level>=0);
    if (decisionLevel() > level){
    	subtrail.clear();
        for (int c = trail.size()-1; c >= trail_lim[level]; c--){
            Var      x  = var(trail[c]);
            if(super_interface(x) && reason(x) == CRef_Undef ){
            	forced_outputs--;
            }else if (sub_interface(x)){
            	forced_inputs--;
            }
            assigns [x] = l_Undef;
            if (phase_saving > 1 || ((phase_saving == 1) && c > trail_lim.last()))
                polarity[x] = sign(trail[c]);
            insertVarOrder(x); }
        qhead = trail_lim[level];
        trail.shrink(trail.size() - trail_lim[level]);
        trail_lim.shrink(trail_lim.size() - level);
        if(trail.size()<track_min_trail){
        	track_min_trail=trail.size();
        }
        if(decisionLevel()<track_min_level){
        	track_min_level=decisionLevel();
        }
        has_ternary=false;

        //Mark the subsolvers, so that they know to backtrack at least this far before they are next used (but don't actually make them backtrack yet)
        for(int s = 0;s<subSolvers.size();s++){
        	if(subSolvers[s]->must_cancel<0 || level<subSolvers[s]->must_cancel)
        		subSolvers[s]->must_cancel=level;
        	sub_qhead[s]=0;
        	subSolvers[s]->has_coi=false;//invalidate the subsolver's coi; it has to be recomputed.
        }

    }
}


//=================================================================================================
// Major methods:


Lit Solver::pickBranchLit()
{
    Var next = var_Undef;

    // Random decision:
    if (drand(random_seed) < random_var_freq && !order_heap.empty()){
        next = order_heap[irand(random_seed,order_heap.size())];
        if (value(next) == l_Undef && decision[next])
            rnd_decisions++; }

    // Activity based decision:
    while (next == var_Undef || value(next) != l_Undef || !decision[next])
        if (order_heap.empty()){
            next = var_Undef;
            break;
        }else
            next = order_heap.removeMin();

    return next == var_Undef ? lit_Undef : mkLit(next, rnd_pol ? drand(random_seed) < 0.5 : polarity[next]);
}


/*_________________________________________________________________________________________________
|
|  analyze : (confl : Clause*) (out_learnt : vec<Lit>&) (out_btlevel : int&)  ->  [void]
|  
|  Description:
|    Analyze conflict and produce a reason clause.
|  
|    Pre-conditions:
|      * 'out_learnt' is assumed to be cleared.
|      * Current decision level must be greater than root level.
|  
|    Post-conditions:
|      * 'out_learnt[0]' is the asserting literal at level 'out_btlevel'.
|      * If out_learnt.size() > 1 then 'out_learnt[1]' has the greatest decision level of the 
|        rest of literals. There may be others from the same level though.
|  
|________________________________________________________________________________________________@*/
bool Solver::analyze(CRef confl, vec<Lit>& out_learnt, int& out_btlevel)
{
	assert(dbg_proveConflict(confl));
    int pathC = 0;
    Lit p     = lit_Undef;

#ifndef NDEBUG
    for (int i = 0;i<seen.size();i++){
    	assert(!seen[i]);
    }
#endif
    // Generate conflict clause:
    //
    out_learnt.push();      // (leave room for the asserting literal)
    int index   = trail.size() - 1;

    do{
     	if(confl==CRef_Undef){
			//then this is a global decision. .
			assert(p!=lit_Undef);
			assert(super_interface(p));

			//can probably do this more efficiently
			for(int i = trail_lim[decisionLevel()-1];i<=index;i++)
					seen[var(trail[i])]=false;
		   for (int j = 0; j < out_learnt.size(); j++) seen[var(out_learnt[j])] = 0;
			out_learnt.clear();
			return false;
		}

        assert(confl != CRef_Undef); // (otherwise should be UIP)
        Clause& c = getClause(p,confl);//Lazily extracts an interface clause from the subsolver if needed
        assert(dbg_proveClause(c));
        if (c.learnt())
            claBumpActivity(c);

        for (int j = (p == lit_Undef) ? 0 : 1; j < c.size(); j++){
            Lit q = c[j];

            if (!seen[var(q)] && level(var(q)) > 0){
                varBumpActivity(var(q));
                assert(dbg_checkEarlierOnTrail(q,index));
                seen[var(q)] = 1;
                if (level(var(q)) >= decisionLevel())
                    pathC++;
                else{
                    out_learnt.push(q);
                }
            }
        }
        assert(dbg_checkPartialLearnt(index,seen, out_learnt));
        // Select next clause to look at:
        while (!seen[var(trail[index--])]);
        assert(index>=-1);
        p     = trail[index+1];
        confl = reason(var(p));
        assert(level(var(p))==decisionLevel());
        seen[var(p)] = 0;
        pathC--;

    }while (pathC > 0);
    out_learnt[0] = ~p;
    assert(dbg_proveLearntClause(out_learnt));
    // Simplify conflict clause:
    //
    int i, j;
    out_learnt.copyTo(analyze_toclear);
    if (ccmin_mode == 2){
        uint32_t abstract_level = 0;
        for (i = 1; i < out_learnt.size(); i++)
            abstract_level |= abstractLevel(var(out_learnt[i])); // (maintain an abstraction of levels involved in conflict)

        for (i = j = 1; i < out_learnt.size(); i++)
            if (reason(var(out_learnt[i])) == CRef_Undef || (!opt_allow_subsolver_redundant && ca[reason(var(out_learnt[i]))].isTheoryMarker()) || !litRedundant(out_learnt[i], abstract_level))
                out_learnt[j++] = out_learnt[i];
        
    }else if (ccmin_mode == 1){
    	//WATCH OUT, this is untested with the SAT modulo SAT solver
        for (i = j = 1; i < out_learnt.size(); i++){
            Var x = var(out_learnt[i]);

            if (reason(x) == CRef_Undef  || (!opt_allow_subsolver_redundant && ca[reason(var(out_learnt[i]))].isTheoryMarker()) )
                out_learnt[j++] = out_learnt[i];
            else{
                Clause& c = getClause(out_learnt[i],reason(var(out_learnt[i])));
                for (int k = 1; k < c.size(); k++)
                    if (!seen[var(c[k])] && level(var(c[k])) > 0){
                        out_learnt[j++] = out_learnt[i];
                        break; }
            }
        }
    }else
        i = j = out_learnt.size();

    max_literals += out_learnt.size();
    out_learnt.shrink(i - j);
    tot_literals += out_learnt.size();

    // Find correct backtrack level:
    //
    if (out_learnt.size() == 1)
        out_btlevel = 0;
    else{
        int max_i = 1;
        // Find the first literal assigned at the next-highest level:
        for (int i = 2; i < out_learnt.size(); i++)
            if (level(var(out_learnt[i])) > level(var(out_learnt[max_i])))
                max_i = i;
        // Swap-in this literal at index 1:
        Lit p             = out_learnt[max_i];
        out_learnt[max_i] = out_learnt[1];
        out_learnt[1]     = p;
        out_btlevel       = level(var(p));
    }
    assert(dbg_proveLearntClause(out_learnt));
    for (int j = 0; j < analyze_toclear.size(); j++) seen[var(analyze_toclear[j])] = 0;    // ('seen[]' is now cleared)
#ifndef NDEBUG
    for (int i = 0;i<seen.size();i++){
    	assert(!seen[i]);
    }
#endif
    return true;
}


// Check if 'p' can be removed. 'abstract_levels' is used to abort early if the algorithm is
// visiting literals at levels that cannot be removed later.
bool Solver::litRedundant(Lit p, uint32_t abstract_levels)
{
    analyze_stack.clear(); analyze_stack.push(p);
    int top = analyze_toclear.size();
    while (analyze_stack.size() > 0){
        assert(reason(var(analyze_stack.last())) != CRef_Undef);
        Clause& c = opt_allow_subsolver_redundant ?  getClause(~(analyze_stack.last()),reason(var(analyze_stack.last()))) :ca[reason(var(analyze_stack.last()))] ;
        //note: if we aren't allowing analysis of subsolver clauses here, then we don't call get clause, which means subsolver clauses are left a just solver_ref clauses (ie, they are empty clauses that have the 'isTheoryMarker()' property).
        //we we do call getClause, then such clauses are replaced filled in with their subsolver clauses as needed. So the following check can't be true if we produced the clause using getClause().
		if(c.isTheoryMarker()){
			 for (int j = top; j < analyze_toclear.size(); j++)
					seen[var(analyze_toclear[j])] = 0;
				analyze_toclear.shrink(analyze_toclear.size() - top);
				analyze_stack.clear();
			return false;
		}

        analyze_stack.pop();
        for (int i = 1; i < c.size(); i++){
            Lit p  = c[i];
            if (!seen[var(p)] && level(var(p)) > 0){
                if (reason(var(p)) != CRef_Undef && (abstractLevel(var(p)) & abstract_levels) != 0){
                    seen[var(p)] = 1;
                    analyze_stack.push(p);
                    analyze_toclear.push(p);
                }else{
                    for (int j = top; j < analyze_toclear.size(); j++)
                        seen[var(analyze_toclear[j])] = 0;
                    analyze_toclear.shrink(analyze_toclear.size() - top);
                    return false;
                }
            }
        }
    }

    return true;
}

void Solver::analyzeFinalSuper(CRef confl, Lit skip_lit, vec<Lit>& out_conflict)
{
#ifndef NDEBUG
    for (int i = 0;i<seen.size();i++){
    	assert(!seen[i]);
    }
#endif
	out_conflict.clear();
    if (decisionLevel() == 0) return;

    Clause & c = ca[confl];

    for (int i = 0; i < c.size(); i++){
        Var     x = var(c[i]);
    	if(x==var(skip_lit))
    		continue;

        assert(x>=0);
               assert(x<nVars());
        if (level(x) > 0)
            seen[x] = 1;
        assert(value(x)!=l_Undef);
    }

    int     start = trail.size()-1;
    int i;
    for ( i = start; i >= trail_lim[0]; i--){
        Var     x = var(trail[i]);
        assert(value(x)!=l_Undef);
        assert(x>=0);
        assert(x<nVars());

        if (seen[x]){
        	  assert(x!=var(skip_lit));
            CRef r = reason(x);
            if (r == CRef_Undef){
            	Var v = var(trail[i]);
            	assert(super_interface(v));
            	assert(value(trail[i])==l_True);
                assert(level(x) > 0);
                assert(level(v) > 0);
                Lit super_lit = toSuper(trail[i]);
                out_conflict.push(~super_lit);
            }else{
				Clause& c = getClause(trail[i],r);
				for (int j = 0; j < c.size(); j++){
					assert(value(c[j])!=l_Undef);
					if (level(var(c[j])) > 0){
						seen[var(c[j])] = 1;
#ifndef NDEBUG
		bool found = false;
		for(int k = 0;k<=i;k++){
			if(var(trail[k])==var(c[j])){
				found = true;
				break;
			}
		}
		assert(found);
#endif
					}
				}
            }
            seen[x] = 0;
        }
    }
#ifndef NDEBUG
    for (int i = 0;i<seen.size();i++){
    	assert(!seen[i]);
    }
#endif

}


/*_________________________________________________________________________________________________
|
|  analyzeFinal : (p : Lit)  ->  [void]
|  
|  Description:
|    Specialized analysis procedure to express the final conflict in terms of assumptions.
|    Calculates the (possibly empty) set of assumptions that led to the assignment of 'p', and
|    stores the result in 'out_conflict'.
|________________________________________________________________________________________________@*/
void Solver::analyzeFinal(Lit p, vec<Lit>& out_conflict)
{
	assert(value(p)==l_True);
    out_conflict.clear();
    out_conflict.push(p);

    if (decisionLevel() == 0)
        return;

    seen[var(p)] = 1;

    for (int i = trail.size()-1; i >= trail_lim[0]; i--){
        Var x = var(trail[i]);
        if (seen[x]){
            if (reason(x) == CRef_Undef){
            	//assert(x!=var(p));//else this is a vacuous reason!
                assert(level(x) > 0);
                out_conflict.push(~trail[i]);
            }else{
                Clause& c = getClause(trail[i], reason(x));
                for (int j = 1; j < c.size(); j++)
                    if (level(var(c[j])) > 0)
                        seen[var(c[j])] = 1;
            }
            seen[x] = 0;
        }
    }

    seen[var(p)] = 0;

}


void Solver::uncheckedEnqueue(Lit p, CRef from)
{
    assert(value(p) == l_Undef);
    assigns[var(p)] = lbool(!sign(p));
    vardata[var(p)] = mkVarData(from, decisionLevel());
    trail.push_(p);

#ifndef NDEBUG
    printTrail();
    assert(dbg_proveUnitPropagation(p));
    if(decisionLevel()==0){
    	assert(dbg_proveUnit(p));
    }
#endif
}

bool Solver::handleSuperConflict(CRef confl, vec<Lit> & conflict, int & start_trail){


	while(okay() && confl!=CRef_Undef){
			conflict.clear();

			if(decisionLevel()==0){
			   ok=false;
			   return false;
		   }
			int min_backtrack =0;

			if (!analyze(confl, conflict, min_backtrack)){
				 analyzeFinalSuper(confl,lit_Undef,conflict);
				 assert(dbg_proveLearntSuperClause(conflict));
				 assert(conflict.size());
				 varDecayActivity();
				 claDecayActivity();
				 return false;
		    }else{
		    	assert(min_backtrack<decisionLevel());
				 cancelUntil(min_backtrack);

				 start_trail = min(start_trail, qhead);
				 assert(conflict.size());
				   if (conflict.size() == 1){
					   uncheckedEnqueue(conflict[0]);
					   vardata[var(conflict[0])] = mkVarData(CRef_Undef,min_backtrack);
				   }else{
					   CRef cr = ca.alloc(conflict, true);
					   learnts.push(cr);
					   attachClause(cr);
					   claBumpActivity(ca[cr]);
					   uncheckedEnqueue(conflict[0], cr);
					   vardata[var(conflict[0])] = mkVarData(cr,min_backtrack);
				   }
				   conflict.clear();
				   varDecayActivity();
				   claDecayActivity();
				   confl=propagate();
			}
	}
	return okay() ?  true:false;
}

void Solver::lazyCancel(int super_level, Lit super_decision)
{

	assert(super_levels.size()>decisionLevel());
	assert(super_levels[0].decision_level==0);

	//cancel lazilly:
	if(must_cancel>-1 && superDecisionLevel()>must_cancel ){
			int backtrack_to=-1;

		//find the level to backtrack to:
		for(int i = decisionLevel();i>=0;i--){
			if(super_levels[i].decision_level<=must_cancel){
				backtrack_to=i;
				break;
			}
		}
		assert(backtrack_to>=0);
		cancelUntil(backtrack_to);
	}

	if(superDecisionLevel()>super_level ){
		int backtrack_to=-1;

		//find the level to backtrack to:
		for(int i = decisionLevel();i>=0;i--){
			if(super_levels[i].decision_level<=super_level){
				backtrack_to=i;
				break;
			}
		}
		assert(backtrack_to>=0);
		cancelUntil(backtrack_to);

	}
	Lit superDec =  superDecision();
	if(decisionLevel()>0 && superDecisionLevel()==super_level && superDec != super_decision && superDec != lit_Undef){


		cancelUntil(decisionLevel()-1);
	}

	must_cancel=-1;
}

//Applies unit propagation from this solver's super solver into this solver.
//Note that this implementation is MUCH more complicated than it needs to be.
//For an efficient, simple, clean implementation, see ModSAT.
bool Solver::propagateTheory(int subSolverNum, int super_level, Lit super_decision, vec<Lit> & to_propagate, int subqhead, vec<Lit> & conflict){

	assert(super_level==0 || super_decision!=lit_Undef);
	assert(super_level>0 || super_decision==lit_Undef);
	assert(conflict.size()==0);
#ifndef NDEBUG
    //double check the interface lines up correctly!
	dbg_super_int=nVars()- super->sub_ninterface;
	dbg_super_off=super->sub_offset;
	dbg_super = super;
#endif

	//Before we do anything else, make sure we cancel at least as far back as the super solver has cancelled
	lazyCancel(super_level,super_decision);

	//this can be handled _much_ better.
	if(decisionLevel()==0){
		assert(decisionLevel()==0);

		for(int i = super_qhead;i<trail.size();i++){
			Lit p = trail[i];

			//if we assigned an interface variable, and it was previously unassigned in the super-solver, enqueue it in the super solver
			if(super_interface(p)){
				Lit super_lit =toSuper(p);
				if(super->value(super_lit) ==l_Undef){
					super->cancelUntil(0);//is this ever needed? should it be?
					super->enqueue(super_lit, CRef_Undef);//level 0
				}else if (super->value(super_lit) ==l_False) {

						super->cancelUntil(0);//is this ever needed? should it be?
						if(super->value(super_lit) ==l_Undef){
							super->cancelUntil(0);//is this ever needed? should it be?
							super->enqueue(super_lit, CRef_Undef);//level 0
						}else{
							conflict.push(super_lit);
							assert(dbg_proveLearntSuperClause(conflict));
							 if(super || (subSolvers.size() && ! opt_first_solver_local_restarts))
							            	global_conflictC++;
							return false;
						}

				}
			}
			super_qhead++;
		}
	}


	//So, if you just enqueue all the globally propagated lits at once and then propagate them all together, you run into a problem:
	//If they would already have been propagated locally, on their own (as happens _often_ because if a clause containing only interface variables is in a module locally, then it is also in the super solver,
	//and if it propagates something to the super solver, it might also propagated it locally independently)
	//then the reason information is lost, making conflict analysis weaker.

	//For this reason, we are going to walk through each assignment from the super solver and propagate them individually


	int initial_level = decisionLevel();
	assert(decisionLevel()==0 || qhead==trail.size());

	if(to_propagate.size()){
		had_unit_prop=true;
	}
	do{
		int startTrail = trail.size();
		if(decisionLevel()==0 && super_units.size()){
			int j=0;
			int i;
			for(i = 0;i<super_units.size();i++){

				Lit local_lit = super_units[i];
				assert(super_interface(local_lit));
				if(value(local_lit)==l_Undef){
					if(decisionLevel()==0){
						newDecisionLevel();
						if( super_level==0 && ! allow_super_units){
							assert(decisionLevel()==1);
						}
						assert(super_levels.size()>decisionLevel());
						super_levels[decisionLevel()].decision_level=0;
						super_levels[decisionLevel()].decision= lit_Undef;

					}
					uncheckedEnqueue(local_lit,CRef_Undef);
					super_units[j++]=local_lit;
				}else if (value(local_lit)==l_False){
					CRef r = reason(var(local_lit));
				   if (r != CRef_Undef){
					   analyzeFinalSuper(r, local_lit,conflict);
					   Lit super_lit = toSuper(local_lit);//mkLit(var(local_lit)+super_offset),sign(local_lit);
					   conflict.push(~super_lit);
				   }else{
					   assert(level(var(local_lit))==0);
					   conflict.clear();
					   Lit super_lit = toSuper(local_lit); //mkLit(var(local_lit)+super_offset),sign(local_lit);
					   conflict.push(~super_lit);

				   }
				   super_units.clear();
					assert(dbg_proveLearntSuperClause(conflict));
					 if(super || (subSolvers.size() && ! opt_first_solver_local_restarts))
					            	global_conflictC++;
				   return false;
				}else{
					assert(value(local_lit)==l_True);
				}
			}
			super_units.shrink(i-j);
		}

		if(qhead==trail.size()){


			for(int i = subqhead;i<to_propagate.size();i++){
				Lit local_lit = fromSuper(to_propagate[i]);
				assert(super->level(var(to_propagate[i]))==super_level);
				assert(super_interface(local_lit));
				if(value(local_lit)==l_Undef){
					if(super_level==0 && ! allow_super_units){
						super_units.push(local_lit);//remember this unit, so we can prop it later after having backtracked past it.
					}
					if((superDecisionLevel()<super_level || (super_level==0 && decisionLevel()==0 && ! allow_super_units))){
						newDecisionLevel();
						if( super_level==0 && ! allow_super_units){
							assert(decisionLevel()==1);
						}
						assert(super_levels.size()>decisionLevel());
						super_levels[decisionLevel()].decision_level=super_level;
						super_levels[decisionLevel()].decision=super_decision;

					}
					forced_outputs++;
					uncheckedEnqueue(local_lit,CRef_Undef);
				}else if (value(local_lit)==l_False){
					CRef r = reason(var(local_lit));
				   if (r != CRef_Undef){
					   analyzeFinalSuper(r, local_lit,conflict);
					   Lit super_lit = toSuper(local_lit);//mkLit(var(local_lit)+super_offset),sign(local_lit);
					   conflict.push(~super_lit);
				   }else{
					   assert(level(var(local_lit))==0);
					   conflict.clear();
					   Lit super_lit = toSuper(local_lit); //mkLit(var(local_lit)+super_offset),sign(local_lit);
					   conflict.push(~super_lit);

				   }
				   if(super || (subSolvers.size() && ! opt_first_solver_local_restarts))
				              	global_conflictC++;
					assert(dbg_proveLearntSuperClause(conflict));
				   return false;
				}else{
					assert(value(local_lit)==l_True);
					if(super_level==0 && ! allow_super_units && level(var(local_lit))>0){
							super_units.push(local_lit);//remember this unit, so we can prop it later after having backtracked past it.
						}
				}
			}
			subqhead = to_propagate.size();
		}

		initial_level = decisionLevel();
		track_min_trail= qhead;
		track_min_level= decisionLevel();

		CRef confl = propagate();
		if(!okay() || confl!=CRef_Undef){
		  if(  handleSuperConflict(confl,conflict,startTrail)){
			  //don't do anything
		  }else{
			  if(super->decisionLevel()>super_level){
				   super->cancelUntil(super_level);
			   }
			  return false;
		  }
		}
		 if(opt_global_unit_prop_shortcut){
		   //check if unit prop alone has forced the input literals to equal the output literals:
		   if(forced_inputs>=forced_outputs && forced_outputs>0){
			   bool all_same = true;

			  // int min_backtrack = 0;
			   local_conflict.clear();
			   for(int i = trail.size()-1;i>=0;i--){//in backward order to ensure the highest level lits are at the begining of the conflict
				   if(super_interface(trail[i])){

					   if( reason(var(trail[i]))==CRef_Undef)
					   {
						   Lit local_lit = fromSuper(trail[i]);
						   //this is a slight abuse of 'to_super', since I'm using it to switch to the inputs of this time frame, not the inputs of the super time frame. I'm counting on these to happen to be the same.
						   if(value(local_lit)!=l_True){
							   all_same=false;
							   local_conflict.clear();
							   break;
						   }
						   local_conflict.push(~to_propagate[i]);
					   }
				   }
			   }
			   if(all_same){
				   //then we can force a conflict and analyze it to strengthen our interpolant.
				  //first, create the new clause
				   assert(dbg_proveLearntClause(local_conflict));
				   if (local_conflict.size() == 1){
					   cancelUntil(0);
					  uncheckedEnqueue(local_conflict[0]);

				  }else{
					  assert(decisionLevel()==level(var(local_conflict[0])));
					  CRef cr = ca.alloc(local_conflict, true);
					  learnts.push(cr);
					  attachClause(cr);
					  claBumpActivity(ca[cr]);


					  if(  handleSuperConflict(cr,conflict,startTrail)){
						  //don't do anything
					  }else{
						  if(super->decisionLevel()>super_level){
							   super->cancelUntil(super_level);
						   }
						  return false;
					  }
				  }

			   }
		   }
	   }

	conflict.clear();


	assert(decisionLevel()==track_min_level || (super_units.size() && decisionLevel()==1 && track_min_level == 0));
	if(track_min_level<initial_level){
	 	int global_lev  = super_levels[decisionLevel()].decision_level;
		super->cancelUntil(global_lev);
		assert(to_propagate.size()==0 || global_lev==0);
		subqhead=to_propagate.size();
	}

	assert(track_min_trail<=startTrail);
	   for(int i = track_min_trail;i<trail.size();i++){
		   Lit a = trail[i];

		   if (super_interface(trail[i])){
			   Lit sub = trail[i];
			   int lev = level(var(sub));
			   Lit super_lit = toSuper(trail[i]);
			   if(super->value(super_lit)==l_True){
				   if(super->level(var(super_lit))>super_level){
					   super->cancelUntil(super_level);
					   super->enqueue(super_lit, super->solver_cref[subSolverNum]);
				   }
				   //do nothing
			   }else if (super->value(super_lit)==l_Undef){
				   assert(dbg_checkValidSuperReason(trail[i], super_lit));
				   if(super->decisionLevel()>super_level){
					   super->cancelUntil(super_level);
				   }
				   super->enqueue(super_lit, super->solver_cref[subSolverNum]);

			   }else{
				   if(super->decisionLevel()>super_level){
					   super->cancelUntil(super_level);
				   }
				   if (super->value(super_lit)==l_Undef){
					   super->enqueue(super_lit, super->solver_cref[subSolverNum]);
				   }else{
					   assert(super->value(super_lit)==l_False);
					   //this is a conflict -- but this can't occur, because the lit would have been propped here already
					   //actually, it CAN occur, if we are not allowing units from the super solver to be learnt, and we backtracked to level 0
					   assert(!allow_super_units); assert(decisionLevel()==0); assert(super->decisionLevel()==0);
					   conflict.push(super_lit);
						 assert(dbg_proveLearntSuperClause(conflict));
						 if(super || (subSolvers.size() && ! opt_first_solver_local_restarts))
						            	global_conflictC++;
					   return false;
				  }
			   }
		   }
	   }
	   assert(okay());



	}while(okay() && (subqhead<to_propagate.size()  || ( super_units.size()>0 && decisionLevel()==0)));



#ifndef NDEBUG
	if(eager_propagation && ! disableSubProp && ! sub_needs_refresh && track_min_level >= initial_level ){
		for(int i = 0;i<super->trail.size();i++){
			Lit superLit = super->trail[i];
			if(super->sub_interface(superLit) && super->level(var(superLit))<=super_level){
				Lit subLit = fromSuper(superLit);
				lbool val = value(subLit);
				assert(value(subLit)==l_True);
				if(super->level(var(superLit))==0 && !allow_super_units){
					super->printTrail();
					assert(super_units.contains(subLit) || ((value(subLit)==l_True && level(var(subLit))==0)));
					if(!(super_units.contains(subLit) || ((value(subLit)==l_True && level(var(subLit))==0)))){
						exit(5);
					}
				}
			}
		}
	}
#endif

	 return true;
}

/*_________________________________________________________________________________________________
|
|  propagate : [void]  ->  [Clause*]
|  
|  Description:
|    Propagates all enqueued facts. If a conflict arises, the conflicting clause is returned,
|    otherwise CRef_Undef.
|  
|    Post-conditions:
|      * the propagation queue is empty, even if there was a conflict.
|________________________________________________________________________________________________@*/
CRef Solver::propagate()
{

	//Note: should change this to enqueue directly in subsolvers when handling propagation

    CRef    confl     = CRef_Undef;
    int     num_props = 0;
    int round =0;
    watches.cleanAll();
    do{
    	round++;
		while (qhead < trail.size()){
			assert(confl     == CRef_Undef);
			Lit            p   = trail[qhead++];     // 'p' is enqueued fact to propagate.
			assert(var(p)<nVars());
			vec<Watcher>&  ws  = watches[p];
			Watcher        *i, *j, *end;
			num_props++;

			for (i = j = (Watcher*)ws, end = i + ws.size();  i != end;){
				// Try to avoid inspecting the clause:
				Lit blocker = i->blocker;
				if (value(blocker) == l_True){
					*j++ = *i++; continue; }

				// Make sure the false literal is data[1]:
				CRef     cr        = i->cref;
				Clause&  c         = ca[cr];
				assert(dbg_proveClause(c));
				Lit      false_lit = ~p;
				if (c[0] == false_lit)
					c[0] = c[1], c[1] = false_lit;
				assert(c[1] == false_lit);
				i++;

				// If 0th watch is true, then clause is already satisfied.
				Lit     first = c[0];
				Watcher w     = Watcher(cr, first);
				if (first != blocker && value(first) == l_True){
					*j++ = w; continue; }

				// Look for new watch:
				for (int k = 2; k < c.size(); k++)
					if (value(c[k]) != l_False){
						c[1] = c[k]; c[k] = false_lit;
						watches[~c[1]].push(w);
						goto NextClause; }

				// Did not find watch -- clause is unit under assignment:
				*j++ = w;
				if (value(first) == l_False){
					confl = cr;
					qhead = trail.size();
					 if(super || (subSolvers.size() && ! opt_first_solver_local_restarts))
					            	global_conflictC++;
					// Copy the remaining watches:
					while (i < end)
						*j++ = *i++;
				}else
					uncheckedEnqueue(first, cr);

			NextClause:;
			}
			ws.shrink(i - j);

			if(sub_interface(p)){
			   	subtrail.push(p);
			   	forced_inputs++;
			}
		}

		assert(qhead == trail.size() );
		if(ok && !disableSubProp &&  subSolvers.size() && confl==CRef_Undef)
			confl= propSubSolvers();

    }while (ok && ( confl == CRef_Undef) && qhead < trail.size());

	propagations += num_props;
		simpDB_props -= num_props;

    return confl;
}

CRef Solver::buildSubReason(Lit p, int solver){
	Solver * subSolver = subSolvers[solver];
	assert(subSolver);
	Lit localP =subSolver->fromSuper(p);

	assert(subSolver->okay());
	assert(value(p)==l_True);
	assert(subSolver->value(localP)!=l_Undef);
	assert(subSolver->value(localP)==l_True);

	tmp_reason_vec.clear();
	subSolver->analyzeFinal(localP, tmp_reason_vec);
	assert(dbg_proveLearntSubClause(tmp_reason_vec));
	assert(tmp_reason_vec[0]==localP);
assert(int_sub_reasons>0);

	if(int_sub_reasons>0){

		{
			int lit_k = -1;

			for(int i = 0;i<tmp_reason_vec.size();i++){
					Lit l = subSolver->toSuper(tmp_reason_vec[i]);
					tmp_reason_vec[i] = l;
					if(var(l)==var(p)){
						lit_k=i;
						break;
					}
			}
			assert(lit_k>=0);
			if(lit_k >0){
				Lit t = tmp_reason_vec[0];
				tmp_reason_vec[0] = tmp_reason_vec[lit_k];
				tmp_reason_vec[lit_k]=t;
			}
		}

		//we WONT have to do this for the L^ over approximation - it can safely use the tmp reason vector, if we wish.
		int highest_level_k = 1;
		int highest_level = 0;
		for(int i = 1;i<tmp_reason_vec.size();i++){
				Lit l = subSolver->toSuper(tmp_reason_vec[i]);
				tmp_reason_vec[i] = l;

				if(i> 0 && level(var(l))>highest_level){
					assert(value(l)==l_False);
					highest_level = level(var(l));
					highest_level_k = i;
				}
				assert(var(tmp_reason_vec[i]) < nVars());
		}

		assert(tmp_reason_vec.size()>1);



		//re-arrange the watchers
		assert(var(tmp_reason_vec[0])==var(p));
		Lit t = tmp_reason_vec[1];
		tmp_reason_vec[1]=tmp_reason_vec[highest_level_k];
		tmp_reason_vec[highest_level_k]=t;

		assert(dbg_proveLearntClause(tmp_reason_vec));

		CRef cr = ca.alloc(tmp_reason_vec, int_clauses==1, opt_subsume_interpolant);

		attachClause(cr);

		if(store_interpolant)
			addToInterpolant(cr);
		else
			clauses.push(cr);

		if(opt_add_global_unit_prop_clauses_to_q && this->cubes && subSolvers.size() && super){
			static vec<Var> seen_latch;

			seen_latch.growTo(sub_ninterface+1);
			TCube tc(this->frame+1);
			for(int i = 0;i<tmp_reason_vec.size();i++){
				tc.assignment->push(~tmp_reason_vec[i]);//invert to convert the clause into a cube
				seen_latch[var(tmp_reason_vec[i])]=true;
			}
			for(int i = 1;i<=sub_ninterface;i++){
				assert(sub_interface(i));
				if(!seen_latch[i]){
					tc.assignment->push(mkLit(i,true));//add lits from the reset state to the assignment. This is safe, because we know the assignment must already have one lit that is outside of the reset state already
				}else{
					seen_latch[i]=false;
				}

			}

		}

		return cr;

	}else{
			if( max_tmp_reason<tmp_reason_vec.size()){
				if(max_tmp_reason>0)
					ca.free(tmp_reason);
				tmp_reason= ca.alloc(tmp_reason_vec, false);
				max_tmp_reason= tmp_reason_vec.size();
			}
			Clause & c = ca[tmp_reason];
			for(int i = 0;i<tmp_reason_vec.size();i++){
				c[i]=  subSolver->toSuper(tmp_reason_vec[i]);
				assert(var(c[i]) < nVars());
				/*if(store_interpolant){
					interpolant.push(c[i]);
				}*/
			}
		/*	if(store_interpolant){
				interpolant.push(lit_Undef);
				interpolant_size++;interpolant_increase++;
			}*/
			assert(c[0]==p);
			c.setSize(tmp_reason_vec.size());
			assert(dbg_proveClause(c));
			return tmp_reason;
		}
}

CRef Solver::buildConflict(vec<Lit> & from){

	if( max_tmp_confl<from.size()){
		if(max_tmp_confl>0)
			ca.free(tmp_confl);
		tmp_confl= ca.alloc(from, false);
		max_tmp_confl= from.size();
	}
	Clause & c = ca[tmp_confl];
	for(int i = 0;i<from.size();i++){
		c[i]= from[i];
		Lit l = c[i];
		Var vl = var(l);
		assert(var(c[i])<nVars());
	}
	c.setSize(from.size());
	return tmp_confl;

}
 CRef Solver::solveSubSolvers(){
	 if(sub_needs_refresh){
	 		assert(decisionLevel()==0);
	 		subtrail.clear();
	 		for(int i = level_q;i<trail.size();i++){
	 				if(sub_interface(trail[i]))
	 					subtrail.push(trail[i]);
	 			}
	 		sub_needs_refresh=false;

	 	}
	local_conflict.clear();
	for(int subSolverNum = 0;subSolverNum<subSolvers.size();subSolverNum++){
		Solver * subSolver = subSolvers[subSolverNum];
		if(! subSolver->allow_unforced_subsearch && opt_subsearch_allow_after_interpolant && subSolver->interpolant_increase > opt_eager_prop_break){
			subSolver->allow_unforced_subsearch=true;
		}
		if((opt_subsearch==dont_subsearch_after_backtrack) &&  (track_min_level< initial_level && !subSolver->allow_unforced_subsearch))
			continue;

		if (opt_subsearch==dont_subsearch_past_interpolant_after_backtrack && (!subSolver->allow_unforced_subsearch && has_any_super_backtracked)){
			continue;
		 }

#ifndef NDEBUG
	if(subSolver->eager_propagation){
	for(int i = 0;i<sub_qhead[subSolverNum];i++){
		if (sub_interface(subtrail[i] )){
			Lit l =subtrail[i];
			assert(value(l)==l_True);
			Lit sub =  subSolver->fromSuper(l);
			assert(subSolver->value(sub)==l_True);
			int lev = level(var(l));
			int sublev = subSolver->level(var(sub));
			if(sublev){
				assert(subSolver->super_levels.size()>=sublev);
				assert(subSolver->super_levels[sublev].decision_level==level(var(l)));
			}else{
				assert(level(var(l))==0);
			}
		}
	}
	}
#endif

	assert(sub_qhead[subSolverNum]==subtrail.size());
	if(subSolver->solveTheory(subSolverNum,trail, local_conflict,has_any_super_backtracked,cubes)){
#ifndef NDEBUG
			if(decisionLevel()==0){
				//add units to interpolant
				for(int i = qhead;i<trail.size();i++){
					assert(dbg_proveUnit(trail[i]));
				}
			}
#endif
			sub_qhead[subSolverNum]=subtrail.size();

			subSolver->succesful_solve=!globalRestartTriggered;

		if(store_interpolant){
			if(decisionLevel()==0){
				//add units to interpolant
				for(int i = qhead;i<trail.size();i++)
					addToInterpolant(trail[i]);
			}
		}
		assert(subSolver->okay());
		if(globalRestartTriggered){
			cancelUntil(0);
			return CRef_Undef;
		}
	}else{
		subSolver->attempted_solve=true;
		subSolver->must_cancel=-1;
		if(!subSolver->okay()){
			ok=false;
			return CRef_Undef;
		}
		assert(local_conflict.size());
		if(value(local_conflict[0])==l_False){
#ifndef NDEBUG
			int maxLev = level(var(local_conflict[0]));
			for(int i = 1;i<local_conflict.size();i++){
				assert(value(local_conflict[i])==l_False);
				int lev = level(var(local_conflict[i]));
				assert(lev<=maxLev);
			}
#endif
			cancelUntil(level(var(local_conflict[0])));
		}else{
			//assert(opt_subsearch==2);//we have already backtracked past the generated conflict
		}
		assert(dbg_proveLearntClause(local_conflict));
		sub_qhead[subSolverNum]=subtrail.size();
#ifndef NDEBUG
			if(decisionLevel()==0){
				//add units to interpolant
				for(int i = qhead;i<trail.size();i++){
					assert(dbg_proveUnit(trail[i]));
				}
			}
#endif
			if(store_interpolant){
				if(decisionLevel()==0){
					//add units to interpolant
						for(int i = qhead;i<trail.size();i++)
								addToInterpolant(trail[i]);

				}
		
			}

		if( local_conflict.size() == 1){
			//treat the interface conflcit as a conflict only
			if(value(local_conflict[0])!=l_False){
				assert(opt_subsearch==2);
				return CRef_Undef;//this is _not_ a conflict because the subsolver forced us to backtrack past it. (Only currently possible if subsearch=2)
			}
			if(store_interpolant ){
				addToInterpolant(local_conflict[0]);//this cancels
				assert(decisionLevel()==0);
				return CRef_Undef;
			}else
				return buildConflict(local_conflict);
		}else{
			//Treat the conflict as either a learnt or permanent clause (units are handled above, and will get turned into permanent clauses regardless)
			CRef cr = ca.alloc(local_conflict, int_clauses==1, opt_subsume_interpolant);
			attachClause(cr);
		
			if(store_interpolant){
				addToInterpolant(cr);
			}else if(int_clauses==1){
				learnts.push(cr);
				claBumpActivity(ca[cr]);
			}else{
				clauses.push(cr);
			}
			if(value(local_conflict[0])!=l_False){
				assert(opt_subsearch==2);
				return CRef_Undef;//this is _not_ a conflict because the subsolver forced us to backtrack past it. (Only currently possible if subsearch=2)
			}
			return cr;
		}
		}
	}

		return CRef_Undef;
}

 CRef Solver::prepare_eagerProp(int subSolverNum){
	 if(subSolverNum>=subSolvers.size())
		 return CRef_Undef;

	 Solver * subSolver = subSolvers[subSolverNum];
	 if(subSolver->eager_propagation){
				 assert(false);
				 return CRef_Undef;
			 }
	 vec<Lit> eager_trail;
	 subSolver->must_cancel=-1;
	subSolver->cancelUntil(0);
	subSolver->eager_propagation=true;
	int pos =0;
	for(int lev = 0;lev<=decisionLevel();lev++){
		eager_trail.clear();
		Lit decisionLit = lit_Undef;
		if(lev>0){
			decisionLit=trail[trail_lim[lev-1]];
		}
		for(;pos< (lev<decisionLevel()? trail_lim[lev]: trail.size());pos++){
			assert(level(var(trail[pos]))==lev);
			if(sub_interface(trail[pos])){
				eager_trail.push(trail[pos]);
			}
		}
		if(eager_trail.size() || lev==0){
			if(subSolver->propagateTheory(subSolverNum,lev,decisionLit,eager_trail,0, local_conflict)){
			#ifndef NDEBUG
						if(decisionLevel()==0){
							//add units to interpolant
							for(int i = qhead;i<trail.size();i++){
								assert(dbg_proveUnit(trail[i]));
							}
						}
			#endif

						if(store_interpolant){
							if(decisionLevel()==0){
								//add units to interpolant
								for(int i = qhead;i<trail.size();i++){
									addToInterpolant(trail[i]);
								}

							}
						}

						assert(subSolver->okay());
						assert(local_conflict.size()==0);
					}else{

						cancelUntil(lev);

						if(!subSolver->okay()){
							ok=false;
							return CRef_Undef;
						}
						assert(local_conflict.size());
				#ifndef NDEBUG
							int maxLev = level(var(local_conflict[0]));
							assert(decisionLevel()>=maxLev);
							for(int i = 1;i<local_conflict.size();i++){
								assert(sub_interface(local_conflict[i]));
								assert(value(local_conflict[i])==l_False);
								int lev = level(var(local_conflict[i]));
								assert(lev<=maxLev);
							}
				#endif


						assert(dbg_proveLearntClause(local_conflict));
						cancelUntil(level(var(local_conflict[0])));
						assert(dbg_conflicting(local_conflict));
			#ifndef NDEBUG
						if(decisionLevel()==0){
							//add units to interpolant
							for(int i = qhead;i<trail.size();i++){
								assert(dbg_proveUnit(trail[i]));
							}
						}
			#endif

						if(store_interpolant){
							if(decisionLevel()==0){
								//add units to interpolant
								for(int i = qhead;i<trail.size();i++)
									addToInterpolant(trail[i]);

							}

						}

						if(local_conflict.size() == 1){
							//treat the interface conflcit as a conflict only
							addToInterpolant(local_conflict[0]);
							return buildConflict(local_conflict);
						}else{
							//Treat the conflict as either a learnt or permanent clause (units are handled above, and will get turned into permanent clauses regardless)

							CRef cr = ca.alloc(local_conflict, int_clauses==1,opt_subsume_interpolant);
							attachClause(cr);

							if(store_interpolant){
								addToInterpolant(cr);
							}else if(int_clauses==1){
								learnts.push(cr);
								claBumpActivity(ca[cr]);
							}else{
								clauses.push(cr);
							}
							return cr;
						}
					}
		}
	}
	 return CRef_Undef;
 }

inline CRef Solver::propSubSolvers(){

if(sub_needs_refresh){
		assert(decisionLevel()==0);

		subtrail.clear();
		for(int i = level_q;i<trail.size();i++){
			if(sub_interface(trail[i]))
				subtrail.push(trail[i]);
		}

		sub_needs_refresh=false;

	}
	local_conflict.clear();
	static int total_it = 0;
	for(int subSolverNum = 0;subSolverNum<subSolvers.size() && qhead==trail.size();subSolverNum++){
		Solver * subSolver = subSolvers[subSolverNum];


#ifndef NDEBUG
	if(subSolver->eager_propagation){
	if(decisionLevel()>0){
		    for(int i = 0;i<nVars();i++){
				if (sub_interface(i)){
					if(value(i)==l_Undef){
						Lit l = mkLit(i, true);
						Lit sub =  subSolver->fromSuper(l);
						int sublev = subSolver->level(var(sub));
						lbool subval = subSolver->value(sub);
						int superlev = subSolver->super_levels[ subSolver->level(var(sub))].decision_level;
						assert( subSolver->value(sub)==l_Undef || (subSolvers[subSolverNum]->must_cancel >=0 && subSolvers[subSolverNum]->must_cancel < subSolver->super_levels[ subSolver->level(var(sub))].decision_level ));
				}
			}
	    }
	}
	}
#endif

	if(! subSolver->eager_propagation && (opt_eager_prop==4 || opt_eager_prop==5) && (switch_eager || (!opt_switch_eager && subSolver->interpolant_increase>opt_eager_prop_break ) )){
		CRef confl = prepare_eagerProp(subSolverNum);
		if(confl!=CRef_Undef){
			return confl;
		}
		sub_qhead[subSolverNum]=subtrail.size();
	}

	Lit decisionLit = lit_Undef;
		if(decisionLevel()>0){
			decisionLit=trail[trail_lim[decisionLevel()-1]];
		}
	static vec<Lit> ignore;
	assert(ignore.size()==0);
	assert(subSolver);
	if(sub_qhead[subSolverNum]<subtrail.size() || decisionLevel()==0){ //prop at level 0 to capture any units in the sub solver
		if(subSolver->propagateTheory(subSolverNum,decisionLevel(),decisionLit, subSolver->eager_propagation ? subtrail:ignore,sub_qhead[subSolverNum], local_conflict)){
#ifndef NDEBUG
			if(decisionLevel()==0){
				//add units to interpolant
				for(int i = qhead;i<trail.size();i++){
					assert(dbg_proveUnit(trail[i]));
				}
			}
#endif
			if(decisionLevel()==0){
				level_q=trail.size();
			}
			sub_qhead[subSolverNum] = subtrail.size();
			if(store_interpolant){
							if(decisionLevel()==0){
								//add units to interpolant
								for(int i = qhead;i<trail.size();i++){
									Lit l = trail[i];
									assert(sub_interface(l));
									addToInterpolant(l);
								}

							}
			}

			assert(subSolver->okay());
			assert(local_conflict.size()==0);
		}else{

			if(!subSolver->okay()){
				ok=false;
				return CRef_Undef;
			}
			assert(local_conflict.size());
	#ifndef NDEBUG
				int maxLev = level(var(local_conflict[0]));
				assert(decisionLevel()>=maxLev);
				for(int i = 1;i<local_conflict.size();i++){
					assert(sub_interface(local_conflict[i]));
					assert(value(local_conflict[i])==l_False);
					int lev = level(var(local_conflict[i]));
					assert(lev<=maxLev);
				}
	#endif


			assert(dbg_proveLearntClause(local_conflict));
			cancelUntil(level(var(local_conflict[0])));
			assert(dbg_conflicting(local_conflict));
#ifndef NDEBUG
			if(decisionLevel()==0){
				//add units to interpolant
				for(int i = qhead;i<trail.size();i++){
					assert(dbg_proveUnit(trail[i]));
				}
			}
#endif

			if(store_interpolant){
				if(decisionLevel()==0){
					//add units to interpolant
					for(int i = qhead;i<trail.size();i++){
						addToInterpolant(trail[i]);
					}

				}

			}
			sub_qhead[subSolverNum]=subtrail.size();
			if(local_conflict.size() == 1){
				//treat the interface conflcit as a conflict only
				addToInterpolant(local_conflict[0]);
				return buildConflict(local_conflict);
			}else{
				//Treat the conflict as either a learnt or permanent clause (units are handled above, and will get turned into permanent clauses regardless)
			//	bool r = opt_local_simplify_int && simplifyInterpolant(local_conflict);
				CRef cr = ca.alloc(local_conflict, int_clauses==1, opt_subsume_interpolant);
				attachClause(cr);

				if(store_interpolant){
					addToInterpolant(cr);
				}else if(int_clauses==1){
					learnts.push(cr);
					claBumpActivity(ca[cr]);
				}else{
					clauses.push(cr);
				}
				return cr;
			}
		}
	}

#ifndef NDEBUG

	for(int i = 0;i<sub_qhead[subSolverNum];i++){
		if(subSolver->eager_propagation){
			if (sub_interface(subtrail[i]  )){
				Lit l =subtrail[i];
				assert(value(l)==l_True);
				Lit sub = subSolver->fromSuper(l);
				assert(subSolver->value(sub)==l_True);
				int lev = level(var(l));
				int sublev = subSolver->level(var(sub));
				if(sublev){
					assert(subSolver->super_levels.size()>=sublev);
					assert(subSolver->super_levels[sublev].decision_level==level(var(l)));
				}else{
					assert(level(var(l))==0);
				}
			}
		}
	}

#endif
	}
#ifndef NDEBUG

	if( qhead==trail.size()){
		for(int subSolverNum = 0;subSolverNum<subSolvers.size();subSolverNum++){
			Solver * subSolver = subSolvers[subSolverNum];
			if(subSolver->eager_propagation){
				for(int i = 0;i<trail.size();i++){
					if (sub_interface(trail[i] )){
						Lit l =trail[i];
						assert(value(l)==l_True);
						Lit sub = subSolver->fromSuper(l);
						int lev = level(var(l));
						assert(subSolver->value(sub)==l_True);

						int sublev = subSolver->level(var(sub));
						if(sublev){
							assert(subSolver->super_levels.size()>=sublev);
							assert(subSolver->super_levels[sublev].decision_level==level(var(l)));
						}else{
							assert(level(var(l))==0);
						}
					}
				}
			}
		}
	}



#endif
	return CRef_Undef;
}
/*_________________________________________________________________________________________________
|
|  reduceDB : ()  ->  [void]
|  
|  Description:
|    Remove half of the learnt clauses, minus the clauses locked by the current assignment. Locked
|    clauses are clauses that are reason to some assignment. Binary clauses are never removed.
|________________________________________________________________________________________________@*/
struct reduceDB_lt { 
    ClauseAllocator& ca;
    reduceDB_lt(ClauseAllocator& ca_) : ca(ca_) {}
    bool operator () (CRef x, CRef y) { 
        return ca[x].size() > 2 && (ca[y].size() == 2 || ca[x].activity() < ca[y].activity()); } 
};
void Solver::reduceDB()
{
    int     i, j;
    double  extra_lim = cla_inc / learnts.size();    // Remove any clause below this activity

    sort(learnts, reduceDB_lt(ca));
    // Don't delete binary or locked clauses. From the rest, delete clauses from the first half
    // and clauses with activity smaller than 'extra_lim':
    for (i = j = 0; i < learnts.size(); i++){
        Clause& c = ca[learnts[i]];
        if (c.size() > 2 && !locked(c) && (i < learnts.size() / 2 || c.activity() < extra_lim))
            removeClause(learnts[i]);
        else
            learnts[j++] = learnts[i];
    }
    learnts.shrink(i - j);

    checkGarbage();
}


void Solver::removeSatisfied(vec<CRef>& cs)
{
    int i, j;
    for (i = j = 0; i < cs.size(); i++){
        Clause& c = ca[cs[i]];
        if (satisfied(c))
            removeClause(cs[i]);
        else
            cs[j++] = cs[i];
    }
    cs.shrink(i - j);
}

void Solver::removeSatisfied(vec<LRef>& cs)
{
    int i, j;
    for (i = j = 0; i < cs.size(); i++){
    	if(!cs[i].isLit()){
        Clause& c = ca[cs[i].cref()];
        if (satisfied(c)){
        	 c.mark(1);
        	 ca.free(cs[i].cref());
        }else
            cs[j++] = cs[i];
    	}else{
    		cs[j++] = cs[i];
    	}
    }
    cs.shrink(i - j);
}


void Solver::rebuildOrderHeap()
{
    vec<Var> vs;
    for (Var v = 0; v < nVars(); v++)
        if (decision[v] && value(v) == l_Undef)
            vs.push(v);
    order_heap.build(vs);
}

/*_________________________________________________________________________________________________
|
|  simplify : [void]  ->  [bool]
|  
|  Description:
|    Simplify the clause database according to the current top-level assigment. Currently, the only
|    thing done here is the removal of satisfied clauses, but more things can be put here.
|________________________________________________________________________________________________@*/
bool Solver::simplify(bool force)
{
	assert(decisionLevel()==0);
	/* if(disable_simplification)
	    	return true;*/
    if (!ok) //||  propagate() != CRef_Undef)
        return ok = false;

    if (!force && ( nAssigns() == simpDB_assigns || (simpDB_props > 0)))
        return true;

    // Remove satisfied clauses:
    removeSatisfied(learnts);
    if (remove_satisfied)        // Can be turned off.
        removeSatisfied(clauses);

    if(force){
     	garbageCollect();
     }else
    	checkGarbage();
    rebuildOrderHeap();

    simpDB_assigns = nAssigns();
    simpDB_props   = clauses_literals + learnts_literals;   // (shouldn't depend on stats really, but it will do for now)

    return true;
}




/*
  Finite subsequences of the Luby-sequence:

  0: 1
  1: 1 1 2
  2: 1 1 2 1 1 2 4
  3: 1 1 2 1 1 2 4 1 1 2 1 1 2 4 8
  ...


 */

static double luby(double y, int x){

    // Find the finite subsequence that contains index 'x', and the
    // size of that subsequence:
    int size, seq;
    for (size = 1, seq = 0; size < x+1; seq++, size = 2*size+1);

    while (size-1 != x){
        size = (size-1)>>1;
        seq--;
        x = x % size;
    }

    return pow(y, seq);
}

/*_________________________________________________________________________________________________
|
|  search : (nof_conflicts : int) (params : const SearchParams&)  ->  [lbool]
|  
|  Description:
|    Search for a model the specified number of conflicts. 
|    NOTE! Use negative value for 'nof_conflicts' indicate infinity.
|  
|  Output:
|    'l_True' if a partial assigment that is consistent with respect to the clauseset is found. If
|    all variables are decision variables, this means that the clause set is satisfiable. 'l_False'
|    if the clause set is unsatisfiable. 'l_Undef' if the bound on number of conflicts is reached.
|________________________________________________________________________________________________@*/
lbool Solver::search(int nof_conflicts,  int & start_trail)
{

    assert(ok);
    int         backtrack_level;
    int         conflictC = 0;
    vec<Lit>    learnt_clause;
    starts++;

    int local_it = 0;
    for (;;){

    	CRef confl = propagate();
    	process:
    	if(!ok){
    		return l_False;
    	}

        if (confl != CRef_Undef){
            // CONFLICT
            conflicts++; conflictC++;


            if (decisionLevel() == 0){
            	ok=false;
            	return l_False;
            }
            learnt_clause.clear();

            if (!analyze(confl, learnt_clause, backtrack_level)){
            	assert(decisionLevel()<=track_min_level);
				 analyzeFinalSuper(confl,lit_Undef,super_conflict);
				 assert(dbg_proveLearntSuperClause(super_conflict));
				 assert(super_conflict.size());
				 varDecayActivity();
				 claDecayActivity();
				 track_min_level=min(track_min_level,decisionLevel());
			     start_trail= min(start_trail,qhead);
				 return l_False;
            }

            cancelUntil(backtrack_level);

            start_trail= min(start_trail,qhead);
            if (learnt_clause.size() == 1){
                uncheckedEnqueue(learnt_clause[0]);
            }else{
                CRef cr = ca.alloc(learnt_clause, true);
                learnts.push(cr);
                attachClause(cr);
                claBumpActivity(ca[cr]);
                uncheckedEnqueue(learnt_clause[0], cr);
            }

            varDecayActivity();
            claDecayActivity();

            if(opt_semi_restart && backtrack_level<initial_level&& assumptions.size()){
            	cancelUntil(0);
            }


            if (--learntsize_adjust_cnt == 0){
                learntsize_adjust_confl *= learntsize_adjust_inc;
                learntsize_adjust_cnt    = (int)learntsize_adjust_confl;
                max_learnts             *= learntsize_inc;

                if (verbosity >= 1)
                    printf("| %9d | %7d %8d %8d | %8d %8d %6.0f | %6.3f %% |\n", 
                           (int)conflicts, 
                           (int)dec_vars - (trail_lim.size() == 0 ? trail.size() : trail_lim[0]), nClauses(), (int)clauses_literals, 
                           (int)max_learnts, nLearnts(), (double)learnts_literals/nLearnts(), progressEstimate()*100);
            }

        }else{
        	assert(!subSolvers.size() || !subSolvers[0]->eager_propagation || disableSubProp || dbg_checkInterfaceMatches());
        	assert(dbg_proveUnitPropComplete(decisionLevel()));

#ifndef NDEBUG
        	if(subSolvers.size() && subSolvers[0]->eager_propagation && ! disableSubProp){
				for(int i = 0;i<trail.size();i++){
					Lit l = trail[i];
					int lev = level(var(l));
					if(sub_interface(l)){
						Solver * subSolver = subSolvers[0];
						Lit sub = subSolver->fromSuper(l);
						assert(subSolver->value(sub)==l_True);
						int sub_lev = subSolver->level(var(sub));
						int sub_super = subSolver->super_levels[sub_lev].decision_level;
						assert(sub_super==lev);
						if(lev==0){
							assert(sub_lev==0 || (sub_lev==1 && subSolver->super_units.contains(sub)));
						}
					}
				}
        	}
#endif
        	 // NO CONFLICT
            if(opt_global_restarts && (super || (subSolvers.size() && ! opt_first_solver_local_restarts))){
            	if (nof_global_conflicts > 0 && global_conflictC >= nof_global_conflicts){
					// Reached bound on number of conflicts:
					progress_estimate = progressEstimate();
					cancelUntil(0);
					globalRestartTriggered=true;
					return super?l_True:l_Undef;
            	}
		   }
            assert(!globalRestartTriggered);

            if(opt_subsearch==eager_quit &&  decisionLevel()< initial_level){
            	return l_True;
             }

            if(!(super || subSolvers.size() ) || opt_local_restarts || (opt_first_solver_local_restarts && ! super ) ){
                if ((decisionLevel()>track_min_level) && ( nof_conflicts >= 0 && conflictC >= nof_conflicts || !withinBudget())){
                    // Reached bound on number of conflicts:
                    progress_estimate = progressEstimate();
                    cancelUntil(track_min_level);
                    return l_Undef; }
            }
            // Simplify the set of problem clauses:
            if (decisionLevel() == 0 && !simplify()){
                return l_False;
            }
            if (learnts.size()-nAssigns() >= max_learnts)
                // Reduce the set of learnt clauses:
                reduceDB();

            Lit next = lit_Undef;
            while (decisionLevel() < assumptions.size()){
                // Perform user provided assumption:
                Lit p = assumptions[decisionLevel()]; //Note: because super propagated literals are packed at least one to a sub-level, and sometimes more, and assumptions are stored in super trail order, the decision level is always <= to the level of the next assumption that was backtracked past, so this is safe.
                assert(var(p)<nVars());
                if (p==lit_Undef || value(p) == l_True){
                    // Dummy decision level:
                    newDecisionLevel();
                }else if (value(p) == l_False){

                    analyzeFinal(~p, conflict);
                    if(opt_only_ternary_conflict){
						for(int i = 0;i<assumptions.size();i++){
							if(assumptions[i]!=lit_Undef)
								seen[var(assumptions[i])]=true;
						}
						int lowest = -1;
						for(int i = 0;i<conflict.size();i++){
							if (!seen[var(conflict[i])]){
								int lev = level(var(conflict[i]));
								if(lev<lowest || lowest<0)
									lowest = lev;
							}
						}
						if(lowest==0){
							lowest=1;//shouldn't happen
						}
						for(int i = 0;i<assumptions.size();i++){
							if(assumptions[i]!=lit_Undef)
								seen[var(assumptions[i])]=false;
						}
						if(lowest>=0){
							conflict.clear();
							cancelUntil(lowest-1);
						}else{
							return l_False;
						}
                    }else{

                    	return l_False;
                    }
                }else{
                    next = p;
                    break;
                }
            }


            if (next == lit_Undef){
                // New variable decision:
                decisions++;

                next = pickBranchLit();
                assert(var(next)<nVars());

                if (next == lit_Undef){

#ifndef NDEBUG
                	for(int i = 0;i<assumptions.size();i++){
                		if(assumptions[i]!=lit_Undef)
                			assert(value(assumptions[i])==l_True);
                	}
#endif

                	 if(ok  && !disableSubSolve && subSolvers.size()  && confl==CRef_Undef   && qhead == trail.size() && nAssigns()==nVars()){
                				 has_any_super_backtracked|=track_min_level<initial_level;

						confl= solveSubSolvers();

						if(confl!=CRef_Undef){
							goto process;
						}else if ( nAssigns()<nVars() || qhead < trail.size()){
							confl=propagate();
							goto process;
						}

					 }

                	assert(dbg_proveUnitPropComplete(decisionLevel()));


#ifndef NDEBUG
                	if(!super || opt_subsearch<3){
						assert(disableSubSolve || dbg_subAssignments());
						assert(disableSubSolve|| dbg_proveAssignment());
                	}

                	for(int i = 0;i<assumptions.size();i++)
                		assert(value(assumptions[i])==l_True);

#endif
                    // Model found:
                    return l_True;
                }
            }

            // Increase decision level and enqueue 'next'
            newDecisionLevel();
            uncheckedEnqueue(next);
        }
    }
}


double Solver::progressEstimate() const
{
    double  progress = 0;
    double  F = 1.0 / nVars();

    for (int i = 0; i <= decisionLevel(); i++){
        int beg = i == 0 ? 0 : trail_lim[i - 1];
        int end = i == decisionLevel() ? trail.size() : trail_lim[i];
        progress += pow(F, i) * (end - beg);
    }

    return progress / nVars();
}

//Solve this solver under the assignment of its super solver.
//Note that this implementation is MUCH more complicated than it needs to be.
//For an efficient, simple, clean implementation, see ModSAT.
bool Solver::solveTheory(int subSolverNum, vec<Lit> & out_trail,vec<Lit> & conflict_out, bool any_super_backtracked, VHeap<TCube> * _cubes){
	has_any_super_backtracked=any_super_backtracked;

	 if (!ok)
		 return false;
	this->cubes=_cubes;
	 assumptions.clear();
#ifndef NDEBUG

		    dbg_super_int=super_min_interface;
		    dbg_super_off=super->sub_offset;
		    dbg_super = super;


#endif
	conflict_out.clear();

	if(!eager_propagation){
		cancelUntil(0);
		assumptions.clear();
		for(int i = 0;i<out_trail.size();i++){
			if(super->sub_interface(out_trail[i])){
				Lit p =fromSuper(out_trail[i]);
				assumptions.push(p);
			}
		}
	}else if (eager_propagation && opt_subsearch==force_assumption_quit){

			//if the super solver doesn't have its coi computed yet, compute it now.
			if(opt_coi && !super->has_coi)
				super->computeCOI();

			if(opt_ternary && !super->has_ternary)
				super->computeTernary();
			//assert(super->current_coi.size()==super->property_coi.size());

			if(opt_force_restart_subsolver==1){
				cancelUntil(0);
			}else if (opt_force_restart_subsolver==2 && (opt_coi || opt_ternary)){
				if(decisionLevel()>0){
					for(int i = trail_lim[0];i<trail.size();i++){
						Lit l = trail[i];
						Var v =toSuper(var(l));
						if (opt_ternary && !super->ternary_coi[v]){
							cancelUntil(level(var(l))-1);
							break;
						}else if (opt_coi &&  !super->current_coi[v]){
							cancelUntil(level(var(l))-1);
							break;
						}
					}
				}
			}

		assumptions.clear();
		for(int i = 0;i<out_trail.size();i++){
			Var v = var(out_trail[i]);
			if(super->sub_interface(v) && (opt_ternary ? super->ternary_coi[v]: (opt_coi ? super->current_coi[v] : true))  ){
				Lit p = fromSuper(out_trail[i]);
				while(value(p)!=l_Undef && assumptions.size()+1< level(var(p))){
					assumptions.push(lit_Undef);//to make the decision levels line up
				}
				assumptions.push(p);
			}
		}


	}

	int start_trail = qhead;
	track_min_trail = qhead;
	track_min_level= decisionLevel();

	 initial_level=decisionLevel();
	solves++;
	if(max_learnts<0){
		 max_learnts               = nClauses() * learntsize_factor;
			learntsize_adjust_confl   = learntsize_adjust_start_confl;
			learntsize_adjust_cnt     = (int)learntsize_adjust_confl;
	}
	    lbool   status        = l_Undef;

	    conflict.clear();
	    local_conflict.clear();
	    super_conflict.clear();

		// Search:
	   int curr_restarts =0;

		while (status == l_Undef){
			double rest_base = luby_restart ? luby(restart_inc, curr_restarts) : pow(restart_inc, curr_restarts);
			status = search(rest_base * restart_first,  start_trail);
			if (!withinBudget()) break;
			curr_restarts++;
		}



		assert(track_min_trail<=start_trail);
			assert(track_min_trail<=qhead);
		    if(status==l_True ){
		    	//ok: now to clean up, need to finish propagating anything here to the super solver
		    	conflict_out.clear();
		    	//if(frame==0){
					return true;
		    	//}
		    }else if(!eager_propagation || (eager_propagation && opt_subsearch==force_assumption_quit && ! super_conflict.size() && conflict.size())){

		    	assert(!ok || conflict.size()>0 || decisionLevel()<initial_level);
		    	conflict_out.clear();
		    	for(int i = 0;i<conflict.size();i++){
		        	conflict_out.push(toSuper(conflict[i]));

		    	}
		    	conflict.clear();
		    }else{

		    	assert(!ok || super_conflict.size()>0 || decisionLevel()<initial_level);
		    	super_conflict.copyTo(conflict_out);
		    }

		    if(conflict_out.size()){
		    	if(opt_print_cubes){
		    		static vec<Lit> c;
		    		conflict_out.copyTo(c);
		    		sort(c);
				printf("Blocked at %d: ", frame+1);
				for(int i = 0;i<c.size();i++){
					printf("%d ",dimacs( c[i]));
				}
				printf("\n");
			}
		    }
	    assert(status==l_True  ||conflict_out.size()>0 || decisionLevel()<initial_level || !ok);


	    if(conflict_out.size() && cubes  && (opt_keep_cubes ||super->frame<max_frame)){
	   			TCube s(super->frame+1);
	   			if(opt_subsearch==force_assumption_quit ){
	   				assert(assumptions.size());

						for(int i = 0;i<assumptions.size();i++){
							if(assumptions[i]!=lit_Undef)
								s.assignment->push(toSuper(assumptions[i]));
						}

	    		}else if(opt_coi || opt_ternary ){
	   				//if the super solver doesn't have its coi computed yet, compute it now.
	   				if(opt_coi && !super->has_coi)
						super->computeCOI();

	   				if(opt_ternary && !super->has_ternary)
	   					super->computeTernary();
	   				//assert(super->current_coi.size()==super->property_coi.size());


					for(int i = 0;i<super->trail.size();i++){
						Lit l = super->trail[i];
						if(super->sub_interface(l) ){
							total_assign_lits++;
							if(opt_ternary? super->ternary_coi[var(l)] : super->current_coi[var(l)]){
								s.assignment->push(l);
							}else{
								num_coi_blocked++;
							}
						}
					}


	   			}else{
					for(int i = 0;i<super->trail.size();i++){

						if(super->sub_interface( super->trail[i])){
							total_assign_lits++;
							s.assignment->push(super->trail[i]);
						}
					}
	   			}
	   			cubes->insert(s.frame,s);
	   	   }
	    this->cubes=NULL;

	    assumptions.clear();
		cancelUntil(track_min_level);
		assert(decisionLevel()<=initial_level);

		if(globalRestartTriggered){
			super->cancelUntil(0);
		}

		//super->cancelUntil(0);
		if( decisionLevel()<initial_level || opt_force_restart_subsolver ){
			int global_lev  = super_levels[decisionLevel()].decision_level;
			super->cancelUntil(global_lev);
		}

	    if(ok && (conflict_out.size()==0 || super->value(conflict_out[0])!=l_False )){
	    	//if we are reporting a conflict out, then the super solver will backtrack regardless, and so we don't need to enqueue anything here.
	    	for(int i = track_min_trail;i<trail.size();i++){
			   if (super_interface(trail[i])){
				   Lit super_lit = toSuper(trail[i]);
				   if(super->value(super_lit)==l_True){
					   //do nothing
				   }else if (super->value(super_lit)==l_Undef){
					   assert(decisionLevel()<=initial_level);//otherwise, all the super lits must already be assigned, or we wouldn't be in this code
					   super->enqueue(super_lit, super->solver_cref[subSolverNum]);
				   }else{
					   assert(super->value(super_lit)==l_False);
					   conflict_out.clear();
					   //this is a conflict -- but this can't occur, because the lit would have been propped here already
					   //actually, it CAN occur, if we are not allowing units from the super solver to be learnt, and we backtracked to level 0
					   assert(!allow_super_units); assert(decisionLevel()==0); assert(super->decisionLevel()==0);
					   conflict_out.push(super_lit);
						 assert(dbg_proveLearntSuperClause(conflict_out));
						 if(super || (subSolvers.size() && ! opt_first_solver_local_restarts))
						            	global_conflictC++;
					   return false;
				   }
			   }
		   }
	    }
	if(!ok)
		return false;
	return status!=l_False;
}

bool 	Solver::solve(const vec<Lit>& assumps,vec<Lit>& cube, VHeap<TCube> *Q){
	cube.clear();
	this->cubes=Q;
	cancelUntil(0);
	budgetOff();

	assumps.copyTo(assumptions);
	if(solve_()==l_True){
		for(int i = 1;i<=sub_ninterface;i++){
			if(sub_interface(i))
				cube.push(mkLit(i , model[i]==l_False));
		}

		cancelUntil(0);
		this->cubes=NULL;
		return true;
	}else{
		conflict.copyTo(cube);
		cancelUntil(0);
		this->cubes=NULL;
		return false;
	}
}

//Specialized procedure for generalizing cubes. Adds the cube temporarily to the input latches of the circuit
bool Solver::solveRelative(vec<Lit> & assumps, vec<Lit> & cube, Lit activation_lit, int offset){
		cube.clear();
		assert(interpolant.size()==0);
		bool ds = disableSubSolve;
		bool dp = disableSubProp;
		bool dsimp = disable_simplification;
		disable_simplification=true;
		disableSubSolve=true;
		disableSubProp=true;//disable the sub solvers temporarily
		cancelUntil(0);
		budgetOff();

		assumptions.clear();
		for(int i = 0;i<assumps.size();i++){
			if(assumps[i]!=lit_Undef){
				assumptions.push(assumps[i]);
			}
		}
		static vec<Lit> not_s;


		if(activation_lit!=lit_Undef){
			assert(frame>0);
			not_s.clear();
			for(int i = 0;i<assumps.size();i++){
				Lit out = assumps[i];
				if(out!=lit_Undef){
					Lit in = mkLit(var(out)-offset,sign(out));
					not_s.push(~in);
				}
			}
			//ok, now requisition an activation literal to add to the clause, so we can disable it later.

			not_s.push(activation_lit);
			addClause(not_s);
			assumptions.push(~activation_lit);
		}

		if(solve_()==l_True){
			disableSubProp=dp;
			disableSubSolve=ds;

				disable_simplification=dsimp;
			cancelUntil(0);
			assert(interpolant.size()==0);
			return true;
		}else{

				for(int i = 0;i<conflict.size();i++){
					seen[var(conflict[i])]=true;
				}
				if(activation_lit!=lit_Undef)
					seen[var(activation_lit)]=false;

				for(int i = 0;i<assumps.size();i++){


					if(assumps[i]!=lit_Undef && seen[var(assumps[i])]){
						cube.push(assumps[i]);
						seen[var(assumps[i])]=false;
					}else{
						cube.push(lit_Undef);
					}

				}
				disableSubProp=dp;
					disableSubSolve=ds;
					disable_simplification=dsimp;
			cancelUntil(0);
			assert(interpolant.size()==0);
			return false;
		}
}

//Solve just this solver's formula, without invoking any subsolvers.
bool Solver::solveStrictlyLocal(vec<Lit> & assumps){

	bool ds = disableSubSolve;
	bool dp = disableSubProp;
	bool dsimp = disable_simplification;
	//disable_simplification=true;
	disableSubSolve=true;
	disableSubProp=true;
	cancelUntil(0);
	budgetOff();

	assumps.copyTo(assumptions);
	if(solve_()==l_True){

		disableSubSolve=ds;
		disableSubProp=dp;
		disable_simplification=dsimp;
		cancelUntil(0);
		return true;
	}else{

		disable_simplification=dsimp;
		disableSubSolve=ds;
		disableSubProp=dp;

		cancelUntil(0);
		return false;
	}
}

// NOTE: assumptions passed in member-variable 'assumptions'.
lbool Solver::solve_()
{

	if(disableSubSolve){
		//If we disabled the subsolver for some reason, then before we use the subsovler again in the future (when disableSubSolve is no longer true), we will need to refresh the subsolver.
		sub_needs_refresh=true;
	}

	has_coi=false;
    model.clear();
    conflict.clear();
    initial_level=0;
    has_any_super_backtracked=false;
    succesful_solve=false;
    had_unit_prop=false;

    attempted_solve=true;
#ifndef NDEBUG
	if(assumptions.size()==1)
		dbg_property=assumptions[0];
	else
		dbg_property=lit_Undef;
#endif


    if (!ok) return l_False;
    top_solver=true;
    solves++;

    max_learnts               = nClauses() * learntsize_factor;
    learntsize_adjust_confl   = learntsize_adjust_start_confl;
    learntsize_adjust_cnt     = (int)learntsize_adjust_confl;
    lbool   status            = l_Undef;

    if (verbosity >= 1){
        printf("============================[ Search Statistics ]==============================\n");
        printf("| Conflicts |          ORIGINAL         |          LEARNT          | Progress |\n");
        printf("|           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |\n");
        printf("===============================================================================\n");
    }
    int ignore=0;
    track_min_level=0;
    initial_level=0;

    if(opt_clear_global_restarts){
		global_restarts=0;
    }
    global_conflictC=0;
    // Search:
    int curr_restarts = 0;
    while (status == l_Undef){
    	globalRestartTriggered=false;//this is as far as we restart.
        nof_global_conflicts=( luby_restart ? luby(restart_inc, global_restarts) : pow(restart_inc, global_restarts)) * restart_first;
        double rest_base = luby_restart ? luby(restart_inc, curr_restarts) : pow(restart_inc, curr_restarts);
        status = search(rest_base * restart_first, ignore);
    	if (globalRestartTriggered){
			global_restarts++;
			global_conflictC=0;
		}else{
			 curr_restarts++;
		}
        if (!withinBudget()) break;

    }

    if (verbosity >= 1)
        printf("===============================================================================\n");

    if (status == l_True){
      	assert(dbg_checkAnswer(true));
        // Extend & copy model:
        model.growTo(nVars());
        for (int i = 0; i < nVars(); i++) model[i] = value(i);
        assert(disableSubSolve || dbg_proveModel(model));
    }else if (status == l_False && conflict.size() == 0){
      	assert(dbg_checkAnswer(false));
        ok = false;
    }
   // cancelUntil(0);
    assumptions.clear();
    top_solver=false;
    return status;
}

//=================================================================================================
// Writing CNF to DIMACS:
// 
// FIXME: this needs to be rewritten completely.

static Var mapVar(Var x, vec<Var>& map, Var& max)
{
    if (map.size() <= x || map[x] == -1){
        map.growTo(x+1, -1);
        map[x] = max++;
    }
    return map[x];
}


void Solver::toDimacs(FILE* f, Clause& c, vec<Var>& map, Var& max)
{
    if (satisfied(c)) return;

    for (int i = 0; i < c.size(); i++)
        if (value(c[i]) != l_False)
            fprintf(f, "%s%d ", sign(c[i]) ? "-" : "", mapVar(var(c[i]), map, max)+1);
    fprintf(f, "0\n");
}


void Solver::toDimacs(const char *file, const vec<Lit>& assumps)
{
    FILE* f = fopen(file, "wr");
    if (f == NULL)
        fprintf(stderr, "could not open file %s\n", file), exit(1);
    toDimacs(f, assumps);
    fclose(f);
}


void Solver::toDimacs(FILE* f, const vec<Lit>& assumps)
{
    // Handle case when solver is in contradictory state:
    if (!ok){
        fprintf(f, "p cnf 1 2\n1 0\n-1 0\n");
        return; }

    vec<Var> map; Var max = 0;

    // Cannot use removeClauses here because it is not safe
    // to deallocate them at this point. Could be improved.
    int cnt = 0;
    for (int i = 0; i < clauses.size(); i++)
        if (!satisfied(ca[clauses[i]]))
            cnt++;
        
    for (int i = 0; i < clauses.size(); i++)
        if (!satisfied(ca[clauses[i]])){
            Clause& c = ca[clauses[i]];
            for (int j = 0; j < c.size(); j++)
                if (value(c[j]) != l_False)
                    mapVar(var(c[j]), map, max);
        }

    // Assumptions are added as unit clauses:
    cnt += assumptions.size();

    fprintf(f, "p cnf %d %d\n", max, cnt);

    for (int i = 0; i < assumptions.size(); i++){
        assert(value(assumptions[i]) != l_False);
        fprintf(f, "%s%d 0\n", sign(assumptions[i]) ? "-" : "", mapVar(var(assumptions[i]), map, max)+1);
    }

    for (int i = 0; i < clauses.size(); i++)
        toDimacs(f, ca[clauses[i]], map, max);

    if (verbosity > 0)
        printf("Wrote %d clauses with %d variables.\n", cnt, max);
}


//=================================================================================================
// Garbage Collection methods:

void Solver::relocAll(ClauseAllocator& to)
{

	if(tmp_confl!=CRef_Undef){
			int oldsz = ca[tmp_confl].size();
			ca[tmp_confl].setSize(max_tmp_confl);
			ca.reloc(tmp_confl,to);
			to[tmp_confl].setSize(oldsz);
		}
		if(tmp_reason!=CRef_Undef){
			int oldsz = ca[tmp_reason].size();
			ca[tmp_reason].setSize(max_tmp_reason);
			ca.reloc(tmp_reason,to);
			to[tmp_reason].setSize(oldsz);
		}

		for(int i = 0;i<solver_cref.size();i++){
			assert(ca[solver_cref[i]].isTheoryMarker());
			ca.reloc(solver_cref[i],to);
			assert(to[solver_cref[i]].isTheoryMarker());
		}
    // All watchers:
    //
    // for (int i = 0; i < watches.size(); i++)
    watches.cleanAll();
    for (int v = 0; v < nVars(); v++)
        for (int s = 0; s < 2; s++){
            Lit p = mkLit(v, s);
            // printf(" >>> RELOCING: %s%d\n", sign(p)?"-":"", var(p)+1);
            vec<Watcher>& ws = watches[p];
            for (int j = 0; j < ws.size(); j++)
                ca.reloc(ws[j].cref, to);
        }

    // All reasons:
    //
    for (int i = 0; i < trail.size(); i++){
        Var v = var(trail[i]);
        								//why is this needed?
        if (reason(v) != CRef_Undef &&  (ca[reason(v)].isTheoryMarker() || ca[reason(v)].reloced() || locked(ca[reason(v)])))
            ca.reloc(vardata[v].reason, to);
    }

    // All learnt:
    //
    for (int i = 0; i < learnts.size(); i++)
        ca.reloc(learnts[i], to);

    // All original:
    //
    for (int i = 0; i < clauses.size(); i++)
        ca.reloc(clauses[i], to);

    for (int i = 0; i < interpolant.size(); i++)
    	if(!interpolant[i].isLit()){
    		CRef r = interpolant[i].cref();
    		ca.reloc(r, to);
    		interpolant[i]=LRef(r);
    	}

    if(external_interpolant){
		for(int i = 0;i<(*external_interpolant)[frame].size();i++){
			if(!(*external_interpolant)[frame][i].isLit()){
		    		CRef r = (*external_interpolant)[frame][i].cref();
		    		ca.reloc(r, to);
		    		(*external_interpolant)[frame][i]=LRef(r);
		    	}
		}
    }

#ifndef NDEBUG
	for(int i = 0;i<solver_cref.size();i++){
		assert(to[solver_cref[i]].isTheoryMarker());;
	}
#endif
}


void Solver::garbageCollect()
{
    // Initialize the next region to a size corresponding to the estimated utilization degree. This
    // is not precise but should avoid some unnecessary reallocations for the new region:
    ClauseAllocator to(ca.size() - ca.wasted());
    ca.relocateAll(to);
    //relocAll(to);
    if (verbosity >= 2)
        printf("|  Garbage collection:   %12d bytes => %12d bytes             |\n", 
               ca.size()*ClauseAllocator::Unit_Size, to.size()*ClauseAllocator::Unit_Size);
    to.moveTo(ca);

#ifndef NDEBUG
	for(int i = 0;i<solver_cref.size();i++){
		assert(ca[solver_cref[i]].isTheoryMarker());;
	}
#endif
}
