/****************************************************************************************[Solver.h]
Copyright (c) 2003-2006, Niklas Een, Niklas Sorensson
Copyright (c) 2007-2010, Niklas Sorensson

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************************************/

#ifndef Minisat_Solver_h
#define Minisat_Solver_h

#include "mtl/Vec.h"
#include "mtl/Heap.h"
#include "mtl/Alg.h"
#include "utils/Options.h"
#include "core/SolverTypes.h"
#include "Config.h"
#include "mtl/VHeap.h"
#include "mtl/Bitset.h"
#include "Ternary.h"
#include "Cone.h"

#ifndef NDEBUG
#include "dbg/Debug_mini.h"

#endif


namespace Minisat {

//=================================================================================================
// Solver -- the main class:
//Solver has been modified to convert it into a simple SMT solver, where its only 'Theory' solver type is itself - another SAT solver.
//This implementation is _far_ more complicated than it needs to be, and supports many experimental options that are no longer used.
//It has also been significantly modified to support some optimizations needed by IC3 (eg, Cone of Influence reduction and Ternary Simulation).
//The theorySolve and theoryPropagate methods, in particular, are byzantine.

//For a clean, efficient, readable, high-performance implementation, see ModSAT.

class Solver : public Allocated  {
public:

    // Constructor/Destructor:
    //
    Solver(ClauseAllocator * external_allocator=NULL);

    virtual ~Solver();

    // Problem specification:
    //
    Var     newVar    (bool polarity = true, bool dvar = true); // Add a new variable with parameters specifying variable mode.
    void 	clearVar(Var v);
    bool    addClause (const vec<Lit>& ps);                     // Add a clause to the solver. 
    bool    addEmptyClause();                                   // Add the empty clause, making the solver contradictory.
    bool    addClause (Lit p);                                  // Add a unit clause to the solver. 
    bool    addClause (Lit p, Lit q);                           // Add a binary clause to the solver. 
    bool    addClause (Lit p, Lit q, Lit r);                    // Add a ternary clause to the solver. 
    bool    addClause_(      vec<Lit>& ps, bool learnt=false);                     // Add a clause to the solver without making superflous internal copy. Will
                                                                // change the passed vector 'ps'.
    void addClauses(const vec<Lit> & clauses);

    void recursiveCancelUntil(int level){
    	cancelUntil(level);
    	for(int i = 0;i<subSolvers.size();i++){
    		subSolvers[i]->recursiveCancelUntil(level);
    	}
    }
    void detachSubSolvers();
    void detachSubSolver(int solverNum);
    void attachSubSolver(Solver * _subSolver, int offset, int min_sub_interface, int max_sub_interface, bool re_attach=false);
    // Solving:
    //
    bool    simplify     (bool force =false);                        // Removes already satisfied clauses.

  //  bool    solve        (const vec<Lit>& assumps,vec<Var> & dbg_interface, Solver * subsolver=NULL, int offset=0, int nInterface=-1); // Search for a model that respects a given set of assumptions and satisfies the subsolver (if not null).
    bool 	solve(const vec<Lit>& assumps,vec<Lit>& cube, VHeap<TCube> * Q=NULL);
    bool    solve         (const vec<Lit>& assumps);
    lbool   solveLimited (const vec<Lit>& assumps); // Search for a model that respects a given set of assumptions (With resource constraints).
    bool    solve        ();                        // Search without assumptions.
    bool    solve        (Lit p);                   // Search for a model that respects a single assumption.
    bool    solve        (Lit p, Lit q);            // Search for a model that respects two assumptions.
    bool    solve        (Lit p, Lit q, Lit r);     // Search for a model that respects three assumptions.
    bool    okay         () const;                  // FALSE means solver is in a conflicting state

    void    toDimacs     (FILE* f, const vec<Lit>& assumps);            // Write CNF to file in DIMACS-format.
    void    toDimacs     (const char *file, const vec<Lit>& assumps);
    void    toDimacs     (FILE* f, Clause& c, vec<Var>& map, Var& max);

    // Convenience versions of 'toDimacs()':
    void    toDimacs     (const char* file);
    void    toDimacs     (const char* file, Lit p);
    void    toDimacs     (const char* file, Lit p, Lit q);
    void    toDimacs     (const char* file, Lit p, Lit q, Lit r);
    
    // Variable mode:
    // 
    void    setPolarity    (Var v, bool b); // Declare which polarity the decision heuristic should use for a variable. Requires mode 'polarity_user'.
    void    setDecisionVar (Var v, bool b); // Declare if a variable should be eligible for selection in the decision heuristic.

    // Read state:
    //
    lbool   value      (Var x) const;       // The current value of a variable.
    lbool   value      (Lit p) const;       // The current value of a literal.
    lbool   modelValue (Var x) const;       // The value of a variable in the last model. The last call to solve must have been satisfiable.
    lbool   modelValue (Lit p) const;       // The value of a literal in the last model. The last call to solve must have been satisfiable.
    int     nAssigns   ()      const;       // The current number of assigned literals.
    int     nClauses   ()      const;       // The current number of original clauses.
    int     nLearnts   ()      const;       // The current number of learnt clauses.
    int		nConstraints()		const;		//Total number of clauses + learnt + units.
    int     nVars      ()      const;       // The current number of variables.
    int     nFreeVars  ()      const;

    // Resource contraints:
    //
    void    setConfBudget(int64_t x);
    void    setPropBudget(int64_t x);
    void    budgetOff();
    void    interrupt();          // Trigger a (potentially asynchronous) interruption of the solver.
    void    clearInterrupt();     // Clear interrupt indicator flag.

    // Memory managment:
    //
    virtual void garbageCollect();
    void    checkGarbage(double gf);
    void    checkGarbage();


    // Extra results: (read-only member variable)
    //
    vec<lbool> model;             // If problem is satisfiable, this vector contains the model (if any).
    vec<Lit>   conflict;          // If problem is unsatisfiable (possibly under assumptions),
                                  // this vector represent the final conflict clause expressed in the assumptions.

    // Mode of operation:
    //

    int freeVars;
    bool	allow_super_units;
    bool sub_needs_refresh;
    int level_q;
    int frame;
    int max_frame;
    int super_qhead;
    vec<Lit> super_units;
    int       verbosity;
    bool has_any_super_backtracked;
    int		 initial_level;
    bool 	allow_unforced_subsearch;

    int forced_outputs;
    int forced_inputs;
    bool store_interpolant;
    double    var_decay;
    double    clause_decay;
    double    random_var_freq;
    double    random_seed;
    bool      luby_restart;
    int       ccmin_mode;         // Controls conflict clause minimization (0=none, 1=basic, 2=deep).
    int       phase_saving;       // Controls the level of phase saving (0=none, 1=limited, 2=full).
    bool      rnd_pol;            // Use random polarities for branching heuristics.
    bool      rnd_init_act;       // Initialize variable activities with a small random value.
    double    garbage_frac;       // The fraction of wasted memory allowed before a garbage collection is triggered.

	int solverNum;
    int       restart_first;      // The initial restart limit.                                                                (default 100)
    double    restart_inc;        // The factor with which the restart limit is multiplied in each restart.                    (default 1.5)
    int int_clauses; //how the solver treats conflict clauses returned by the sub solver
    int int_sub_reasons;

    //This is >-1 if this solver must backtrack to this level before solving next (this is used to lazily backtrack solvers)
    int must_cancel;

    vec<char> seen_subsume;


    vec<LRef> interpolant;

    //*******Additions for PDR*******

    /**
     * This keeps tracks of all the solutions found by the solver. Required for PDR.
     */
    VHeap<TCube> * cubes;
    //This is _very_ ugly, should be fixed.
    vec<vec<LRef> > * external_interpolant;

    //This vector stores, for each latch, a bitset describing which latches are in the coi for that latch.
    ConeOfInfluence * latch_coi;
    Bitset property_coi;
    Bitset current_coi;
    Bitset ternary_coi;

    bool has_coi;
    bool has_ternary;
    Lit negatedProperty;

    Ternary* ternary;
    vec<Lit> ternary_assign;
    vec<lbool> ternary_inputs;
    vec<Var> ternary_preserve;

    void computeTernary(){
    	if(opt_ternary && ! has_ternary){
    		has_ternary=true;
    		if(opt_coi && !has_coi){
    			computeCOI();
    		}
    		if(super && !super->has_ternary && !assumptions.size())
    			super->computeTernary();
			ternary_inputs.clear();
			for(int i = ternary->min_primary_input;i<ternary->max_primary_input;i++){
				Var v = i;

				ternary_inputs.push(value(v));

			}

			ternary_assign.clear();
			for(int i = 1;i<=sub_ninterface;i++){
				Var v = i;
				if(value(v)!=l_Undef && (!opt_coi || current_coi[v])){
					ternary_assign.push(mkLit(v, value(v)==l_False));
				}

			}
			int sz =ternary_assign.size();
			ternary_preserve.clear();
			if (!super && assumptions.size()==1 && var(assumptions[0]) == var(negatedProperty)){
				ternary_preserve.push(var_Undef);
			}else if (top_solver){

				for(int i = 0;i<assumptions.size();i++){
					Lit l = assumptions[i];
					Lit latch_in = toSuper(l);
					Var v = var(latch_in);
					ternary_preserve.push(v);
				}

			}else if (super){
				for(int i = 1;i<=sub_ninterface;i++){
					Var v = i;
					if(opt_ternary?  super->ternary_coi[v]: super->current_coi[v]){
						ternary_preserve.push(v);
					}
				}
			}else{
				assert(false);
			}

			ternary->reduce(ternary_assign,ternary_inputs,ternary_preserve);
			assert(ternary_assign.size());
			num_ternary_blocked+= sz- ternary_assign.size();
			ternary_coi.clear();
			ternary_coi.growTo(sub_ninterface+1);
			ternary_coi.zero();
			for(int i = 0;i<ternary_assign.size();i++){
				ternary_coi.set(var(ternary_assign[i]));
			}


    	}
    }

    void computeCOI(){

    	if(opt_coi && ! has_coi){
    		has_coi=true;

    		//this is such a hack...
			if (!super && assumptions.size()==1 && var(assumptions[0]) == var(negatedProperty)){
				property_coi.copyTo(current_coi);
			}else if (top_solver){
				current_coi.clear();
				current_coi.growTo(sub_ninterface+1);
				for(int i = 0;i<assumptions.size();i++){
					Lit l = assumptions[i];
					Lit latch_in = toSuper(l);
					Var v = var(latch_in);
					current_coi.Or(latch_coi->latch_coi[v]);
					assert(current_coi.size()==property_coi.size());
				}

			}else if (super){
				if(!super->has_coi){
					super->computeCOI();
				}
				if(opt_ternary && ! super->has_ternary){
					super->computeTernary();
				}
				//ok, the coi is computed from the super solver's coi
				current_coi.clear();
				current_coi.growTo(sub_ninterface+1);

				for(int i = 1;i<=sub_ninterface;i++){
					Var v = i;
					if(opt_ternary?  super->ternary_coi[v]: super->current_coi[v]){
						current_coi.Or(latch_coi->latch_coi[v]);
					}
				}
				assert(current_coi.size()==property_coi.size());
			}else{
				assert(false);
			}


    	}
    	if(opt_ternary)
    		computeTernary();
    }

    //This records whether any new clauses have been added to this solvers interpolant
    int interpolant_increase;

    vec<bool> in_interpolant;//True if the unit clause corresponding to this literal is already in the interpolant
    //vec<Lit> interpolant;
    int interpolant_size;
    double    learntsize_factor;  // The intitial limit for learnt clauses is a factor of the original clauses.                (default 1 / 3)
    double    learntsize_inc;     // The limit for learnt clauses is multiplied with this factor each restart.                 (default 1.1)

    int       learntsize_adjust_start_confl;
    double    learntsize_adjust_inc;

    bool top_solver;
    bool attempted_solve;
    bool succesful_solve;
    bool had_unit_prop;



    static int nof_global_conflicts,global_conflictC,global_restarts;
    static bool globalRestartTriggered;
    // Statistics: (read-only member variable)
    //
    uint64_t solves, starts, decisions, rnd_decisions, propagations, conflicts;
    uint64_t dec_vars, clauses_literals, learnts_literals, max_literals, tot_literals;
    uint64_t num_coi_blocked,total_assign_lits,num_ternary_blocked;

protected:
public:
    // Helper structures:
    //
    struct VarData {
    	CRef reason;
    	int level;
    };
    static inline VarData mkVarData(CRef cr,int l){ VarData d = {cr, l}; return d; }

    struct Watcher {
        CRef cref;
        Lit  blocker;
        Watcher(CRef cr, Lit p) : cref(cr), blocker(p) {}
        bool operator==(const Watcher& w) const { return cref == w.cref; }
        bool operator!=(const Watcher& w) const { return cref != w.cref; }
    };

    struct WatcherDeleted
    {
        const ClauseAllocator& ca;
        WatcherDeleted(const ClauseAllocator& _ca) : ca(_ca) {}
        bool operator()(const Watcher& w) const { return ca[w.cref].mark() == 1; }
    };

    struct VarOrderLt {
        const vec<double>&  activity;
        bool operator () (Var x, Var y) const { return activity[x] > activity[y]; }
        VarOrderLt(const vec<double>&  act) : activity(act) { }
    };

    // Solver state:
    //
    bool                ok;               // If FALSE, the constraints are already unsatisfiable. No part of the solver state may be used!
    int allocated_index;
    bool using_external_allocator;
    ClauseAllocator  &   ca;

    bool disableSubProp;
    bool disableSubSolve;
    bool disable_simplification;
    bool switch_eager;
    //Control whether eager unit propagation (aka, 'theory propagation'), is used
    bool eager_propagation;
    int track_min_trail;
    int track_min_level;
    bool firstCall;
    Solver * super;
    int sub_offset;
    int sub_ninterface;
    int super_offset;
    int super_min_interface;
    int super_max_interface;

    bool quit_eager;

    struct SuperData{
    	int decision_level;
    	Lit decision;
    	SuperData():decision_level(0),decision(lit_Undef){};
    };

    vec<SuperData> super_levels;
    vec<Lit> retain;
    vec<Solver*> 			subSolvers;
    vec<int> sub_qhead;
    //Theory-markers, for identifying clauses that must be lazily constructed.
    vec<CRef> solver_cref;
    int num_irrelevant;
    vec<CRef>           clauses;          // List of problem clauses.
    vec<CRef>           learnts;          // List of learnt clauses.
    double              cla_inc;          // Amount to bump next clause with.
    vec<double>         activity;         // A heuristic measurement of the activity of a variable.
    double              var_inc;          // Amount to bump next variable with.
    OccLists<Lit, vec<Watcher>, WatcherDeleted>
                        watches;          // 'watches[lit]' is a list of constraints watching 'lit' (will go there if literal becomes true).
    vec<lbool>          assigns;          // The current assignments.
    vec<char>           polarity;         // The preferred polarity of each variable.
    vec<char>           decision;         // Declares if a variable is eligible for selection in the decision heuristic.
    vec<Lit> subtrail;

    vec<Lit>            trail;            // Assignment stack; stores all assigments made in the order they were made.
    vec<int>            trail_lim;        // Separator indices for different decision levels in 'trail'.
    vec<VarData>        vardata;          // Stores reason and level for each variable.
    int                 qhead;            // Head of queue (as index into the trail -- no more explicit propagation queue in MiniSat).
    int                 simpDB_assigns;   // Number of top-level assignments since last execution of 'simplify()'.
    int64_t             simpDB_props;     // Remaining number of propagations that must be made before next execution of 'simplify()'.
    vec<Lit>            assumptions;      // Current set of assumptions provided to solve by the user.
    Heap<VarOrderLt>    order_heap;       // A priority queue of variables ordered with respect to the variable activity.
    double              progress_estimate;// Set by 'search()'.
    bool                remove_satisfied; // Indicates whether possibly inefficient linear scan for satisfied clauses should be performed in 'simplify'.



    // Temporaries (to reduce allocation overhead). Each variable is prefixed by the method in which it is
    // used, exept 'seen' wich is used in several places.
    //
    vec<char>           seen;
    vec<Lit>            analyze_stack;
    vec<Lit>            analyze_toclear;
    vec<Lit> tmp_reason_vec;
    vec<Lit>            add_tmp;
    vec<Lit> 	local_conflict;
    vec<Lit> super_conflict;
    double              max_learnts;
    double              learntsize_adjust_confl;
    int                 learntsize_adjust_cnt;

    // Resource contraints:
    //
    int64_t             conflict_budget;    // -1 means no budget.
    int64_t             propagation_budget; // -1 means no budget.
    bool                asynch_interrupt;

    CRef tmp_confl;
    int max_tmp_confl;

   CRef tmp_reason;
    int max_tmp_reason;

    // Main internal methods:
    //
    void     insertVarOrder   (Var x);                                                 // Insert a variable in the decision order priority queue.
    Lit      pickBranchLit    ();                                                      // Return the next decision variable.
    void     newDecisionLevel ();                                                      // Begins a new decision level.
    void     uncheckedEnqueue (Lit p, CRef from = CRef_Undef);                         // Enqueue a literal. Assumes value of literal is undefined.
    void uncheckedEnqueueLocal(Lit global, int solver, Lit local);

    CRef prepare_eagerProp(int subSolverNum);//This needs to be called before propagating to a subsolver, IF we have solved this super solver with eager unit propagation disabled.

    inline bool sub_interface(Var v)const{
    	//exclude lit 0, which is just the constant lit
    	return v && (v<= sub_ninterface);
    }
    inline bool sub_interface(Lit l)const{
     	return  sub_interface(var(l));
    }
    inline bool super_interface(Lit p)const{
      	return var(p)>=super_min_interface && var(p) < super_max_interface;
      }
    inline bool super_interface(Var v)const{
    	return v>=super_min_interface  && v < super_max_interface;
    }



    bool addToInterpolant(Lit l);
    void addToInterpolant(CRef cr){

    	interpolant.push(LRef(cr));

    	assert(!opt_subsume_interpolant || ca[cr].has_extra());
#ifndef NDEBUG
    	vec<Lit> c;

    	LRef(cr).copyTo(c, ca);
  

    	Solver * s = this;
    	assert(dbg_proveLearntClause(c));
    	while(s->subSolvers.size()){
    		assert(s->dbg_proveLearntClause(c));
    		s = s->subSolvers[0];
    	}
#endif
    	interpolant_size++;interpolant_increase++;
    }

    bool inInterpolant(Var v){
    	assert(in_interpolant.size()>v);
    	return in_interpolant[v];
    }

    LRef buildInterpolantClause(vec<Lit> &c);

    Clause & getClause(Lit p, CRef r){
    	assert(r!=CRef_Undef);
    	Clause & c = ca[r];
    	if(!c.isTheoryMarker())
    	    return c;

    	//lazily construct the reason and add it to the interpolant

    	int solverNum = c.getTheory();
		assert(solverNum>=0);
		assert(solverNum<subSolvers.size());
		assert(p!=lit_Undef);

		CRef ref =buildSubReason(p,solverNum);
    	assert(r==reason(var(p)));
    	vardata[var(p)].reason=ref;
    	Clause & cls = ca[ref];
    	return cls;
    }


    bool     enqueue          (Lit p, CRef from = CRef_Undef);                         // Test if fact 'p' contradicts current state, enqueue otherwise.

    CRef     propagate        ();                                                      // Perform unit propagation. Returns possibly conflicting clause.
    bool solveRelative(vec<Lit> & cube, vec<Lit> & out, Lit activation, int offset); //Specialized method for applying inductive generalization
    bool solveStrictlyLocal(vec<Lit> & cube);//Solve just this solver's formula, without invoking any attached subsolvers
    bool solveTheory(int subSolverNum,vec<Lit>  &local_prop, vec<Lit> &local_conflict, bool any_super_backtracked, VHeap<TCube> * trace );//Solve this solver under the assignment of its super solver.
	bool propagateTheory(int subSolverNum, int level, Lit decision, vec<Lit> & to_propagate, int qhead, vec<Lit> & conflict);//Applies unit propagation from this solver's super solver into this solver.
    CRef  propSubSolvers();
    CRef  solveSubSolvers();
    void     cancelUntil      (int level);                                             // Backtrack until a certain level.
    bool     analyze          (CRef confl, vec<Lit>& out_learnt, int& out_btlevel);    // (bt = backtrack)
    void 		analyzeFinalSuper(CRef confl, Lit skip_lit, vec<Lit>& out_conflict);
    void     analyzeFinal     (Lit p, vec<Lit>& out_conflict);                         // COULD THIS BE IMPLEMENTED BY THE ORDINARIY "analyze" BY SOME REASONABLE GENERALIZATION?
    bool     litRedundant     (Lit p, uint32_t abstract_levels);                       // (helper method for 'analyze()')
    lbool    search           (int nof_conflicts,    int & start_trail);                                     // Search for a given number of conflicts.
    lbool    solve_           ();                                                      // Main solve method (assumptions given in 'assumptions').
    void     reduceDB         ();                                                      // Reduce the set of learnt clauses.
    void     removeSatisfied  (vec<CRef>& cs);                                         // Shrink 'cs' to contain only non-satisfied clauses.
    void 	 removeSatisfied(vec<LRef>& cs);
    void     rebuildOrderHeap ();
public:
    //Cancel the subsolver at least as far back as the super solver has been cancelled, before attempting to solve the subsolver
    void lazyCancel(int super_level, Lit super_decision);
    bool handleSuperConflict(CRef confl, vec<Lit> & conflict,int & start_trail);

    int superDecisionLevel()const{
        assert(super_levels.size()>decisionLevel());
       	return super_levels[decisionLevel()].decision_level;
    }
    Lit superDecision()const{
        assert(super_levels.size()>decisionLevel());
      	return super_levels[decisionLevel()].decision;

    }


    CRef buildConflict(vec<Lit> & from);
    //Derive a reason clause from the subsolver to explain why literal p was forced.
    //We are doing this lazily, only when required by conflict analysis in the super solver.
    CRef buildSubReason(Lit p, int solver);


    inline Var toSuper(Var p){
    	assert(p<nVars());
    	Var v=  p-super_offset;

        return v;
    }

    inline Lit toSuper(Lit p){
    	assert(var(p)<nVars());
    	Lit l=  mkLit(var(p)-super_offset,sign(p));

    	return l;
    }
    inline Var fromSuper(Var p){
    	return  p+super_offset;
    }

    inline Lit fromSuper(Lit p){
    	return  mkLit(var(p)+super_offset,sign(p));
    }

    // Maintaining Variable/Clause activity:
    //
    void     varDecayActivity ();                      // Decay all variables with the specified factor. Implemented by increasing the 'bump' value instead.
    void     varBumpActivity  (Var v, double inc);     // Increase a variable with the current 'bump' value.
    void     varBumpActivity  (Var v);                 // Increase a variable with the current 'bump' value.
    void     claDecayActivity ();                      // Decay all clauses with the specified factor. Implemented by increasing the 'bump' value instead.
    void     claBumpActivity  (Clause& c);             // Increase a clause with the current 'bump' value.

    // Operations on clauses:
    //
    void     attachClause     (CRef cr);               // Attach a clause to watcher lists.
    void     detachClause     (CRef cr, bool strict = false); // Detach a clause to watcher lists.
    void     removeClause     (CRef cr);               // Detach and free a clause.
    bool     locked           (const Clause& c) const; // Returns TRUE if a clause is a reason for some implication in the current state.
    bool     satisfied        (const Clause& c) const; // Returns TRUE if a clause is satisfied in the current state.

    void     relocAll         (ClauseAllocator& to);

    // Misc:
    //
    int      decisionLevel    ()      const; // Gives the current decisionlevel.
    uint32_t abstractLevel    (Var x) const; // Used to represent an abstraction of sets of decision levels.
    CRef     reason           (Var x) const;
    int      level            (Var x) const;
    double   progressEstimate ()      const; // DELETE THIS ?? IT'S NOT VERY USEFUL ...
    bool     withinBudget     ()      const;

    // Static helpers:
    //

    // Returns a random float 0 <= x < 1. Seed must never be 0.
    static inline double drand(double& seed) {
        seed *= 1389796;
        int q = (int)(seed / 2147483647);
        seed -= (double)q * 2147483647;
        return seed / 2147483647; }

    // Returns a random integer 0 <= x < size. Seed must never be 0.
    static inline int irand(double& seed, int size) {
        return (int)(drand(seed) * size); }

public:
    void setAllocatedIndex(int index){
     	allocated_index=index;
    }
    int getAllocatedIndex(){
    	return allocated_index;
    }


    //DEBUGGING interface
#ifndef NDEBUG
    static int iteration;

    int dbg_solverNum;
    int dbg_super_int;
    int dbg_super_off;
    Lit dbg_property;
    Solver * dbg_super;
    bool dbg_exported;
    bool computedDbgClauses;
    int maxSubVar;
	DbgMini dbg_mini;
	DbgMini *dbg_sub_mini;
	bool dbg_expect_answer;
	vec<vec<Lit> > dbgclauses;
	vec<vec<Lit> > dbg_local_clauses;
#endif

	 void print(Lit l);
	 void printVector(vec<Lit> &v);

	 void printTrail();
	 void dbg_AddClause(const vec<Lit> & clause);
	 void dbg_AddLocalClause(const vec<Lit> & clause);
	 void dbg_AddLocalClause(const Clause & c);
	 void dbg_exportClauses(Solver * const to, int solverNum);
	 bool dbg_Solve();

	 void dbg_RefreshSubSolvers();

	 void dbg_AddSubClause(const vec<Lit> & clause);
	 void dbg_AddSubClause(const Clause & clause);
	 vec<vec<Lit> > & dbg_getSubClauses();
	 void dbg_AddInterfaceClause(const vec<Lit> & clause);
	 int dbg_nextFreeSubVar();

	 bool dbg_checkReasons();

	 void dbg_getClause(Lit p, CRef r,  vec<Lit> & reason);
	 void dbg_buildSubReason(Lit p, int solverNum, vec<Lit> & reason);
	 bool dbg_checkValidSuperReason(Lit sub, Lit super);
	 bool dbg_proveClause(Clause & c);
	 bool dbg_proveLocalClause(const vec<Lit> & clause);
	 bool dbg_proveLocalClause(Clause & c);
	 bool dbg_proveConflict(CRef confl);
	 bool dbg_proveInterfaceAssignment();
	 bool dbg_proveLocalAssignment();
	 bool dbg_checkInterfaceMatches();
	 bool dbg_proveAssignment();
	 bool dbg_subAssignments();
	 void dbg_copyConstraints(vec<vec<Lit> > & constraints)const;
      //Prove that the whole trail is correct, given the decision variables.
      bool dbg_proveWholeTrail();
      //Prove that this literal follows from unit propagation on the decisions made up to it's level (even if that level is lower than the solvers current decision level)
      bool dbg_proveUnitPropagation(Lit l);
      bool dbg_checkInterpolant();
      bool dbg_checkConflict(int level, bool expected);
      //Prove that this learnt clause is correct (ie, that it is redundant in the original CNF)
      bool dbg_proveLearntClause(const vec<Lit> & clause);
      bool dbg_proveLearntSubClause(const vec<Lit> & clause);
      bool dbg_conflicting(const vec<Lit> & clause);
      bool dbg_proveUnit(Lit p);
      //Add this clause to the debug solver's CNF.
      bool dbg_checkEarlierOnTrail(Lit q,int index);
      bool dbg_checkPartialLearnt(int index,vec<char> & seen,vec<Lit> & out_learnt);

      bool dbg_proveModel(const vec<lbool> & model);

      bool dbg_checkAnswer(bool answer);

      bool dbg_proveUnitPropComplete(int level);

      bool dbg_checkWatchers(bool check_levels=false);

      bool dbg_correctVsidsDecisions(int after_level);

      bool dbg_correctTrailInfo();

      bool dbgQheads(int up_to_level);

      bool dbg_checkCauses();

      bool dbg_proveLearntSuperClause(const vec<Lit> & clause);

};

#include "dbg/Debug.h"
//=================================================================================================
// Implementation of inline methods:

inline CRef Solver::reason(Var x) const { return vardata[x].reason; }
inline int  Solver::level (Var x) const { return vardata[x].level; }

inline void Solver::insertVarOrder(Var x) {
    if (!order_heap.inHeap(x) && decision[x]) order_heap.insert(x); }

inline void Solver::varDecayActivity() { var_inc *= (1 / var_decay); }
inline void Solver::varBumpActivity(Var v) { varBumpActivity(v, var_inc); }
inline void Solver::varBumpActivity(Var v, double inc) {
    if ( (activity[v] += inc) > 1e100 ) {
        // Rescale:
        for (int i = 0; i < nVars(); i++)
            activity[i] *= 1e-100;
        var_inc *= 1e-100; }

    // Update order_heap with respect to new activity:
    if (order_heap.inHeap(v))
        order_heap.decrease(v); }

inline void Solver::claDecayActivity() { cla_inc *= (1 / clause_decay); }
inline void Solver::claBumpActivity (Clause& c) {
        if ( (c.activity() += cla_inc) > 1e20 ) {
            // Rescale:
            for (int i = 0; i < learnts.size(); i++)
                ca[learnts[i]].activity() *= 1e-20;
            cla_inc *= 1e-20; } }

inline void Solver::checkGarbage(void){ return checkGarbage(garbage_frac); }
inline void Solver::checkGarbage(double gf){
    if (ca.wasted() > ca.size() * gf)
        garbageCollect(); }

// NOTE: enqueue does not set the ok flag! (only public methods do)
inline bool     Solver::enqueue         (Lit p, CRef from)      { return value(p) != l_Undef ? value(p) != l_False : (uncheckedEnqueue(p, from), true); }
inline bool     Solver::addClause       (const vec<Lit>& ps)    { ps.copyTo(add_tmp); return addClause_(add_tmp); }
inline bool     Solver::addEmptyClause  ()                      { add_tmp.clear(); return addClause_(add_tmp); }
inline bool     Solver::addClause       (Lit p)                 { add_tmp.clear(); add_tmp.push(p); return addClause_(add_tmp); }
inline bool     Solver::addClause       (Lit p, Lit q)          { add_tmp.clear(); add_tmp.push(p); add_tmp.push(q); return addClause_(add_tmp); }
inline bool     Solver::addClause       (Lit p, Lit q, Lit r)   { add_tmp.clear(); add_tmp.push(p); add_tmp.push(q); add_tmp.push(r); return addClause_(add_tmp); }
inline bool     Solver::locked          (const Clause& c) const {assert(!c.isTheoryMarker()); return value(c[0]) == l_True && reason(var(c[0])) != CRef_Undef && ca.lea(reason(var(c[0]))) == &c; }
inline void     Solver::newDecisionLevel()                      { trail_lim.push(trail.size());
super_levels.growTo(decisionLevel()+1);
}

inline int      Solver::decisionLevel ()      const   { return trail_lim.size(); }
inline uint32_t Solver::abstractLevel (Var x) const   { return 1 << (level(x) & 31); }
inline lbool    Solver::value         (Var x) const   { return assigns[x]; }
inline lbool    Solver::value         (Lit p) const   { return assigns[var(p)] ^ sign(p); }
inline lbool    Solver::modelValue    (Var x) const   { return model[x]; }
inline lbool    Solver::modelValue    (Lit p) const   { return model[var(p)] ^ sign(p); }
inline int      Solver::nAssigns      ()      const   { return trail.size(); }
inline int      Solver::nClauses      ()      const   { return clauses.size(); }
inline int      Solver::nLearnts      ()      const   { return learnts.size(); }
inline int      Solver::nConstraints         ()      const   { return clauses.size() +  learnts.size() + (trail_lim.size() == 0 ? trail.size() : trail_lim[0]); }
inline int      Solver::nVars         ()      const   { return vardata.size()-freeVars; }

inline int      Solver::nFreeVars     ()      const   { return (int)dec_vars - (trail_lim.size() == 0 ? trail.size() : trail_lim[0]); }
inline void     Solver::setPolarity   (Var v, bool b) { polarity[v] = b; }
inline void     Solver::setDecisionVar(Var v, bool b) 
{ 
    if      ( b && !decision[v]) dec_vars++;
    else if (!b &&  decision[v]) dec_vars--;

    decision[v] = b;
    insertVarOrder(v);
}
inline void     Solver::setConfBudget(int64_t x){ conflict_budget    = conflicts    + x; }
inline void     Solver::setPropBudget(int64_t x){ propagation_budget = propagations + x; }
inline void     Solver::interrupt(){ asynch_interrupt = true; }
inline void     Solver::clearInterrupt(){ asynch_interrupt = false; }
inline void     Solver::budgetOff(){ conflict_budget = propagation_budget = -1; }
inline bool     Solver::withinBudget() const {
    return !asynch_interrupt &&
           (conflict_budget    < 0 || conflicts < (uint64_t)conflict_budget) &&
           (propagation_budget < 0 || propagations < (uint64_t)propagation_budget); }

// FIXME: after the introduction of asynchronous interrruptions the solve-versions that return a
// pure bool do not give a safe interface. Either interrupts must be possible to turn off here, or
// all calls to solve must return an 'lbool'. I'm not yet sure which I prefer.
inline bool     Solver::solve         ()                    { budgetOff(); assumptions.clear(); bool r = solve_() == l_True; cancelUntil(0); return r; }
inline bool     Solver::solve         (Lit p)               { budgetOff(); assumptions.clear(); assumptions.push(p); bool r= solve_() == l_True; 	cancelUntil(0); return r;}
inline bool     Solver::solve         (Lit p, Lit q)        { budgetOff(); assumptions.clear(); assumptions.push(p); assumptions.push(q); return solve_() == l_True; }
inline bool     Solver::solve         (Lit p, Lit q, Lit r) { budgetOff(); assumptions.clear(); assumptions.push(p); assumptions.push(q); assumptions.push(r); return solve_() == l_True; }
inline bool     Solver::solve         (const vec<Lit>& assumps){
	budgetOff();

	assumps.copyTo(assumptions);
	bool r = solve_()== l_True;
	cancelUntil(0);
	return r;
}

inline lbool    Solver::solveLimited  (const vec<Lit>& assumps){ assumps.copyTo(assumptions); return solve_(); }
inline bool     Solver::okay          ()      const   { return ok; }

inline void     Solver::toDimacs     (const char* file){ vec<Lit> as; toDimacs(file, as); }
inline void     Solver::toDimacs     (const char* file, Lit p){ vec<Lit> as; as.push(p); toDimacs(file, as); }
inline void     Solver::toDimacs     (const char* file, Lit p, Lit q){ vec<Lit> as; as.push(p); as.push(q); toDimacs(file, as); }
inline void     Solver::toDimacs     (const char* file, Lit p, Lit q, Lit r){ vec<Lit> as; as.push(p); as.push(q); as.push(r); toDimacs(file, as); }


//=================================================================================================
}

#endif
