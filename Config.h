/*
 * Config.h
 *
 *  Created on: 2012-10-20
 *      Author: sam
 */

#ifndef CONFIG_H_
#define CONFIG_H_

#ifndef NDEBUG
extern int dbg_total_iterations;

//#define DEBUG_MINI
//#define DBG_CHECK_BMC

//#define PROVE_LEARNT

//#define PROVE_ANALYSIS
//#define PROVE_UNIT_PROP
//#define PROVE_CONFLICTS
//#define PROVE_TRAIL
#define CHECK_INTERPOLANT


//#define REPORT
#ifdef REPORT
#define REPORT_TRAIL

#endif

#endif


#include "utils/Options.h"
namespace Minisat {
extern IntOption      opt_verb;
extern  DoubleOption  opt_var_decay;
extern  DoubleOption  opt_clause_decay ;
extern  DoubleOption  opt_random_var_freq ;
extern  DoubleOption  opt_random_seed;
extern  IntOption     opt_ccmin_mode  ;
extern  IntOption     opt_phase_saving ;
extern  BoolOption    opt_rnd_init_act ;
extern  BoolOption    opt_luby_restart ;
extern  IntOption     opt_restart_first ;
extern  DoubleOption  opt_restart_inc   ;
extern  DoubleOption  opt_garbage_frac  ;
extern  IntOption opt_interface_clauses;
extern  IntOption opt_learn_sub_reasons;
extern IntOption opt_eager_prop;
extern IntOption opt_eager_prop_break;
extern IntOption opt_subsearch;
const int eager_quit=0;
const int classic_search=1;
const int force_assumption_quit=2;
const int dont_subsearch_after_backtrack=3;
const int dont_subsearch_past_interpolant_after_backtrack=4;

extern BoolOption opt_subsearch_allow_after_interpolant;
extern BoolOption opt_lowest_unsolved;
extern BoolOption opt_switch_eager;
extern IntOption opt_force_restart_subsolver;


extern BoolOption opt_allow_super_units;
extern BoolOption opt_print_interpolants;
extern BoolOption opt_print_lowest_frame;

extern BoolOption opt_global_restarts;
extern BoolOption opt_first_solver_local_restarts;
extern BoolOption opt_clear_global_restarts;
extern BoolOption opt_local_restarts;
extern BoolOption opt_subsearch_allow_after_interpolant;

extern BoolOption opt_interpolants;

extern BoolOption opt_subsume_interpolant;
extern BoolOption opt_add_negated_reset;
extern BoolOption opt_global_unit_prop_shortcut;
extern BoolOption opt_selfloop;
extern BoolOption opt_early_clause_prop;
extern BoolOption opt_check_blocked;
extern BoolOption opt_queue_forward;


extern BoolOption opt_print_cubes;

extern BoolOption opt_keep_cubes;
extern BoolOption opt_forward_all;

extern BoolOption opt_push_forward_without_conflict;

extern BoolOption opt_use_queue;
extern BoolOption opt_list_interpolants;
extern IntOption opt_sort_when_generalizing;
extern BoolOption opt_generalize;
extern BoolOption opt_generalize_all;


extern BoolOption opt_allow_subsolver_redundant;

extern BoolOption opt_add_global_unit_prop_clauses_to_q;
extern BoolOption opt_check_subsume_eager;
extern BoolOption opt_use_naive_modular_sat_solver;
extern BoolOption opt_generalize_during_sat;
extern BoolOption opt_remove_subsumed_last;
extern BoolOption opt_remove_subsumed_during_clause_prop;
extern BoolOption opt_fast_clause_propagation;

extern IntOption opt_generalize_rounds;
extern BoolOption opt_push_clauses_down;
extern IntOption opt_recycle_type;
extern IntOption opt_recycle;
extern IntOption opt_max_generalization_failures;
extern IntOption opt_force_full_propagation_interval;
extern BoolOption opt_coi;
extern BoolOption opt_maintain_cti_order;
extern BoolOption opt_keep_all_cubes;



extern BoolOption opt_print_gen;
extern BoolOption opt_list_gen;
extern BoolOption opt_ternary;
extern BoolOption opt_print_coi;
extern BoolOption opt_coi_during_sat;
extern BoolOption opt_verify;
extern BoolOption opt_semi_restart;
extern BoolOption opt_only_ternary_conflict;
extern BoolOption opt_prop_first;
extern BoolOption opt_print_depth;
extern BoolOption opt_separate_ca;
extern BoolOption opt_print_ca;
extern BoolOption opt_force_garbage_collect;
extern IntOption  opt_prop_zero;
}

#endif /* CONFIG_H_ */
