/*
 * NaiveSolver.cpp
 *
 *  Created on: 2013-10-07
 *      Author: sam
 */


#include "Config.h"
#include <cassert>
#include "NaiveSolver.h"

bool NaiveSolver::solve(int frame,  vec<Lit> & assumptions, vec<Lit> & cube, VHeap<TCube> * Q){
	//ok, now solve S1 modulo S0
	naive_assigns.growTo(frame+1);
	naive_coi.growTo(frame+1);
	naive_tern.growTo(frame+1);
	int iteration =0;
	if(opt_coi){
			if(assumptions.size()==1 && var(assumptions[0])==var(negatedProperty)){
				property_coi.copyTo(naive_coi[frame]);
			}else{
				naive_coi[frame].clear();
				naive_coi[frame].growTo(in_latches.last()+1);
				for(int i = 0;i<assumptions.size();i++){
					Lit l = assumptions[i];
					Var v = var(l)-offset;
					naive_coi[frame].Or(latch_coi->latch_coi[v]);
					assert(naive_coi[frame].size()==property_coi.size());
				}
			}
		}
	while(true){
		++iteration;
		Solver & S = *solvers[frame];

		if (!S.solve(assumptions)){
			S.conflict.copyTo(cube);
			return false;
		}

		//now solve S0 under this assignment - in this case, meaning that S0's latches have to equal the input latches to S1
		//first nInterface variables are shared between the solvers.
		if(frame==0){
			return true;//done
		}
		vec<Lit> & assign = naive_assigns[frame];
		assign.clear();
		TCube c(frame+1);
		for(Var i = 0;i<in_latches.size();i++){
			Var v = in_latches[i];
			//the modsat solver doesn't apply coi reduction until after finding the conflict, but it probably makes sense for the naive solver to do so in advance
			if( (opt_coi || opt_ternary) && !naive_coi[frame][v])
				continue;
			int o = (opt_ternary?0:offset);
			assign.push(mkLit(v+o, S.model[v]==l_False));
		}

		if(opt_ternary){
				ternary_preserve.clear();
				if(assumptions.size()==1 && var(assumptions[0])==var(negatedProperty)){
					ternary_preserve.push(var_Undef);
				}else if (assumptions.size()){

					for(int i = 0;i<assumptions.size();i++){
						Lit l = assumptions[i];
						Var v = var(l)-offset;
						ternary_preserve.push(v);
					}

				}
				ternary_inputs.clear();
				for(int i = ternary->min_primary_input;i<ternary->max_primary_input;i++){
					Var v = i;

					ternary_inputs.push(S.model[v]);

				}



				ternary->reduce(assign,ternary_inputs,ternary_preserve);
				assert(assign.size());

				for(Var i = 0;i<assign.size();i++){
					Lit l = assign[i];
					assign[i]=mkLit(var(l)+offset,sign(l));
				}

			}

		 if(Q  && (opt_keep_cubes ||frame<solvers.size()-1)){
				 TCube c(frame+1);
				 for(Var i = 0;i<assign.size();i++){
					 Lit l = assign[i];

					 c.assignment->push(mkLit(var(l)-offset,sign(l)));
				}
				Q->insert(c.frame,c);
		 }

		 if(opt_coi){
			naive_coi[frame-1].clear();
			naive_coi[frame-1].growTo(in_latches.last()+1);
			for(int i = 0;i<assign.size();i++){
				Lit l = assign[i];
				Var v = var(l)-offset;
				naive_coi[frame-1].Or(latch_coi->latch_coi[v]);
				assert(naive_coi[frame-1].size()==property_coi.size());
			}
		 }

		if(solve(frame-1,assign,cube,Q)){

			return true;
		}else if (!solvers[frame-1]->okay()){
			//A and B is unsat because B is unsat (so the interpolant is empty in this case)
			solvers[frame]->ok=false;
			return false;
		}else{

			for(int i =0;i<cube.size();i++){
				Lit l = cube[i];
				Lit c = mkLit(var(l)-offset,sign(l));
				cube[i]=c;
			}
			if(opt_generalize_during_sat)//the modsat solver doesn't do this, but it probably makes sense for the naive solver to do so
				generalizer->generalize(cube,frame-1);

			S.cancelUntil(0);

			LRef lr = S.buildInterpolantClause(cube);
			S.interpolant.push(lr);

			cube.clear();
		}
	}
	assert(false);//unreachable
	return true;

}
