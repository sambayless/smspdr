/*
 * Ternary.cpp
 *
 *  Created on: 2013-05-01
 *      Author: sam
 */



#include "Ternary.h"
extern "C" {
#include "aiger/aiger.h"
}
#include "Config.h"
using namespace Minisat;

	Ternary::Ternary(aiger * _aig,vec<int> * _priority):aig(_aig),priority(_priority){

		 //inputs were assigned variables i+1+num_latches
		 min_primary_input=1+aig->num_latches;
		 max_primary_input=min_primary_input+aig->num_inputs;

		fanouts.growTo(aig->maxvar+1);
		for(int i=0;i<aig->num_ands;i++){
			aiger_and & a = aig->ands[i];
			int vfor = aiger_lit2var(a.lhs);
			fanouts[aiger_lit2var(a.rhs0)].push(vfor);
			fanouts[aiger_lit2var(a.rhs1)].push(vfor);
		}

		assigns.growTo(aig->maxvar+1);
		preserve.growTo(aig->maxvar+1);


		empty.growTo(aig->maxvar+1);
			property_var= aiger_lit2var( aig->outputs[0].lit);

	 }

	 bool  Ternary::propagate( unsigned lit){
		if(aiger_and * a = aiger_is_and(aig,lit)){
			lbool cur = assigns[aiger_lit2var(lit)];
			int input0 = a->rhs0;
			int var0 = aiger_lit2var(input0);
			lbool assign0 = assigns[var0]^aiger_sign(input0);

			int input1 = a->rhs1;
			int var1 = aiger_lit2var(input1);
			lbool assign1 = assigns[var1]^aiger_sign(input1);

			lbool assign= assign0&& assign1;

			if(toInt(assign)>toInt(l_Undef)){
						assign=l_Undef;
						}

			assigns[aiger_lit2var(lit)]=assign;
			assert((assigns[aiger_lit2var(lit)]==l_False) || (assigns[aiger_lit2var(lit)]==l_True) || (assigns[aiger_lit2var(lit)]==l_Undef));
			return assigns[aiger_lit2var(lit)]!=cur;
		}
		return false;
	}





	bool  Ternary::simulate(int input_var,lbool assign, vec<bool> & preserve ){

		seen.clear();
		seen.growTo(aig->maxvar+1);
		to_update.clear();
		to_update.push(input_var);

		if(preserve[input_var])
			return false;


		seen[input_var]=true;
		lbool old_assign = assigns[input_var];
		if(old_assign==assign){
			return true;
		}

#ifndef NDEBUG
		vec<lbool> start_state;
		assigns.copyTo(start_state);
#endif

		assigns[input_var]=assign;

		for(int i = 0;i<to_update.size();i++){
			int v = to_update[i];
			seen[v]=false;
			vec<int> & fanout = fanouts[v];
			for(int j = 0;j<fanout.size();j++){
				int o = fanout[j];

				if(!seen[o]){
					if(propagate(aiger_var2lit(o))){
						seen[o]=true;
						if(preserve[o]){
							simulate(input_var, old_assign,empty);
#ifndef NDEBUG
							for(int i = 0;i<assigns.size();i++){
								assert(assigns[i]==start_state[i]);
							}
#endif
							return false;
						}
						to_update.push(o);
					}
				}
			}
		}
		return true;
	}




	bool Ternary::reduce(vec<Lit>& latch_assignment,vec<lbool>& primary_input_assignments, vec<Var> & to_preserve){
		if(!opt_ternary){
			return false;
		}
		int sz = latch_assignment.size();

		for(int i = 0;i<assigns.size();i++){
			assigns[i]=l_Undef;
			preserve[i]=false;
		}

		assigns[0]=l_False; //ground literal is constant

		for(int i = 0;i<to_preserve.size();i++){

			Var v =  to_preserve[i];
			if(v==var_Undef){
				assert(to_preserve.size()==1);
				preserve[property_var]=true;
			}else{
				assert(v-1<aig->num_latches);
				//this is a latch
				int latch_num = v-1;
				int lit = aig->latches[latch_num].next;
				preserve[aiger_lit2var(lit)]=true;
			}
		}

		for(int i = 0;i<latch_assignment.size();i++){
			int latch_num = var(latch_assignment[i])-1;
			int lit = aig->latches[latch_num].lit;
			int v = aiger_lit2var(lit);
			assigns[v]=sign(latch_assignment[i])?l_False:l_True;
		}

		for(int i = 0;i<primary_input_assignments.size();i++){

			//assert(in_latches[latch_num]==var(assignment[i]));
			int lit = aig->inputs[i].lit;

			int v = aiger_lit2var(lit);
			assert(assigns[v]==l_Undef);
			assigns[v]=primary_input_assignments[i];
		}

		//ok, now propagate those assignments through the circuit
		for(int i = 0;i<=aig->maxvar;i++){
			int l = aiger_var2lit(i);
			propagate(l);
		}

#ifndef NDEBUG
		static vec<lbool> start_assign;
		assigns.copyTo(start_assign);
#endif

		//from ABC's PDR implementation

		// try removing high priority flops
		if(priority){
			for(int i = 0;i<latch_assignment.size();i++){
				Var v = var(latch_assignment[i]);
				int latch_num = v-1;
				int p = (*priority)[v];
				if(p>0){
					int lit = aig->latches[latch_num].lit;
					simulate(aiger_lit2var(lit),l_Undef,preserve);

				}
			}
		}

		// try removing low priority flops
		for(int i = 0;i<latch_assignment.size();i++){
			Var v = var(latch_assignment[i]);
			int latch_num = v-1;
			int p = priority? (*priority)[v]:0;
			if(p<=0){
				int lit = aig->latches[latch_num].lit;
				simulate(aiger_lit2var(lit),l_Undef,preserve);
			}
		}


		int i,j=0;
		for( i = 0;i<latch_assignment.size();i++){
			Lit l = latch_assignment[i];
			int latch_num = var(l)-1;
			//assert(in_latches[latch_num]==var(l));
			int lit = aig->latches[latch_num].lit;

			lbool assign = assignment(lit);
			if(assign==l_True || assign==l_False){
				latch_assignment[j++]=l;
			}
		}
		latch_assignment.shrink(i-j);

#ifndef NDEBUG

			for(int i = 0;i<preserve.size();i++){
				if(preserve[i]){
					assert(assigns[i]==start_assign[i]);
				}
			}
#endif

		return latch_assignment.size()<sz;
	}

