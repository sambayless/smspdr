/*
 * Generalize.cpp
 *
 *  Created on: 2013-10-07
 *      Author: sam
 */

#include "Generalize.h"

//Replace the solver at the given frame with a fresh one (as ABC does)
void Generalize::recycle(int frame){

	recycle_count++;
	act_vars[frame]=0;
	if(opt_recycle_type>=2 ){

		//instead of deleting the solver, directly clean out the activation clauses from the solver

		static vec<Lit> cl;
		Solver * S = solvers[frame];
		S->cancelUntil(0);
		//S->propagate();
		int i,j = 0;
		for(i = 0;i<S->clauses.size();i++){
			CRef cr = S->clauses[i];
			Clause & c = S->ca[cr];
			bool remove=false;
			bool clean=false;
			for(int k = 0;k<c.size();k++){
				Lit l = c[k];
				Var v = var(l);
				if(v>=maxNormalVars){
					//this is an activation lit; so we can remove this clause
					if(S->value(l)==l_True){
						//then this clause is satisfied, can be removed
						remove=true;
						break;
					}else if (S->value(l)==l_False){
						clean=true;
					}else{
						assert(false);//this shouldn't happen, but it _is_ safe to remove the clause in this case...
						remove=true;
						break;
					}
				}
			}

			if(remove){
				S->removeClause(cr);
			}else if (clean){
				cl.clear();

				for(int j = 0;j<c.size();j++){
					Lit l = cl[j];
					Var v = var(l);
					if(v<maxNormalVars){
						cl.push(l);
					}
				}
				S->removeClause(clean);
				S->addClause(cl);
			}else{
				S->clauses[j++]=cr;
			}
		}
		S->clauses.shrink(i-j);

		j = 0;
		for(i = 0;i<S->learnts.size();i++){
			CRef cr = S->learnts[i];
			Clause & c = S->ca[cr];
			bool remove=false;
			bool clean=false;
			for(int k = 0;k<c.size();k++){
				Lit l = c[k];
				Var v = var(l);
				if(v>=maxNormalVars){
					//this is an activation lit; so we can remove this clause
					if(S->value(l)==l_True){
						//then this clause is satisfied, can be removed
						remove=true;
						break;
					}else if (S->value(l)==l_False){
						clean=true;
					}else{
						assert(false);//this shouldn't happen, but it _is_ safe to remove the clause in this case...
						remove=true;
						break;
					}
				}
			}

			if(remove){
				S->removeClause(cr);
			}else if (clean){
				cl.clear();

				for(int j = 0;j<c.size();j++){
					Lit l = cl[j];
					Var v = var(l);
					if(v<maxNormalVars){
						cl.push(l);
					}
				}
				S->removeClause(clean);
				S->addClause(cl);
			}else{
				S->learnts[j++]=cr;
			}
		}
		S->learnts.shrink(i-j);
		S->watches.cleanAll();

		//remove the activation lits from the trail (ie, remove their from the unit clauses)
		j=0;
		int nq=0;int nlq=0;int nsq=0;
		for(i = 0;i<S->trail.size();i++){
			Lit l = S->trail[i];
			if(var(l)<maxNormalVars){
				S->trail[j++]=l;
			}else{
				if (i<S->qhead){
					nq++;
				}
				if(i<S->level_q){
					nlq++;
				}
				if(i<S->super_qhead){
					nsq++;
				}
			}
		}
		S->trail.shrink(i-j);
		S->qhead-=nq;
		S->level_q-=nlq;
		S->super_qhead-=nsq;
		//ok, now we can safely reuse the remaining activation lits:
		while(S->nVars()>maxNormalVars){
			Var v = S->nVars()-1;
			S->clearVar(v);

		}

		//PDR doesn't just clear the activity vars, it recycles the whole solver, which clears the learnts and heuristics, etc.
		//Emulate that behaviour by clearing the learnts and heuristics:
		if(opt_recycle_type==3 ){
			for(int i = 0;i<S->nVars();i++){
				S->activity[i]=S->rnd_init_act ? S->drand(S->random_seed) * 0.00001 : 0;
				S->polarity[i]=true;
			}
			while(S->learnts.size()){
				CRef cr = S->learnts.last();
				S->learnts.pop();
				S->removeClause(cr);
			}
		}
		if(opt_force_garbage_collect)
			S->garbageCollect();

#ifndef NDEBUG
		if(frame<solvers.size()-1){
			Solver * super = solvers[frame+1];

				for(int i = 0;i<super->nVars();i++){
					if (super->sub_interface(i)){
						if(super->value(i)==l_Undef){
							Lit l = mkLit(i, true);
							Lit sub = S->fromSuper(l);
							int sublev =S->level(var(sub));
							lbool subval = S->value(sub);
							int superlev = S->super_levels[ S->level(var(sub))].decision_level;
							assert( S->value(sub)==l_Undef  );
					}
				}
			}
		}

#endif



		return;
	}

	static vec<Lit> c;
	Solver *SOld = solvers[frame];
	if(opt_print_ca)
		printf("Old ca size is %d\n",  SOld->ca.size());
	Solver * SNew = new Solver(opt_separate_ca? NULL : &SOld->ca);

  	 SOld->cancelUntil(0);
 	SNew->frame=frame;
 		SNew->max_frame=SOld->max_frame;

 		SNew->num_coi_blocked=SOld->num_coi_blocked;
 		SNew->total_assign_lits=SOld->total_assign_lits;
 		SNew->num_ternary_blocked = SOld->num_ternary_blocked;
 		SNew->ternary=SOld->ternary;
 		SNew->latch_coi=SOld->latch_coi;
 		SOld->property_coi.copyTo(SNew->property_coi);

	if(opt_recycle_type==0){

			prepare(*SNew,mgr,in_latches);
			unroll(*SNew,mgr, in_latches, out_latches);

		//copy units
			for(int i = 0;i<SOld->trail.size();i++){
				Lit unit = SOld->trail[i];
				if(var(unit)<SNew->nVars()){
					SNew->enqueue(unit);
				}
			}
			//add interpolant:
		    for(int i = frame+1;i<interpolants.size();i++){
		    	for(int j = 0;j<interpolants[i].size();j++){
		    		LRef lr = interpolants[i][j];
		    		lr.copyTo(c,solvers[i]->ca);
		    		SNew->addClause(c);
		    	}
		    }

	}else if(opt_recycle_type==1){
		while(SNew->nVars() <maxNormalVars)
			SNew->newVar();
		SNew->sub_ninterface=in_latches.size();
		SNew->in_interpolant.growTo(SNew->sub_ninterface+1);
		//copy units
		for(int i = 0;i<SOld->trail.size();i++){
			Lit unit = SOld->trail[i];
			if(var(unit)<SNew->nVars()){
				SNew->enqueue(unit);
			}
		}
		for(int i = 0;i<SOld->clauses.size();i++){
			c.clear();
			Clause & clause = SOld->ca[SOld->clauses[i]];
			for(int j = 0;j<clause.size();j++){
				Lit l = clause[j];
				Var v = var(l);
				if(v>=maxNormalVars){
					c.clear();//if this clause contains an activation variable, don't use it
					break;
				}
				c.push(l);
			}

			if(c.size()){
				SNew->addClause(c);
			}
		}

	}


	//copy remaining interpolant:

	for(int i = 0;i<SOld->interpolant.size();i++){
				c.clear();
	    		LRef lr_old = SOld->interpolant[i];
	    		lr_old.copyTo(c,SOld->ca);
				LRef lr = SNew->buildInterpolantClause(c);
				SNew->interpolant.push(lr);
	}

	SNew->external_interpolant=&interpolants;
	for(int j = 0;j<interpolants[frame].size();j++){
		LRef lr = interpolants[frame][j];
		lr.copyTo(c,SOld->ca);
#ifndef NDEBUG
		SNew->dbg_AddClause(c);
#endif
		LRef lrNew = SNew->buildInterpolantClause(c);
		interpolants[frame][j]=lrNew;
	}
	//SNew->interpolant_size=SOld->interpolant_size;
	//SNew->interpolant_increase=SOld->interpolant_increase;

	if(!SOld->okay())
		SNew->ok=false;

	 solvers[frame]=SNew;

	 if(!opt_use_naive_modular_sat_solver){
		if(frame>0 ){
			SOld->detachSubSolvers();
			solvers[frame-1]->cancelUntil(0);
			SNew->attachSubSolver(solvers[frame-1],offset, out_latches[0], out_latches.last()+1);
		}

		if(frame<solvers.size()-1){
			solvers[frame+1]->cancelUntil(0);
			solvers[frame+1]->detachSubSolvers();
			solvers[frame+1]->attachSubSolver(SNew,offset, out_latches[0], out_latches.last()+1,true);
		}
	 }
	delete(SOld);
	if(opt_print_ca)
		printf("New ca size is %d\n",  SNew->ca.size());
	if(opt_force_garbage_collect){
		SNew->garbageCollect();
		if(opt_print_ca)
				printf("Post collect ca size is %d\n",  SNew->ca.size());
	}
}

bool Generalize::generalize(vec<Lit> & clause, int frame){
	if(clause.size()==1)
		return false;
	Solver *S = getSolver(frame);
	fail_histogram.growTo(in_latches.last()+1);
	int initial_size = clause.size();
	S->cancelUntil(0);
	static vec<Lit> min;
	static vec<int> order;
	order.clear();
	if(opt_sort_when_generalizing>0){
		 sortByPriority(clause, order,frame );
	}else{
		for(int i = 0;i<clause.size();i++)
			order.push(i);
	}

	int num_latches_true=0;
	for (int j = 0; j < clause.size(); j++ )
	{
		num_latches_true+= !sign(~clause[j]);
		clause[j]=~mkLit(var(clause[j])+offset,sign(clause[j]));
	}
	static vec<Lit> ignore;
	//assert(S->solveStrictlyLocal(ignore));

					//there is no need to add not s to frame 0, since the inputs are fully constrained
	Lit activation = (frame>0)  ? (	act_vars[frame]++,mkLit( S->newVar(),false)) : lit_Undef;
	int num_fails=0;
	if(opt_print_gen){
				printf("gen:");
			}
	for (int j = 0; j < order.size() && num_fails<opt_max_generalization_failures; j++ )
	{
		// use ordering
//        i = j;
		int i = order[j];
		assert(!conflictsWithReset(clause));

		// try removing this literal
		//Lit = pCubeMin->Lits[i]; pCubeMin->Lits[i] = -1;
		Lit l = clause[i];
		if(l==lit_Undef)
			continue;
#ifndef NDEBUG
		int tcount = 0;
		for(int i = 0;i<clause.size();i++){
			if(clause[i] != lit_Undef  &&!sign(clause[i]))
				tcount++;
		}
		assert(tcount==num_latches_true);
#endif
		if(!sign(l) && num_latches_true==1){
			continue;//can't remove this without intersecting reset
		}
		clause[i]=lit_Undef;

		if(S->solveRelative(clause,min,activation,offset)){
			clause[i]=l;//can't remove this literal without making the current time frame satisfiable (or without conflicting with the reset state)
			//need to kill this activation lit
			num_fails++;
			if(activation!=lit_Undef){
				S->addClause(activation);
				S = getSolver(frame);
				act_vars[frame]++;
				activation = mkLit( S->newVar(),false) ;
			}
			continue;
		}
		fail_histogram[num_fails]++;
		if(opt_print_gen){
			printf("%d ",num_fails);
		}

		num_fails=0;

		assert(min.size()==clause.size());
		//min.copyTo(clause);
#ifndef NDEBUG
		int d1_num_latches_true=0;
		for (int j = 0; j < clause.size(); j++ )
			d1_num_latches_true+=( (clause[j] != lit_Undef) & !sign(clause[j]));
		assert(num_latches_true==d1_num_latches_true+!sign(l));
#endif

		if(!sign(l))
			num_latches_true--;
		for(int i =0;i<min.size();i++){
			Lit a = min[i];
			Lit o = clause[i];
			if (a==lit_Undef && o!=lit_Undef && !sign(o)){
				if(num_latches_true<=1){
					//cant drop this literal without conflicting with reset
					clause[i]=o;
				}else{
					num_latches_true--;
					clause[i]=a;
				}
			}else{
				clause[i]=a;
			}
		}

		assert(!conflictsWithReset(clause));
#ifndef NDEBUG
		int d_num_latches_true=0;
		for (int j = 0; j < clause.size(); j++ )
			d_num_latches_true+=( (clause[j] != lit_Undef) & !sign(clause[j]));
		assert(num_latches_true==d_num_latches_true);
#endif
	}
	fail_histogram[num_fails]++;
	if(opt_print_gen){
		printf("%d\n",num_fails);
	}
	num_gen++;
	if(opt_max_generalization_failures >-1 && num_fails>=opt_max_generalization_failures){
		num_fail_blocked++;
	}

	if(activation!=lit_Undef){
		S->addClause(activation);
	}

	assert(!conflictsWithReset(clause));
	//ok, now drop all the undef's from the lit
	int i,j = 0;
	for(i = 0;i<clause.size();i++){
		if(clause[i]!=lit_Undef)
			clause[j++]=~mkLit(var(clause[i])-offset,sign(clause[i]));
	}
	clause.shrink(i-j);
	return clause.size()< initial_size;

}


void Generalize::sortByPriority(const vec<Lit> & cube, vec<int> &order, int frame)
{

	order.clear();
    // initialize variable order
    for (int i = 0; i < cube.size(); i++ )
    	order.push(i);
    for (int i = 0; i < cube.size()-1; i++ )
    {
        int best_i = i;
        for (int j = i+1; j < cube.size(); j++ )
//            if ( pArray[j] < pArray[best_i] )
        	if(opt_sort_when_generalizing==1){
				if(priority[var(cube[j])] < priority[var(cube[best_i])])
					 best_i = j;
        	}else if(opt_sort_when_generalizing==2){
				if(priority[var(cube[j])] > priority[var(cube[best_i])])
					 best_i = j;
        	}else if(opt_sort_when_generalizing==3){
				if(solvers[frame]->activity[var(cube[j])] < solvers[frame]->activity[var(cube[best_i])])
					 best_i = j;
        	}else if(opt_sort_when_generalizing==4){
				if(solvers[frame]->activity[var(cube[j])] > solvers[frame]->activity[var(cube[best_i])])
					 best_i = j;
        	}

        int temp = order[i];
        order[i] = order[best_i];
        order[best_i] = temp;
    }


}
