/*
 * ITPDebug.cpp
 *
 *  Created on: 2012-10-26
 *      Author: sam
 */

#include "ITPDebug.h"
#include "Aiger.h"
#include <iostream>
using namespace Minisat;

#ifndef NDEBUG
template<class T> static inline T min(T x, T y) { return (x < y) ? x : y; }
template<class T> static inline T max(T x, T y) { return (x > y) ? x : y; }

void dbg_print(const vec<vec<Lit> > & interpolant){
	std::cout<<"{";
	for(int i = 0;i<interpolant.size();i++){
		const vec<Lit> & c = interpolant[i];
		std::cout<<" (";
		for(int j = 0;j<c.size();j++){
			std::cout<<dimacs(c[j])<<",";
		}
		std::cout<<"),";
	}
	std::cout<<"}\n";
}

bool dbg_checkApprox( aiger * aig, int k,const Minisat::Solver & S){
	return true;
}

//Adds clauses to the solver such that if the solver is NOT a subset of toCheck, then the returned literal is TRUE
//To check is a list of clauses to check, separated by Lit_Undefs
Lit dbg_constructSubsetProperty(Solver & solver, const vec<Lit>  & toCheck, int offset = 0){
	//construct a circuit that is satisfied only if at least one of these clauses is _not_ satisfied
	//assert(solver.dbg_Solve());
	int originalVars = solver.nVars();
	static vec<Lit> negated_interpolant;
	negated_interpolant.clear();

	for(int i = 0;i<toCheck.size();){
		Var g = solver.newVar();
		assert(!solver.super_interface(g));
		assert(toCheck[i]!=lit_Undef);
		Lit l = lit_Undef;
		while((l = toCheck[i++]) != lit_Undef ){
			assert(i<=toCheck.size());
			assert(var(l)<originalVars);
			Var v= var(l);
			v+=offset;
			assert(v<originalVars);

			//if this literal in c is satisfied, then this OR-gate (g)'s output is true
			solver.addClause(~(mkLit(v,sign(l))),mkLit(g,false));
			solver.num_irrelevant++;//don't count the subset checking clauses here
			//the assignment is not in the previous interpolant if at least one OR-gate is false
		}
		negated_interpolant.push(mkLit(g,true));
		assert(toCheck[i-1]==lit_Undef);

	}
	//assert(solver.dbg_Solve());


	Var g=solver.newVar();
	//ok now add an OR gate over the negated interpolant
	//if ALL the inputs are false, then so is the output.
	negated_interpolant.push(mkLit(g,true));

	solver.addClause(negated_interpolant);
	solver.num_irrelevant++;//don't count the subset checking clauses here
	negated_interpolant.clear();
//	assert(solver.dbg_Solve());

	return (mkLit(g,false));//assume that at least one of the previous constraints is violated
}

bool dbg_checkInterpolant(aiger * aig,int from, const Minisat::vec<Minisat::Solver*> solvers,const Minisat::vec<Minisat::vec<Minisat::LRef> >& allInterpolants){
/*
	for(int i = from;i<allInterpolants.size();i++){
		for(int j = 0;j<allInterpolants.size();j++){
			LRef lr = allInterpolants[j];
			full_interpolant.push();
			vec<Lit> & to = full_interpolant.last();
			lr.copyTo(to, solvers[i]->ca);
		}
	}*/
}

bool dbg_checkInterpolantSafety(aiger * aig,const vec<vec<Lit> > & all_interpolants,int k){
	Solver  S;
	vec<Var> in_latches;
	vec<Var> out_latches;

	prepare(S,aig,in_latches);
	Lit safety=lit_Undef;
	for(int i = 0;i<k;i++){

		safety = unroll(S,aig, in_latches, out_latches);
		vec<Lit> assumptions;
		assumptions.push(safety);

		if(S.solve(assumptions)){
			return true;
		}
		in_latches.clear();
		out_latches.copyTo(in_latches);
		out_latches.clear();
	}

	assert(S.solve());
	for(int i = 0;i<all_interpolants.size();i++){
		S.addClause(all_interpolants[i]);
	}
	assert(S.solve());
	assert(!S.solve(safety));

	return true;
}

bool dbg_inductiveInterpolant(aiger * aig, const Minisat::vec<Minisat::Solver*>  &solvers,const Minisat::vec<Minisat::vec<Minisat::LRef> >& allInterpolants ){

	//first, check that each interpolant really is sufficient to prevent reaching the property in the next step
#ifdef CHECK_INTERPOLANT

	vec<vec<Lit> > full_interpolant;
	for(int i = allInterpolants.size()-1;i>=0;i--){
		for(int j = 0;j<allInterpolants[i].size();j++){
			LRef lr = allInterpolants[i][j];
			full_interpolant.push();
			vec<Lit> & to = full_interpolant.last();
			lr.copyTo(to, solvers[i]->ca);
		}
		//first check that this interpolant is strong enough to prevent us from getting to the property after k-i steps
		assert(dbg_checkInterpolantSafety(aig,full_interpolant,allInterpolants.size() -i));
	}

#endif
	return true;
}

bool dbg_inductiveInterpolant(aiger*aig,int interpolantToCheck,const vec<vec<Lit> > & all_interpolants){

	{

		vec<Var> in;
		vec<Var> out;
		Solver *checker2 = new Solver();
		prepare(*checker2,aig,in);
		vec<Lit> ignore;
		ignore.clear();
		//zero(*checker2,aig,in,ignore);
		checker2->allow_super_units=false;
		Var originalIn = in[0];
		//int toCheck2 =interpolant_to_check+1;
		Lit safety =  unroll(*checker2,aig, in, out);
		assert( checker2->solve());
		checker2->addClauses(all_interpolants[interpolantToCheck]);
	//	checker2->addClauses(solvers[toCheck]->interpolant);

		assert( checker2->solve());
		for(int i = 0;i<= interpolantToCheck;i++){
			Lit isNotSubset = dbg_constructSubsetProperty(*checker2, all_interpolants[i],out[0]-in[0]);
			checker2->addClause(isNotSubset);
		}
		bool expect = checker2->solve();

		assert(!expect);
	}
	return true;
}

bool dbg_checkApprox( aiger * aig,int k, const Minisat::vec<Minisat::Lit>  & interpolant){
	if(k==0)
			return true;
	Solver S;
	vec<Var> sin;
	vec<Var> sout;

	prepare (S, aig, sin);
	int prev_constraint_size = zero(S,aig,sin);
	Lit safety = unroll(S,aig,k, sin, sout);
	int offset = sout[0]-sin[0];
	vec<vec<Lit> > clauses;
	S.dbg_copyConstraints(clauses);

	vec<vec<Lit> > inter;

	static vec<Lit> c;
	c.clear();
	for(int i = 0;i<interpolant.size();i++){

			Lit l = interpolant[i];
			if(l==lit_Undef){
				assert(c.size());
				inter.push();
				c.copyTo(inter[inter.size()-1]);
				c.clear();
			}else{
				Var v = var(l) + offset;
				bool found = false;
				for(int j = 0;j<sout.size();j++){
					if(v==sout[j])
						found=true;
				}

				assert(found);
				while(S.nVars()<=v)
					assert(false);
				c.push(mkLit(v,sign(l)));
			}
		}

	assert(dbg_is_subset(clauses,inter, S.nVars()));

	return true;

}

bool dbg_checkInterpolant( aiger * aig, int k,const vec<vec<Lit> > & interpolant){
	Solver S;
	static vec<Lit> a;
	vec<Var> sin;
	vec<Var> sout;

	prepare (S, aig, sin);
	int maxIn = 1;
	int minIn = S.nVars();
	for(int i = 0;i<sin.size();i++){
		if (sin[i] > maxIn ){
			maxIn = sin[i];
		}
		if (sin[i] < minIn ){
			minIn = sin[i];
		}
	}

	Lit safety = unroll(S,aig,k, sin, sout);

	for(int i =0;i<interpolant.size();i++){
		const vec<Lit> & c = interpolant[i];
		for(int j = 0;j<c.size();j++){
			Var v = var(c[j]);
			assert(v<S.nVars());
			assert(v>=minIn);
			assert(v<=maxIn);
		}
		S.addClause(c);
	}

	a.push(safety);
	bool base =  S.solve();
	bool prop =  S.solve(a);
	assert(base);
	assert(!prop);
	assert(S.okay());
	return true;
}



//The new over approximation is a subset of the old one if it is not possible to satisfy the new over approximation while also dis-satisfying the old one
bool dbg_is_subset(const vec<vec<Lit> > & interpolant,const vec<vec<Lit> > & previous_interpolant, int n){
	Solver SInt;//temporary solver which we will discard soon, just to test if the new interpolant contains the old one
	for(int i = 0;i<n;i++)
		SInt.newVar();

	for(int i = 0;i<interpolant.size();i++){
		SInt.addClause(interpolant[i]);
	}

	//construct a circuit that is satisfied only if at least one of these clauses is _not_ satisfied
	int originalVars = SInt.nVars();
	static vec<Lit> negated_interpolant;
	negated_interpolant.clear();
	for(int i = 0;i<previous_interpolant.size();i++){
		const vec<Lit> & c = previous_interpolant[i];
		int g = SInt.newVar();
		assert(c.size());
		for(int j = 0;j<c.size();j++){
			assert(var(c[j])<n);
			//if this literal in c is satisfied, then this OR-gate (g)'s output is true
			SInt.addClause(~c[j],mkLit(g,false));
		}
		//the assignment is not in the previous interpolant if at least one OR-gate is false
		negated_interpolant.push(mkLit(g,true));
	}
	SInt.addClause(negated_interpolant);
	negated_interpolant.clear();
	return !SInt.solve();
}

bool dbg_is_subset(const Minisat::Solver & current,const Minisat::Solver & previous){
	//investigate better ways to do this later.
	vec<vec<Lit> > curVec;
	vec<vec<Lit> > prevVec;

	current.dbg_copyConstraints(curVec);
	previous.dbg_copyConstraints(prevVec);


	return dbg_is_subset(curVec,prevVec, max(current.nVars(), previous.nVars()));
}

bool dbg_checkBMC( aiger * aig, int k){
#ifndef DBG_CHECK_BMC
	return false;
#endif
	Solver  S;


		vec<Var> s0in;
		vec<Var> s0out;


		prepare (S, aig, s0in);

		zero(S,aig,s0in);

		if(!S.solve())
			return false;

		for(int i = 0;i<k;i++){

			Lit safety = unroll(S,aig, s0in, s0out);
			vec<Lit> assumptions;
			assumptions.push(safety);

			if(S.solve(assumptions)){
				return true;
			}
			s0in.clear();
			s0out.copyTo(s0in);
			s0out.clear();
		}
		return false;

	return false;
}

bool 	dbg_checkBMC( aiger * aig, int k,const vec<Var> & sOut,const Minisat::Solver & S){
	vec<vec<Lit> >  approx;
	S.dbg_copyConstraints(approx);
	return dbg_checkBMC(aig,k,sOut,approx);
}


bool 	dbg_checkBMC(aiger * aig, int k,const vec<Var> & sOut, const Minisat::vec<Minisat::Lit>  & interpolant){
#ifndef DBG_CHECK_BMC
	return false;
#endif
	Solver  S;
	vec<Var> s0in;
	vec<Var> s0out;
	prepare(S,aig,s0in);
	sOut.copyTo(s0in);
	static vec<Lit> c;
	c.clear();
	Var max_v=0;
	for(int i = 0;i<interpolant.size();i++){

		Lit l = interpolant[i];
		if(l==lit_Undef){
			assert(c.size());
			S.addClause(c);
			c.clear();
		}else{
			Var v = var(l);
			if(v>max_v)
				max_v=v;
			while(S.nVars()<=v)
				S.newVar();
			c.push(l);
		}
	}



	if(!S.solve())
		return false;

	for(int i = 0;i<k;i++){

		Lit safety = unroll(S,aig, s0in, s0out);
		vec<Lit> assumptions;
		assumptions.push(safety);
		Var v= var(safety);

		assert(S.solve());


		if(S.solve(assumptions)){
			return true;
		}
		s0in.clear();
		s0out.copyTo(s0in);
		s0out.clear();
	}



	return false;
}

bool 	dbg_checkBMC(aiger * aig, int k,const vec<Var> & sOut, const Minisat::vec<Minisat::vec<Minisat::Lit> > & interpolant){
	Solver  S;
	vec<Var> s0in;
	vec<Var> s0out;
	prepare(S,aig,s0in);
	sOut.copyTo(s0in);

	for(int i = 0;i<interpolant.size();i++){
		const vec<Lit> & c = interpolant[i];
		for(int j = 0;j<c.size();j++){
			Var v = var(c[j]);
			while(S.nVars()<=v)
				S.newVar();
		}
		S.addClause(interpolant[i]);
	}


	if(!S.solve())
		return false;

	for(int i = 0;i<k;i++){

		Lit safety = unroll(S,aig, s0in, s0out);
		vec<Lit> assumptions;
		assumptions.push(safety);

		if(S.solve(assumptions)){
			return true;
		}
		s0in.clear();
		s0out.copyTo(s0in);
		s0out.clear();
	}
	return false;
}

bool dbg_constraints( Minisat::Solver & S,Minisat::Lit safety){

	bool res = S.dbg_proveUnit(~safety);
	assert(res);
	return true;
}

#endif
