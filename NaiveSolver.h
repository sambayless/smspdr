/*
 * NaiveSolver.h
 * This implements the 'naive,' unoptimized modular SAT solver from the paper.
 *
 *  Created on: 2013-10-07
 *      Author: sam
 */

#ifndef NAIVESOLVER_H_
#define NAIVESOLVER_H_



#include "mtl/Vec.h"
#include "core/Solver.h"
#include "mtl/VHeap.h"
#include "Cone.h"
#include "Ternary.h"
#include "mtl/Bitset.h"
#include "core/SolverTypes.h"
#include "Generalize.h"
#include "Cone.h"
namespace Minisat{
class NaiveSolver{


	vec<Solver*> & solvers;
	Generalize * generalizer;
	Ternary * ternary;
	ConeOfInfluence * latch_coi;
	vec<Bitset> naive_coi;
	vec<Bitset> naive_tern;

	Bitset & property_coi;
	vec<vec <Lit> > naive_assigns;
	vec<Var> & in_latches;
	vec<Var> & out_latches;


	vec<Var> ternary_preserve;
	vec<lbool>ternary_inputs;
	vec<Lit> ternary_assign;


	int offset;
	Lit negatedProperty;
public:
	NaiveSolver(vec<Solver*> & _solvers,Generalize*_generalizer,Ternary *_ternary,ConeOfInfluence * _latch_coi,Bitset & _property_coi,vec<Var>& _in_latches,vec<Var>& _out_latches,Lit _negatedProperty):solvers(_solvers),generalizer(_generalizer),ternary(_ternary),latch_coi(_latch_coi),property_coi(_property_coi),in_latches(_in_latches),out_latches(_out_latches),negatedProperty(_negatedProperty){
		offset = 0;
		if(out_latches.size()){
			offset=out_latches[0]-in_latches[0];
		}
	}

	bool solve(int frame,  vec<Lit> & assumptions, vec<Lit> & cube, VHeap<TCube> * Q);
};

};


#endif /* NAIVESOLVER_H_ */
