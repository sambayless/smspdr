#ifndef DEBUG_H_
#define DEBUG_H_


#include "core/Solver.h"
#include "Config.h"
using namespace Minisat;




#ifndef NDEBUG






#else

inline void Solver::print(Lit l){

}
//These inline definitions should optimize out

inline void Solver::printTrail(){
#ifdef REPORT_TRAIL_SUB
/*	 if((*global_it)<68){
		 return;
	 }
	 if(solver_num!=0)
		 return;*/
		 printf("SUB? TRAIL:" );
	 	int lev = decisionLevel();
	 	int nextI= lev>0? trail_lim[lev-1]:0;
	 	for(int i = trail.size()-1;i>=0;i--)
	 	{
	 		Lit l = trail[i];
	 		int d =dimacs(l);
	 		if(i==qhead)
	 	    	printf("Q:");

	 		if(global(var(l))){
	 			assert(sms->hasLocalInterfaceVariable(var(l)));

	 			printf("%d{G}",d);
	 		}else
	 			printf("%d",d);


	 		if(level(var(l))!=lev){
	 		 			printf("@L%d,",level(var(l)));
	 		 		}else
	 		 			printf(", ");
	 		if(i==nextI || i==0)
	 		{
	 			printf("L(%d) ",level(var(l)));
	 			lev--;
	 		     nextI=lev>0? trail_lim[lev-1]:0;
	 		}
	 	}

	 	printf("\n");
#endif
}
/*
inline bool Solver::dbg_correctVsidsDecisions(int after_level){
	return true;
}
*/


inline void Solver::dbg_AddLocalClause(const Clause & c){

}

inline void Solver::printVector(vec<Lit> &v){
}


inline void Solver::dbg_AddClause(const vec<Lit> & clause){

}
inline void Solver::dbg_AddLocalClause(const vec<Lit> & clause){

}
inline void Solver::dbg_exportClauses(Solver * const to, int solverNum){

}
inline bool Solver::dbg_Solve(){
	 return true;
}
inline void Solver::dbg_AddSubClause(const vec<Lit> & clause){

}

inline vec<vec<Lit> > & Solver::dbg_getSubClauses(){
	static vec<vec<Lit> > ignore;
	 return ignore;
}
inline void Solver::dbg_AddInterfaceClause(const vec<Lit> & clause){
	 return;
}
inline int Solver::dbg_nextFreeSubVar(){
	 return true;
}

inline bool Solver::dbg_proveInterfaceAssignment(){
	 return true;
}
inline bool Solver::dbg_proveLocalAssignment(){
	 return true;
}
inline bool Solver::dbg_proveAssignment(){
	 return true;
}
 //Prove that the whole trail is correct, given the decision variables.
inline bool Solver::dbg_proveWholeTrail(){
	 return true;
 }
 //Prove that this literal follows from unit propagation on the decisions made up to it's level (even if that level is lower than the solvers current decision level)
inline bool Solver::dbg_proveUnitPropagation(Lit l){
	 return true;
 }
inline bool Solver::dbg_checkInterpolant(){
	 return true;
 }
inline  bool Solver::dbg_checkConflict(int level, bool expected){
	 return true;
 }
 //Prove that this learnt clause is correct (ie, that it is redundant in the original CNF)
inline  bool Solver::dbg_proveLearntClause(const vec<Lit> & clause){
	 return true;
 }
inline  bool Solver::dbg_proveLearntSubClause(const vec<Lit> & clause){
	 return true;
 }
inline  bool Solver::dbg_conflicting(const vec<Lit> & clause){
	 return true;
 }
inline  bool Solver::dbg_proveUnit(Lit p){
	 return true;
 }

 //Add this clause to the debug solver's CNF.
inline  bool Solver::dbg_checkEarlierOnTrail(Lit q,int index){
	 return true;
 }

inline  bool Solver::dbg_checkPartialLearnt(int index,vec<char> & seen,vec<Lit> & out_learnt){
	 return true;
 }




inline  bool Solver::dbg_proveLearntSuperClause(const vec<Lit> & clause){
	 return true;
 }

#endif


#endif /* DEBUG_H_ */
