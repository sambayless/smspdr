#include "Debug_mini.h"

#ifndef NDEBUG
#ifdef DEBUG_MINI

#include "minilib/Minisat_Solver.h"

DbgMini::DbgMini(){
	proverPtr = new mini::Solver();
	unit_prop_proverPtr = new mini::Solver();
}
DbgMini::~DbgMini(){

	delete((mini::Solver*)proverPtr);
	delete((mini::Solver*) unit_prop_proverPtr);
}
mini::Lit fromDimacs(int v)
{
	assert(v!=0);
	  int      var = abs(v)-1;

	   return ( (v > 0) ? mini::Lit(var) : ~ (mini::Lit(var)) );
}

bool DbgMini::proverSolve(int * assumptions, int sz){
	mini::Solver & prover = *(mini::Solver*)proverPtr;
	mini::Solver &  unit_prop_checker= *(mini::Solver*) unit_prop_proverPtr;

	static mini::vec<mini::Lit> c;

		c.clear();
		for(int i = 0;i<sz;i++)
		{
			int nv = prover.nVars();
			int l = assumptions[i];
			while(abs(l)>=prover.nVars())
				prover.newVar();
			assert(abs(assumptions[i]) <= prover.nVars());
			c.push(fromDimacs ( assumptions[i]));
		}
		return prover.solve(c);
}
int DbgMini::nVars(){
	mini::Solver & prover = *(mini::Solver*)proverPtr;
	return prover.nVars();
}

void DbgMini::addClauseToProver(int * lits, int sz){
	mini::Solver & prover = *(mini::Solver*)proverPtr;
	mini::Solver &  unit_prop_checker= *(mini::Solver*) unit_prop_proverPtr;
	static mini::vec<mini::Lit> c;
	c.clear();
	for(int i = 0;i<sz;i++)
	{
		int nv = prover.nVars();
		int l = lits[i];
		while(abs(l)>=prover.nVars())
			prover.newVar();
		while(abs(l)>=unit_prop_checker.nVars())
			unit_prop_checker.newVar();
		assert(abs(lits[i]) <= prover.nVars());
		c.push(fromDimacs ( lits[i]));
	}
	unit_prop_checker.addClause(c);
	prover.addClause(c);
}

bool DbgMini::proveClause(int * lits, int sz){
	mini::Solver & prover = *(mini::Solver*)proverPtr;
	mini::Solver &  unit_prop_checker= *(mini::Solver*) unit_prop_proverPtr;
	//p.clear();

	static mini::vec<mini::Lit> c;
	c.clear();
	for(int i = 0;i<sz;i++)
	{
		int nv = prover.nVars();
		int l = lits[i];
		while(abs(l)>=prover.nVars())
			prover.newVar();
		assert(abs(lits[i]) <= prover.nVars());
		c.push(~ fromDimacs ( lits[i]));
	}

	return ! prover.solve(c);

}

//Returns true if unit propagation ALONE on the ORIGINAL cnf would detect a conflict with these assumptions
bool DbgMini::checkConflict(int * assumptions, int sz){
	mini::Solver & prover = *(mini::Solver*)proverPtr;
	mini::Solver &  unit_prop_checker= *(mini::Solver*) unit_prop_proverPtr;
	static mini::vec<mini::Lit> c;
	c.clear();
	for(int i = 0;i<sz;i++)
	{
		int nv = prover.nVars();
		int l = assumptions[i];
		while(abs(l)>=unit_prop_checker.nVars())
			unit_prop_checker.newVar();
		assert(abs(assumptions[i]) <= unit_prop_checker.nVars());
		c.push(fromDimacs ( assumptions[i]));
	}

	return  !unit_prop_checker.solve(c,true);

}


bool DbgMini::proveUnitPropComplete(int * assumptions, int sz,bool(*ignore)(int)){
	mini::Solver & prover = *(mini::Solver*)proverPtr;
	mini::Solver &  unit_prop_checker= *(mini::Solver*) unit_prop_proverPtr;
	static mini::vec<mini::Lit> c;
	c.clear();
	for(int i = 0;i<sz;i++)
	{
		int nv = prover.nVars();
		int l = assumptions[i];
		while(abs(l)>=unit_prop_checker.nVars())
			unit_prop_checker.newVar();
		assert(abs(assumptions[i]) <= unit_prop_checker.nVars());
		c.push(fromDimacs ( assumptions[i]));
	}
	if(!unit_prop_checker.okay())
		return true;
	bool result=  unit_prop_checker.solve(c,true,false,ignore);
	if(!unit_prop_checker.okay())
		return true;
	return result;
}

#endif

#endif
