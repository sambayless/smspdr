#ifndef DEBUG_MINI_H_
#define DEBUG_MINI_H_

#include "Config.h"
class DbgMini{
protected:
#ifdef DEBUG_MINI
	void * proverPtr;
	void * unit_prop_proverPtr;

#endif
public:
	DbgMini();
	~DbgMini();
	//The clause is an array of literals, using dimacs numbering (but not 0 terminated).
	void addClauseToProver(int * lits,int sz);

	//Returns true if the clause described by the literals is redundant in the solver (ie, if it is a valid learnt clause).
	bool proveClause(int * lits,int sz);
	bool proverSolve(int * assumptions, int sz);
	bool checkConflict(int * assumptions, int sz);
	int nVars();

	bool proveUnitPropComplete(int * assumptions, int sz,bool(*ignore)(int)=0);
	//If we are NOT using the minilib debug library, then the definitions are empty, and inline - they should get optimized out by the compiler.
};

#ifndef DEBUG_MINI

inline DbgMini::DbgMini(){

}
inline DbgMini::~DbgMini(){

}
inline void DbgMini::addClauseToProver(int * lits, int sz){

}

inline bool DbgMini::proveClause(int * lits, int sz){
	return true;
}
inline bool DbgMini::proverSolve(int * assumptions, int sz){
	return true;
}
inline bool DbgMini::checkConflict(int * assumptions, int sz){
	return true;
}


inline bool DbgMini::proveUnitPropComplete(int * assumptions, int sz, bool(*ignore)(int)){
	return true;
}
#endif

#endif /* DEBUG_MINI_H_ */
