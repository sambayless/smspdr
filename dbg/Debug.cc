#include "Debug.h"

#ifndef NDEBUG

bool Solver::dbg_proveWholeTrail()
{
#ifdef PROVE_TRAIL

if(iteration<=0)
	 return true;
	static vec<int> tr;
	tr.clear();
	for(int l = 0;l<trail.size();l++)
	{
		Lit p = trail[l];
		if(reason(var(p))==CRef_Undef || ca[reason(var(p))].isSolver() ){
			tr.push(dimacs(p));
		}else{
			tr.push(dimacs(~p));
			assert( dbg_sub_mini->proveClause((int*) tr,tr.size()));
			tr.pop();
		}
	}


#endif
	return true;
}
bool Solver::dbg_checkValidSuperReason(Lit sub, Lit super){
	/* static vec<Lit> local_sub_reason;
	 local_sub_reason.clear();

	 //Can't call this, its not safe in debug mode - it can change the state of the solver!!
	////analyzeFinal(sub,local_sub_reason);


	 assert(value(sub)==l_True);
	 static vec<Lit> sub_reason;
	 sub_reason.clear();*/
	 //dbg_super->dbg_getClause(super,sub_reason);

	 return true;
}

void Solver::dbg_getClause(Lit p, CRef r, vec<Lit> & reason){
	reason.clear();
	Clause & c = ca[r];
	if(!c.isTheoryMarker()){

		for(int i = 0;i<c.size();i++){
			reason.push(c[i]);
		}
	}else{
		assert(p!=lit_Undef);
		int solverNum = c.size();
		dbg_buildSubReason(p,solverNum,reason);
	}
}

void Solver::dbg_buildSubReason(Lit p, int subSolver, vec<Lit> & reason){
	assert(subSolver);
	Lit localP =subSolvers[subSolver]->fromSuper(p);
	assert(subSolvers[subSolver]->okay());

	reason.clear();
	subSolvers[subSolver]->analyzeFinal(localP, reason);
	assert(dbg_proveLearntSubClause(reason));
	assert(reason[0]==localP);

	for(int i = 0;i<reason.size();i++){
				Lit l = subSolvers[subSolver]->toSuper(reason[i]);
				reason[i] = l;
				assert(var(reason[i]) < nVars());
		}
}


bool Solver::dbg_proveUnit(Lit e)
{
	static vec<Lit> c;
	c.clear();
	c.push(e);
	return dbg_proveLearntClause(c);
}

 bool Solver::dbg_proveUnitPropagation(Lit e)
{
#ifdef PROVE_TRAIL

	if(iteration<=0)
		 return true;
	static vec<int> tr;
	if(reason(var(e))==CRef_Undef){
		return true;
	}
	if(ca[reason(var(e))].isSolver()){
			return true;
		}
	tr.clear();
	for(int i = 0;i<trail.size();i++){
		Lit d = trail[i];
		if(trail[i]==e){
			break;
		}
		tr.push(dimacs(~d));
	}

	tr.push(dimacs(e));
	assert( dbg_sub_mini->proveClause((int*) tr,tr.size()));
#endif
#ifdef DEBUG_LOCAL
	if(reason(var(e))!=CRef_Undef &&  level(var(e))>0){
		static vec<Lit> r;
		r.clear();
		dbg_getClause(e, reason(var(e)),r);
		for(int i = 0;i<r.size();i++){
			Lit l = r[i];
			if(l!=e){
				assert(value(l)==l_False);
				assert(level(var(l))<=level(var(e)));
			}
		}
	}
#endif

	return true;
}

 bool Solver::dbg_checkReasons(){
	 if(dbg_super){
		 if(decisionLevel()>0){
			 int d = decisionLevel();

			// int s = super_levels.size();
			// assert(s==d+1);

			 for(int i = 0;i<trail.size();i++){
				 Lit l = trail[i];
				 if(level(var(l))>0){
					 if (reason(var(l))==CRef_Undef){
						 Lit p = toSuper(l);
						 assert(dbg_super->value(p)==l_True);

						 int sl = dbg_super->level(var(p));
						 assert(sl==super_levels[level(var(l))].decision_level);
					 }
				 }
			 }

		 }
	 }
	 return true;
 }

 //Checks that unit propagation ALONE on the ORIGINAL cnf doesn't detect a conflict with these assumptions.
 bool Solver::dbg_checkConflict(int lev, bool expected){
#ifdef PROVE_CONFLICTS
	 	 if(iteration<=0)
	 		 return true;
		static vec<int> tr;
		tr.clear();

		for (int i = 0;i< trail.size();i++)
		{
			Lit cur = trail[i];
			tr.push(dimacs(cur));
		}
		bool no_sub_res = dbg_mini.checkConflict((int*)tr,tr.size());
		assert(expected== dbg_sub_mini->checkConflict((int*)tr,tr.size()));
#endif
		return true;
 }


bool Solver::dbg_proveLocalAssignment(){
	static vec<int> tr;
	tr.clear();
	if(!dbg_mini.proverSolve(tr,0)){
		return true;
	}
	for (int i = 0;i< trail.size();i++)
	{
		Lit cur = trail[i];
		tr.push( dimacs(cur));
	}
	assert(dbg_mini.proverSolve(tr,tr.size()));
	return true;
}

bool Solver::dbg_subAssignments(){

	for(int i = 0;i<subSolvers.size();i++){
			Solver * subSolver = subSolvers[i];
			static vec<int> tr;
				tr.clear();

				for (int i = 0;i< trail.size();i++)
				{
					Lit cur = trail[i];
					if(sub_interface(cur)){
						Lit sub = subSolver->fromSuper(cur);
						tr.push( dimacs(sub));
					}
				}
				assert(subSolver->dbg_sub_mini->proverSolve(tr,tr.size()));
		}
		return true;
}

bool Solver::dbg_proveAssignment(){
#ifdef PROVE_UNIT_PROP
	static vec<int> tr;
	tr.clear();
	if(!dbg_sub_mini->proverSolve(tr,0)){
		return true;
	}
	for (int i = 0;i< trail.size();i++)
	{
		Lit cur = trail[i];
		tr.push( dimacs(cur));
	}
	assert(dbg_sub_mini->proverSolve(tr,tr.size()));
#endif
	return true;
}

bool Solver::dbg_proveInterfaceAssignment(){

	for(int i = 0;i<subSolvers.size();i++){
		Solver * subSolver = subSolvers[i];
		static vec<int> tr;
		tr.clear();
		if(!subSolver || ! subSolver->dbg_sub_mini->proverSolve(tr,0))
			return true;

		for (int i = 0;i< trail.size();i++)
		{
			Lit cur = trail[i];
			if(sub_interface(cur)){
				tr.push( dimacs(subSolver->fromSuper (cur)));
			}
		}
		assert(subSolver->dbg_sub_mini->proverSolve(tr,tr.size()));
	}
	return true;
}

bool Solver::dbg_checkInterfaceMatches(){

	for(int i = 0;i<subSolvers.size();i++){
		Solver * subSolver = subSolvers[i];
		if(!subSolver || !subSolver->okay())
			return true;
		for (int i = 0;i< trail.size();i++)
		{
			Lit cur = trail[i];
			if(sub_interface(cur)){
				Lit sub = subSolver->fromSuper(cur);
				assert(subSolver->value(sub)==value(cur));
			}
		}
	}
	return true;
}

bool Solver::dbg_proveLearntSuperClause(const vec<Lit> & clause){
#ifdef PROVE_LEARNT
	 if(iteration<=0)
		 return true;
	 if(!dbg_super)
		 return true;

	 int solverNum = 0;
	 for(int i =0;i<super->subSolvers.size();i++){
		 if(super->subSolvers[i]==this)
			 solverNum=i;
	 }
		static vec<int> tr;
		tr.clear();
		if(decisionLevel()>0){
			for(int i =0;i<trail_lim[0];i++)
			{
				tr.push(dimacs(~ trail[i]));
			}
		}else{
			for(int i =0;i<trail.size();i++)
					{
						tr.push(dimacs(~trail[i]));
					}
		}
		for(int i =0;i<clause.size();i++)
		{
			tr.push(dimacs(fromSuper( clause[i])));
		}
		assert( dbg_sub_mini->proveClause((int*) tr,tr.size()));
		//assert( dbg_mini.proveClause((int*) tr,tr.size()));
#endif
 	return true;
}

bool Solver::dbg_conflicting(const vec<Lit> & clause){
	for(int i = 0;i<clause.size();i++){
		Lit l = clause[i];
		assert(value(l)==l_False);
	}
	return true;
}

bool Solver::dbg_proveLearntSubClause(const vec<Lit> & clause){
#ifdef PROVE_LEARNT
	 if(iteration<=0)
		 return true;
		static vec<int> tr;
/*		tr.clear();
		if(decisionLevel()>0){
			for(int i =0;i<trail_lim[0];i++)
			{
				tr.push(dimacs(~trail[i]));
			}
		}else{
			for(int i =0;i<trail.size();i++)
					{
						tr.push(dimacs(~trail[i]));
					}
		}
		for(int i =0;i<clause.size();i++)
		{
			tr.push(dimacs(fromSub(clause[i])));
		}
		assert( dbg_sub_mini->proveClause((int*) tr,tr.size()));*/
#endif
 	return true;
 }

bool Solver::dbg_proveConflict(CRef confl){
	assert(confl!=CRef_Undef);

	Clause & c = ca[confl];
	if(c.isTheoryMarker())
		return true;
	static vec<Lit> cls;
	cls.clear();
	for(int i = 0;i<c.size();i++){
		cls.push(c[i]);
	}
	assert(dbg_proveLearntClause(cls));
	cls.clear();
	for(int i = 0;i<trail.size();i++){
		cls.push(~(trail[i]));//at least one assignment must be false
	}
	assert(dbg_proveLearntClause(cls));
	assert(dbg_proveWholeTrail());
	return true;
}

bool Solver::dbg_proveLocalClause(const vec<Lit> & clause){
#ifdef PROVE_LEARNT
	 if(iteration<=0)
		 return true;
		static vec<int> tr;
		tr.clear();
		if(decisionLevel()>0){
			for(int i =0;i<trail_lim[0];i++)
			{
				tr.push(dimacs(~trail[i]));
			}
		}else{
			for(int i =0;i<trail.size();i++)
					{
						tr.push(dimacs(~trail[i]));
					}
		}
		for(int i =0;i<clause.size();i++)
		{
			tr.push(dimacs(clause[i]));
		}
		assert( dbg_mini.proveClause((int*) tr,tr.size()));
#endif
 	return true;
}

 bool Solver::dbg_proveLearntClause(const vec<Lit> & clause){
#ifdef PROVE_LEARNT
	 if(iteration<=0)
		 return true;
		static vec<int> tr;
		tr.clear();
		if(decisionLevel()>0){
			for(int i =0;i<trail_lim[0];i++)
			{
				tr.push(dimacs(~trail[i]));
			}
		}else{
			for(int i =0;i<trail.size();i++)
					{
						tr.push(dimacs(~trail[i]));
					}
		}
		for(int i =0;i<clause.size();i++)
		{
			tr.push(dimacs(clause[i]));
		}
		assert( dbg_sub_mini->proveClause((int*) tr,tr.size()));
		//assert( dbg_mini.proveClause((int*) tr,tr.size()));
#endif
 	return true;
 }

 bool Solver::dbg_checkEarlierOnTrail(Lit q,int index){
#ifdef PROVE_ANALYSIS
	 for(int i = index;i>=0;i--){
		 Lit l = trail[i];
		 if(l==~q){
			 return true;
		 }
		 if(var(l)==var(q)){
			 //this is an error!
			 assert(false);
			 return false;
		 }
	 }
	 return false;
#endif
	 return true;
 }
 bool Solver::dbg_checkPartialLearnt(int index,vec<char> & seen,vec<Lit> & out_learnt){
#ifdef PROVE_ANALYSIS
	 static vec<Lit> tmp;
	 tmp.clear();
	 //skip the 0th entry in out_learnt, which is a placeholder
	 for(int i = 1;i<out_learnt.size();i++){
			tmp.push(out_learnt[i]);
		 }
	 for(int i = index;i>=0;i--){
		 Lit l = trail[i];
		 if(seen[var(l)]){
			 tmp.push(~l);
		 }
	 }
	 assert(dbg_proveLearntClause(tmp));
#endif

	 return true;
 }
 void Solver::dbg_copyConstraints(vec<vec<Lit> >  & out)const{
	 for(int i =0;i< (trail_lim.size() == 0 ? trail.size() : trail_lim[0]);i++){
	 		out.push();
	 		out[out.size()-1].push(trail[i]);
	 	}

	 	for(int i =0;i<clauses.size();i++){
	 		out.push();
	 		const Clause & c = ca[clauses[i]];
	 		for(int j = 0;j<c.size();j++){
	 			out[out.size()-1].push(c[j]);
	 		}
	 	}

	 	for(int i =0;i<learnts.size();i++){
	 		out.push();
	 		const Clause & c =  ca[learnts[i]];
	 		for(int j = 0;j<c.size();j++){
	 			out[out.size()-1].push(c[j]);
	 		}
	 	}
 }
 bool Solver::dbg_proveClause(Clause & c){
	 bool learnt = c.learnt();
	 static vec<Lit> t;
	 t.clear();
	 for(int i = 0;i<c.size();i++){
		 t.push(c[i]);
	 }
	 return dbg_proveLearntClause(t);
 }
 bool Solver::dbg_proveLocalClause(Clause & c){
	 static vec<Lit> t;
	 t.clear();
	 for(int i = 0;i<c.size();i++){
		 t.push(c[i]);
	 }
	 return dbg_proveLocalClause(t);
 }

 bool Solver::dbg_proveUnitPropComplete(int level){

#ifdef PROVE_UNIT_PROP
		static vec<int> tr;
		tr.clear();
		for (int i = 0;i< trail.size();i++)
		{
			Lit cur = trail[i];
			tr.push(dimacs(cur));
		}
		if( opt_eager_prop && ! disableSubProp && subSolvers.size() && subSolvers[0]->eager_propagation && opt_subsearch <3){
			//
					assert(dbg_sub_mini->proveUnitPropComplete((int*)tr,tr.size()));
		}else{
			assert(dbg_mini.proveUnitPropComplete((int*)tr,tr.size()));
		}

#endif
		return true;
 }



 bool Solver::dbg_checkWatchers(bool check_levels){
#ifdef DEBUG_WATCHERS
	 for(int i = 0;i<clauses.size();i++){
		 CRef clause = clauses[i];
		 Clause & c = ca[clause];

		 if(c.size()==2){
			 for (int j = 0;j<2;j++){
				 bool found = false;
				 Lit watcher = c[j];
				 vec<Watcher>&  wbin  = watchesBin[~watcher];
				 for(int k = 0;k<wbin.size();k++){
					 Lit imp = wbin[k].blocker;
					 if(imp==c[1-j]){
						 found=true;
						 break;
					 }
				 }
				 assert(found);
			 }
		 }else if (c.size()>2){

			 Var w0 = var(c[0]);
			 Var w1 = var(c[1]);
			 for (int j = 0;j<2;j++){
				 bool found = false;
				Lit watcher = c[j];
				vec<Watcher>&  ws  = watches[~watcher];
				for(int k = 0;k<ws.size();k++){
					 CRef     cr        = ws[k].cref;

					 if(cr==clause){
						 found=true;
						 break;
					 }
				}
				assert(found);
			 }

			 if(check_levels){
				 bool is_sat = false;
				 for(int k=0;k<c.size();k++){
					 if(value(c[k])==l_True){
						 is_sat=true;
						 break;
					 }
				}

				 if(!is_sat){
					 int lev0 = level(var(c[0]));
					 int lev1 = level(var(c[1]));
					 //if the clause is not satisfied, then the two watchers must have the highest levels (or be unassigned)
					 for(int k=2;k<c.size();k++){
						 Var v = var(c[k]);
						 lbool a = value(c[k]);
						 int lev = level(var(c[k]));
						 if(value(c[0])==l_False){
							 assert(value(c[k])==l_False);
							 assert(level(var(c[k]))<=lev0);
						 }
						 if(value(c[1])==l_False){
							 assert(value(c[k])==l_False);
							 assert(level(var(c[k]))<=lev1);
						 }
					 }
				 }
			 }
		 }

	 }
#endif
	 return true;
 }

 void Solver::dbg_RefreshSubSolvers(){

	 	 	 dbgclauses.clear();
	 		for(int i = 0;i<dbg_local_clauses.size();i++){
	 			dbgclauses.push();
	 			dbg_local_clauses[i].copyTo(dbgclauses[dbgclauses.size()-1]);
	 		}

			delete(dbg_sub_mini);
			dbg_sub_mini= new DbgMini();

			for (int i = 0;i<dbg_local_clauses.size();i++){
				vec<Lit> & clause = dbg_local_clauses[i];
				dbg_AddSubClause(clause);
			}
			if(subSolvers.size()){
				for(int i = 0;i<subSolvers.size();i++){
					subSolvers[i]->dbg_exported=false;
					subSolvers[i]->dbg_exportClauses(this,i);
				}
			}
/*
			for(int i = 0;i<clauses.size();i++){
					assert(dbg_proveClause(ca[clauses[i]]));
				}
				for(int i = 0;i<learnts.size();i++){
						assert(dbg_proveClause(ca[learnts[i]]));
					}
				for(int i = 0;i<trail.size();i++){
					assert(level(var(trail[i]))==0);
					assert(dbg_proveUnit(trail[i]));
				}*/

 }

/* void dbg_AddSubClause(const Clause & clause){
	 static vec<Lit> t;
		 t.clear();
		 for(int i = 0;i<clause.size();i++){
			 t.push(clause[i]);
		 }
		 dbg_AddSubClause(t);
 }*/
 void Solver::dbg_AddSubClause(const vec<Lit> & clause){
	 for(int i = 0;i<clause.size();i++){
		 Var v = var(clause[i]);
		 if(v>maxSubVar)
			 maxSubVar= v;
	 }
	 dbgclauses.push();

	 clause.copyTo(dbgclauses[dbgclauses.size()-1]);

	static vec<int> tr;
	tr.clear();
	for(int i =0;i<clause.size();i++)
	{
		tr.push(dimacs(clause[i]));
	}
	dbg_sub_mini->addClauseToProver((int*)tr,tr.size());


 }

 vec<vec<Lit> > & Solver::dbg_getSubClauses(){
	 if(!computedDbgClauses && subSolvers.size()){
		 computedDbgClauses=true;
		 for(int i = 0;i<subSolvers.size();i++)
			 subSolvers[i]->dbg_exportClauses(this,i);
	 }
	 return dbgclauses;
 }

 int Solver::dbg_nextFreeSubVar(){
	// return maxSubVar;
#ifdef DEBUG_MINI
	 return dbg_sub_mini->nVars();
#else
	 return 0;
#endif

 }

 bool Solver::dbg_checkInterpolant(){

		{
			vec<int> tr;
			assert(dbg_sub_mini->proverSolve(tr,tr.size()));
			if(dbg_property!=lit_Undef){
				tr.push(dimacs(dbg_property));
				assert(!dbg_sub_mini->proverSolve(tr,tr.size()));
			}
		}


		return true;

 }

 void Solver::dbg_exportClauses(Solver * const to, int subSolverNum){
#ifdef DEBUG_MINI
	 if(!dbg_exported){
		 dbg_exported=true;
	// assert(decisionLevel()==0);
	 static   vec<Lit>  c;
	 int nextVar = to->dbg_nextFreeSubVar()+10000 ;//this is a not good-enough solution to keep the debug solver literal namespace from being incompatible with any later literals that might be added to the main solver.
	 //This obviously will fail if more than 1,000,000 new literals are added to the main solver, but hopefully debugging wont be used for such an instance.
	 vec<vec<Lit> > & dbgcls = dbg_getSubClauses();
	 c.clear();
	 for(int i = 0;i<dbgcls.size();i++){
		 vec<Lit> & clause =dbgcls[i];
		 assert(dbg_proveLearntClause(clause));
		 for(int j = 0;j<clause.size();j++){
			 Lit l = clause[j];
			 Lit p = mkLit(var(l)+nextVar,sign(l));
			 c.push(p);
		 }
		 to->dbg_AddSubClause(c);
		 c.clear();
	 }
	 {
		 vec<int> tr;
			tr.push(dimacs(to->dbg_property));

			//assert(!dbg_sub_mini->proverSolve(tr,tr.size()));
		 tr.clear();
			Lit p = mkLit(var(to->dbg_property)+nextVar,sign(to->dbg_property));
			tr.push(dimacs(p));

	//	assert(!to->dbg_sub_mini->proverSolve(tr,tr.size()));
	 }
	 int maxv = to->dbg_sub_mini->nVars();
	 //tie the register variables together.
	 for(int i = 0;i<to->nVars();i++){
		 if(to->sub_interface(i)){
			 Var super = i;
			 c.clear();

			 Var sub = fromSuper(super);
			 if(subSolverNum ==0){
				 assert(sub<super_max_interface);
			 }else{
				 sub=super;
			 }
			 Var subNew = sub+nextVar;



			 c.push(mkLit(super,true));
			 c.push(mkLit(subNew,false));
			 to->dbg_AddSubClause(c);
			 c.clear();
			 c.push(mkLit(super,false));
			 c.push(mkLit(subNew,true));
			 to->dbg_AddSubClause(c);
			 c.clear();
		 }
	 }

	 {
		 vec<int> tr2;
	/*	 assert(to->dbg_sub_mini->proverSolve(tr2,tr2.size()));
	//	 for(int i = 0;i<to->assumptions.size();i++){
		//	Lit l = to->assumptions[i];
			tr2.push(dimacs(to->dbg_property));
		//}

		 bool solveable = to->dbg_sub_mini->proverSolve(tr2,tr2.size());*/
		//assert(to->dbg_expect_answer== solveable);

		 int a=1;
	 }

		    dbg_super_int=nVars()- to->sub_ninterface;
		    dbg_super_off=to->sub_offset;
		    dbg_super = to;
		 if(subSolverNum==0){

	    //double check the interface lines up correctly!
	    for(int i = 0;i<nVars();i++){
	    	if(super_interface(i)){
	    		Var v = i;
	    		Var superv = toSuper(v);
	    		assert(to->sub_interface(superv));
	    		assert(fromSuper(superv)==v);
	    	}
	    }
	    for(int i = 0;i<to->nVars();i++){
	   	    	if(to->sub_interface(i)){
	   	    		Var v = i;
	   	    		Var subv = fromSuper(v);
	   	    		assert(super_interface(subv));
	   	    		assert(toSuper(subv)==v);
	   	    	}
	   	    }
	 }
	 }
#endif
 }

 void Solver::dbg_AddLocalClause(const Clause & c){
	 static vec<Lit> clause;
	 clause.clear();
	 for(int i = 0;i<c.size();i++){
		 clause.push(c[i]);
	 }
	 dbg_AddLocalClause(clause);
	 clause.clear();
 }

 void Solver::dbg_AddLocalClause(const vec<Lit> & clause){
	 static vec<int> tr;
	tr.clear();
	for(int i =0;i<clause.size();i++)
	{
		tr.push(dimacs(clause[i]));
	}
	dbg_mini.addClauseToProver((int*)tr,tr.size());
	dbg_local_clauses.push();
	clause.copyTo( dbg_local_clauses[dbg_local_clauses.size()-1]);
 }


 void Solver::dbg_AddClause(const vec<Lit> & clause){
	 static vec<int> tr;
	tr.clear();
	for(int i =0;i<clause.size();i++)
	{
		tr.push(dimacs(clause[i]));
	}
	dbg_mini.addClauseToProver((int*)tr,tr.size());
	dbg_local_clauses.push();
	clause.copyTo( dbg_local_clauses[dbg_local_clauses.size()-1]);
	dbg_AddSubClause(clause);
 }

 void Solver::dbg_AddInterfaceClause(const vec<Lit> & clause){
	 static vec<int> tr;

	tr.clear();
	for(int i =0;i<clause.size();i++)
	{
		Var v = var(clause[i]);
		assert(sub_interface(v));
		tr.push(dimacs(clause[i]));
	}
	dbg_sub_mini->proveClause((int*)tr,tr.size());
	dbg_mini.addClauseToProver((int*)tr,tr.size());
	dbg_AddSubClause(clause);
 }

 bool Solver::dbg_proveModel(const vec<lbool> & model){
#ifdef PROVE_UNIT_PROP
	static vec<int> tr;
	tr.clear();
	for(int i =0;i<model.size();i++)
	{
		lbool val = model[i];
		assert(val !=l_Undef);
		tr.push(dimacs(mkLit(i,model[i]==l_False)));
	}

//	if(subSolver){
	//	bool localres = dbg_mini.proverSolve((int*)tr,tr.size());
		bool res = dbg_sub_mini->proverSolve((int*)tr,tr.size());
	 	 assert(res);
	/*}else{
		bool res = dbg_mini.proverSolve((int*)tr,tr.size());
			 assert(res);
	}*/
#endif
 	return true;
 }

 bool Solver::dbg_checkAnswer(bool answer){
	 static vec<int> tr;
		 		tr.clear();
		 		if(decisionLevel()>0){
		 			for(int i =0;i<trail_lim[0];i++)
		 			{
		 				tr.push(dimacs(trail[i]));
		 			}
		 		}else{
		 			for(int i =0;i<trail.size();i++)
		 					{
		 						tr.push(dimacs(trail[i]));
		 					}
		 		}
	 bool res = dbg_sub_mini->proverSolve((int*) tr,tr.size());

	 return answer==res;
 }
 bool Solver::dbg_Solve(){
	 static vec<int> tr;
	 		tr.clear();
	 		if(decisionLevel()>0){
	 			for(int i =0;i<trail_lim[0];i++)
	 			{
	 				tr.push(dimacs(trail[i]));
	 			}
	 		}else{
	 			for(int i =0;i<trail.size();i++)
	 					{
	 						tr.push(dimacs(trail[i]));
	 					}
	 		}
	 bool res = dbg_sub_mini->proverSolve((int*) tr,tr.size());
	 return res;
 }

 bool Solver::dbg_checkCauses(){
#ifdef SUB_CHECK_CAUSES
    static vec<bool> seen_dbg;
    seen_dbg.clear();
    seen_dbg.growTo(nVars());
    for(int i = 0;i<trail.size();i++){
    	Var v = var(trail[i]);
    	seen_dbg[v]=true;
    	assert(value(trail[i])==l_True);
    	CRef cause = reason(v);
    	if(cause==CRef_Undef){

    	}else{
    		Clause & c = ca[cause];
    		for(int j = 0;j<c.size();j++){
    			Var vc = var(c[j]);
    			int lev = level(vc);
    			lbool val = value(c[j]);
    			assert(seen_dbg[vc]);
    		}
    	}
    }
#endif
    return true;
 }

 void Solver::print(Lit l){
#ifdef REPORT

	 printf("%d", dimacs(l));
#endif
 }

 void Solver::printVector(vec<Lit> &v){
	 printf("(");
	 for(int i = 0;i<v.size();i++){
		 print(v[i]);
		 printf(" ");
	 }
	 printf(")\n");
 }

 void Solver::printTrail(){
#ifdef REPORT_TRAIL
/*	 if((*global_it)<68){
		 return;
	 }
	 if(solver_num!=0)
		 return;*/

	 static int total_lines=0;
	 total_lines++;


		 printf("%d: FRAME%d TRAIL: ",total_lines, frame);

	 	int lev = decisionLevel();
	 	int nextI= lev>0? trail_lim[lev-1]:0;
	 	for(int i = trail.size()-1;i>=0;i--)
	 	{
	 		Lit l = trail[i];
	 		int d =dimacs(l);
	 		if(i==qhead)
	 	    	printf("Q:");

	 		if(var(l)==0){
	 			printf("G");//ground
			}
	 		printf("%d",d);
	 		if(dbg_super_int && dbg_super && super_interface(var(l))){
	 			Lit superLit = toSuper(l);
	 			printf("{^%d%s}", dimacs(superLit), (reason(var(l))==CRef_Undef ) ? "*" : ""  );
	 		}
	 		if(sub_interface(l) && reason(var(l))!=CRef_Undef){
	 			Clause & c = ca[reason(var(l))];
				printf("{v%d}", dimacs(subSolvers[0]->fromSuper(l)), c.isSolver()  ? "*" : ""  );
			}



	 		if(level(var(l))!=lev){
	 		 			printf("@L%d,",level(var(l)));
	 		 		}else
	 		 			printf(", ");
	 		if(i==nextI || i==0)
	 		{
	 			printf("L(%d) ",level(var(l)));
	 			lev--;
	 		     nextI=lev>0? trail_lim[lev-1]:0;
	 		}
	 	}

	 	printf("\n");


#endif
 }
#endif
