/*
 * Generalize.h
 * Coordinates and applies inductive generalization for each time frame.
 *
 *  Created on: 2013-10-07
 *      Author: sam
 */

#ifndef GENERALIZE_H_
#define GENERALIZE_H_

#include "mtl/Vec.h"
#include "core/SolverTypes.h"
#include "Aiger.h"
#include "core/Solver.h"
#include <iostream>
namespace Minisat{

class Generalize{



vec<Solver*> & solvers;
aiger * mgr;

//These are shared with the main PDR procedure, which is ugly.
vec<vec<LRef> > & interpolants;
vec<int> & priority;

int num_gen;
int num_fail_blocked;
vec<int> fail_histogram;

vec<int> act_vars;


//Total number of _non_ activity variables
int maxNormalVars;

vec<Var> in_latches;
vec<Var> out_latches;
int offset;
vec<Lit> c;
vec<Lit> tmp_unit;

public:
int recycle_count;

Generalize(vec<Solver*> & _solvers,aiger * aig, vec<vec<LRef> > & _interpolants,vec<int> & _priority, vec<Var> & _in_latches, vec<Var> & _out_latches):solvers(_solvers),mgr(aig),interpolants(_interpolants),priority(_priority){
	 num_gen=0;
	 num_fail_blocked=0;
	 recycle_count=0;
	 _in_latches.copyTo(in_latches);
	 _out_latches.copyTo(out_latches);
	 if(out_latches.size())
		 offset = out_latches[0]-in_latches[0];
	 else
		 offset=0;


}
void recycle(int frame);
bool generalize(vec<Lit> & clause, int frame);
void recycle(bool force=false);
void sortByPriority(const vec<Lit> & cube, vec<int> &order, int frame);

void printHistogram(){
	if(opt_list_gen){
		std::cout<<"Gen: ";
		int max = 0;
		for(int i = 0;i<fail_histogram.size();i++)
			if(fail_histogram[i]>0){
				max = i;
			}
		for(int i = 0;i<max;i++)
			std::cout<< " " << fail_histogram[i];
			std::cout<<"\n";
	}
}

void addTimeFrame(){
	act_vars.push(0);
}
private:
Solver * getSolver(int frame){
	assert(frame>=0 && frame<solvers.size());
	if(opt_recycle>0 && act_vars[frame]>=opt_recycle){
		recycle(frame);
	}

	return solvers[frame];

}
bool conflictsWithReset(vec<Lit> & cube){
	//since reset state is all latches false, we conflict if we aren't assigning at least one latch to true
	for(int i = 0;i<cube.size();i++){
		if(cube[i]!=lit_Undef && !sign(cube[i]))
			return false;
	}
	return true;
}



};
};
#endif /* GENERALIZE_H_ */
