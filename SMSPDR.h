/*
 * ITP.h
 *
 *  Created on: 2012-10-18
 *      Author: sam
 */
#ifndef ITP_H_
#define ITP_H_


#include "Aiger.h"
#include <cstdio>
#include <iostream>
#include "Config.h"
#include "ITPDebug.h"
#include "mtl/Heap.h"
#include "mtl/Queue.h"
#include "mtl/VHeap.h"
#include "mtl/Sort.h"
#include "Cone.h"
#include "Ternary.h"
#include "utils/System.h"
#include "Generalize.h"
#include "NaiveSolver.h"

using namespace Minisat;

class SMSPDR{
public:



aiger * mgr;

/**
 * List of SAT solvers, one for each expanded time frame.
 */
vec<Solver*> solvers;

/**
 * The constraints for each time frame (eg, 'F[]' from PDR),
 * each in the last time frame they occur in (LRef's are really just pointers to the clauses in those respective solvers, or they are unit clauses)
 *
 * The LRefs were intended to avoid duplication of memory for the interpolants between the solvers and PDR, but turned out to be a huge hassle.
 */
vec<vec<LRef> > interpolants;

/**
 * If opt_use_naive_modular_sat_solver is true then the 'naive', unoptimized modular SAT solver will be used instead of the optimized one.
 */
NaiveSolver * naiveSolver;

//These two vectors hold the variables of the input and output latches for a time frame (these will be the same for the solvers in each time frame, because each time frame has its own pool of variables)
vec<Var> in_latches;
vec<Var> out_latches;



//Offset stores the difference between the variable numbers of the input and output latches; it is used to translate between them. Equal to out_latches[0]-in_latches[0]
int offset;

//The negatedProperty property is asserted to be violated if output of the circuit is forced to TRUE.
Lit negatedProperty;


//This vector is used for heuristics on which latches to try removing first in ternary simulation and generalization
vec<int>  priority;

//Applies inductive generalization
Generalize * generalizer;

//Applies Ternary Simulation, as in PDR
Ternary * ternary;

//The property cone of influence is static, and pre-computed.
Bitset property_coi;

//The cone of influence of a combination of latches is computed on the fly using the ConeOfInfluence class
ConeOfInfluence * latch_coi;


/**
 * For memory efficiency, we can optionally have all our SAT solvers share the same clause allocation space.
 */
ClauseAllocator * external_ca;


//Used to force occasional full clause propagation, if both fast_clause_propagation and force_full_propagation_interval are enabled
int next_prop_interval;

//This is the main PDR queue, holding all the bad states that need to be blocked.
VHeap<TCube> Q;

//temporary vectors
vec<char> seen_subsume;
vec<int> seen_subsumed;


//Stats
double total_solve_time;
int total_iterations;
int total_force_prop_cout;
int total_coi_blocked;
int total_assign_lits;
int total_ternary_blocked;

SMSPDR(aiger * aig):mgr(aig),naiveSolver(NULL),generalizer(NULL),ternary(NULL),external_ca(NULL),next_prop_interval(0),total_solve_time(0),total_iterations(0),total_force_prop_cout(0),total_coi_blocked(0),total_assign_lits(0),total_ternary_blocked(0){

}


int depth(){
	return solvers.size()- 1;
}

//Remove subsumed clauses from the interpolant at a frame, starting from entry 'start'.
void removeSubsumed(vec<Lit> & gen, int frame, int start=0){
	seen_subsumed.clear();
	seen_subsumed.growTo(in_latches.last()*2+3); //fix this..

	for(int j = 0;j<gen.size();j++){
		Lit l = gen[j];
		seen_subsumed[toInt(l)]=true;
	}
	uint32_t abstraction = calcAbstraction(gen);

	//check if any clauses in the interpolant for frame are subsumed:
	int j, h = start;
	for(j=start;j<interpolants[frame].size();j++){
		LRef lr =interpolants[frame][j];
		bool subsumed= false;
		if(!lr.isLit()){

			Clause & c = solvers[frame]->ca[lr.cref()];
			if(c.isSubsumed()){
				subsumed=true;
			}else if(gen.size() && c.size()>gen.size() &&  ((abstraction & ~c.abstraction()) == 0)) {
				int count = 0;
				for(int k = 0;k<c.size();k++){// && (gen.size()- count > (c.size()-k) )
					if(seen_subsumed[toInt(c[k])]){
						count++;
					}
				}

				assert(count<=gen.size());
				subsumed= count==gen.size();

			}
		}


		if(subsumed){
			//all literals of gen are in c, and c is bigger than gen, so gen subsumes c
			 //solvers[frame]->ca[lr.cref()].setSubsumed(true);//mark this as subsumed so it will be removed from the solver later
			solvers[frame]->removeClause(lr.cref());
		}else{
			interpolants[frame][h++]=interpolants[frame][j];
		}
	}
	interpolants[frame].shrink(j-h);
}


/**
 * Updates the heuristic weights in the priority vector, as in PDR
 */
void updateLatchCounts(){
	if(!opt_sort_when_generalizing)
		return;
	priority.clear();
	priority.growTo(in_latches.last()+1);
	//count the occurences of each latch in each clause in the interpolant
	for(int i = 1;i<solvers.size();i++){
		Solver * S = solvers[i];
		//only look at interpolants that have not been passed up from this solver to a later solver. (this will keep us from counting interpolants multiple times)
		for(int j = 0;j<interpolants[i].size();j++){
			LRef lr = interpolants[i][j];
			if(lr.isLit()){
			//	priority[var(lr.lit())]++; //don't bother
			}else{
				Clause & c = S->ca[lr.cref()];
				for(int k = 0;k<c.size();k++){
					priority[var(c[k])]++;
				}
			}
		}
	}
}



void printInterpolant(Solver * S, bool only_unique=true){

	if(opt_print_interpolants){
	vec<Lit> c;
	printf("F: %d ",S->frame);
	if(only_unique){
		for(int i = 0;i<interpolants[S->frame].size();i++){
			c.clear();
			interpolants[S->frame][i].copyTo(c,S->ca);

			printf("(");
			for(int j = 0;j<c.size();j++){
				Lit l = c[j];
				printf("%d ", dimacs(l));
			}

			printf(") ");

		}
		printf("\n");
	}else{
		for(int i = 0;i<S->interpolant.size();i++){
			c.clear();
			S->interpolant[i].copyTo(c,S->ca);

			printf("(");
			for(int j = 0;j<c.size();j++){
				Lit l = c[j];
				printf("%d ", dimacs(l));
			}

			printf(") ");

		}
		printf("\n");
	}


	}
}

void checkInvariant(int frame){
	if (opt_verify){
		if(opt_verb>0){
			printf("Double checking invariant...");
			fflush(stdout);
		}
		vec<Lit> c;
		Solver * checker=new Solver();
		prepare(*checker,mgr,in_latches);
		unroll(*checker,mgr, in_latches, out_latches);
		for(int i = frame;i<interpolants.size();i++){
			for(int j = 0;j<interpolants[i].size();j++){
				c.clear();
				interpolants[i][j].copyTo(c,solvers[i]->ca);
				checker->addClause(c);
			}
		}

		for(int i = frame;i<interpolants.size();i++){
			for(int j = 0;j<interpolants[i].size();j++){
				c.clear();
				interpolants[i][j].copyTo(c,solvers[i]->ca);
				for(int k = 0;k<c.size();k++){
					Lit l = c[k];
					l=~mkLit(var(l)+offset,sign(l));
					c[k]=l;
				}
				if(checker->solve(c)){
					fprintf(stderr,"ERROR: Invariant isn't inductive!\n");
					fflush(stderr);
					exit(3);
				}
			}
		}

		for(int i = 0;i<in_latches.size();i++){
			Lit l = mkLit(in_latches[i],true);
			if(!checker->solve(l)){
				fprintf(stderr,"ERROR: Invariant doesn't hold in base case!\n");
				fflush(stderr);
				exit(3);
			}
		}

		if( checker->solve(negatedProperty)){
				fprintf(stderr,"ERROR: Invariant doesn't prove the property!\n");
				fflush(stderr);
				exit(3);
			}
		if( !checker->solve(~negatedProperty)){
			fprintf(stderr,"ERROR: Invariant excludes the property!\n");
			fflush(stderr);
			exit(3);
		}
		if(opt_verb>0){
			printf("Invariant Verified.\n");
		}
		delete(checker);
	}
}


struct LessThanLRef{
	Solver* _S;
	bool operator () (const LRef & x, const LRef & y){
		if(y.isLit()) //this check _MUST_ come first!
			return false;
		if(x.isLit())
			return true;

		Clause & cx = _S->ca[x.cref()];
		Clause & cy = _S->ca[y.cref()];
		return cx.size()<cy.size();
	}

	LessThanLRef(Solver * S):_S(S){	}
};

void sort_by_size(vec<LRef> & clauses, int frame){
	sort(clauses,LessThanLRef(solvers[frame]));
}

/**
 * Apply clause propagation. This works very similarly to ABC's PDR implementations, except for its (optional) use of 'fast clause propagation'
 */
bool propagateClauses(){

	int total_conflicts=0;
	int explowist = solvers.size();
	for(int i = 0;i<solvers.size();i++){
		total_conflicts+= solvers[i]->conflicts;
		if(i>1 && solvers[i]->interpolant_increase && explowist>i){
			explowist=i;
		}
	}

	//Forcing occasional full clause propagation (when fast clause propagation is enabled) was used for experiments testing whether fast clause propagation might be leading to loss of convergence.
	bool force_prop = opt_force_full_propagation_interval>=0 && total_conflicts>= next_prop_interval;
	if(force_prop){
		next_prop_interval= total_conflicts+ opt_force_full_propagation_interval;
		total_force_prop_cout++;
		std::cout<<"Forced propagation from frame 1 instead of frame " <<  explowist << " because of " << total_conflicts << " conflicts)\n";
	}

	static vec<Lit> c;
	static vec<Lit> clause;
	int lowest=-1;


	static bool first_round=true;
	int from = 1;
	if(opt_prop_zero==2){
		from=0;
	}else if(opt_prop_zero==1 && first_round){
		from=0;
		first_round=false;
	}
	//we can probably safely start at t=1 after the first run through; and the benefit to ever starting at frame 0 is unclear... it just means we force the checker to attempt to check for any constant latches.
	for(int frame = from;frame<=depth()-1;frame++){
		if(interpolants[frame].size()==0){
			checkInvariant(frame);
			return true;//invariant found
		}

		//This is our 'fast clause propapgation' check, from the paper.
		//NOTE: The second-to-last time frame will always have had its interpolant increased since the last time clause propagation was applied (becuase at that time, it was the last frame, and it must have either had clauses pushed to it at that time, or been strengthened in the cube blocking procedure)
		if(opt_fast_clause_propagation && ! force_prop && !solvers[frame]->interpolant_increase)
			continue;

		if(lowest<0){
			lowest=frame;
		}

		solvers[frame]->interpolant_increase=0;
		assert(interpolants.size()>frame);
		vec<LRef> & interpolant = interpolants[frame];
		sort_by_size(interpolant,frame);//sort the interpolant by size (so we can ensure that a clayse cannot subsume an earlier one, ignore equal sized clauses which we assume to be unique )

		int j,i=0;
		for(j = 0;j<interpolant.size();j++){
			//first remove any subsumed clauses from this time frame
			interpolant[j].copyTo(c,solvers[frame]->ca);

			if(opt_remove_subsumed_during_clause_prop)
				removeSubsumed(c,frame,j+1);

			for (int k = 0;k<c.size();k++){
				c[k]=~mkLit(var(c[k])+offset,sign(c[k]));
			}

			if(!solvers[frame]->solveStrictlyLocal(c)){
				clause.clear();
				for(int i = 0;i<solvers[frame]->conflict.size();i++){
					clause.push(mkLit(var(solvers[frame]->conflict[i])-offset,sign(solvers[frame]->conflict[i])));
				}


				//PDR does this, but I'm not clear on why this extra subsumption check is necessary (as it should happen anyhow at the next iteration)
				if(opt_check_subsume_eager)
					removeSubsumed(clause,frame+1);

				solvers[frame]->addClause(clause);

				//push this clause forward to the next time frame
				LRef c = solvers[frame+1]->buildInterpolantClause(clause);
				interpolants[frame+1].push(c);
			}else{
				//keep this clause in the current time frame
				interpolant[i++]=interpolant[j];
			}

		}
		interpolant.shrink(j-i);

		if(interpolant.size()==0){
			checkInvariant(frame);
			return true;//invariant found
		}
	}

	//An optional, final round of subsumption
	if(opt_remove_subsumed_last){
		vec<LRef> & interpolant = interpolants[depth()];
		sort_by_size(interpolant,depth());
		//check each entry in the last time frame for subsumption

		for(int i = 0;i<interpolant.size();i++){
			LRef lr = interpolant[i];
			lr.copyTo(c,solvers[depth()]->ca);
			removeSubsumed(c,depth(),i+1);
		}
	}

	if(opt_print_lowest_frame){
		printf("Lowest strengthened frame is %d of %d\n", lowest,depth());
	}
	return false;
}



void printCube(const TCube & cube){
	if(opt_print_cubes){
		printf("F%d (", cube.frame);

		static vec<Lit> c;
		c.clear();
		cube.assignment->copyTo(c);
		sort(c);

		for(int i = 0;i<c.size();i++){
			printf("%d, ", dimacs(c[i]));
		}
		printf(")\n");
	}
}


/**
 * Check if a clause is SYNTACTICALLY blocked, as in ABC's PDR implementation.
 */
bool isBlocked(int frame, vec<Lit> & assignment, int & blocked_at){
	blocked_at=frame;

	if(!opt_check_blocked || frame>depth())//need to change this to > if we allow frame_infinity
		return false;
	if(frame==0)
		return true;
	static vec<Lit> check;
	static vec<Lit> cube;
	check.clear();
	for(int i = 0;i<assignment.size();i++){
		assert(!solvers[0]->super_interface(assignment[i]));
		check.push(~assignment[i]);
	}
		assert(seen_subsume.size()>0);
		for(int i = 0;i<seen_subsume.size();i++)
			seen_subsume[i]=false;


		uint32_t abstraction=0;
		for(int i = 0;i<check.size();i++){
			Lit l = check[i];
			 seen_subsume[toInt(l)]=true;
			 abstraction |= 1 << (var(l) & 31);

			if(solvers[frame]->value(l)==l_True && solvers[frame]->level(var(l))==0){
				return true;
			}
		}

		assert(abstraction == calcAbstraction(check));


		for(int f = interpolants.size()-1;f>=frame;f--){
			vec<LRef> & interpolant = interpolants[f];
			for(int i = 0;i<interpolant.size();i++){
				LRef lr = interpolant[i];
				if(lr.isLit()){
					continue;//lits are already checked for blocking above
				}
				CRef cr = lr.cref();
				Clause & c = solvers[f]->ca[cr];

				if (!c.isSubsumed() && c.size()<=check.size() && (c.abstraction() & ~abstraction) == 0){
					bool subsumes=true;
					for(int j = 0;j<c.size();j++){
						Lit l = c[j];
						if(!seen_subsume[toInt(l)]){
							subsumes=false;
							break;
						}
					}
					if(subsumes){
						blocked_at=f;
						return true;
					}
				}
			}
		}
		return false;
}


/**
 * Add a new clause to the frame constraints, generalizing it in the process.
 */
void addNewClause(int frame, vec<Lit> & cube, bool already_in_solver){
	static vec<Lit> assign;
	static vec<Lit> discard;
	assert(cube.size());
	bool generalized = false;
	if(opt_generalize_rounds>0){
		for(int i = 0;i<opt_generalize_rounds;i++){
			generalized=generalizer->generalize(cube, frame-1);
			if(!generalized)
				break;
		}
		 // set priority flops
		if(opt_sort_when_generalizing== 1 || opt_sort_when_generalizing==2){
			for (int i = 0; i < cube.size(); i++ )
			{
				priority[var(cube[i])]++;
			}
		}
	}

	//Immediately try to propagate the clause forward as far as possible (PDR does this)

	int max = frame;
	if(opt_early_clause_prop){

		int f= frame;
		for(;f<solvers.size()-1;f++){
			assign.clear();
			solvers[f]->cancelUntil(0);
			solvers[f]->addClause(cube); //this effectively add's not s to constrain the inputs - but we can safely do so permanently, rather than using an activation clause
			for (int k = 0;k<cube.size();k++){
				assign.push( ~mkLit(var(cube[k])+offset,sign(cube[k])));
				}
			//check if blocked is also stopped by later time frame
			//this should really temporarily add ~assign to the solver.
			if(solvers[f]->solveStrictlyLocal(assign)){
				max=f;
				break;
			}else{
				cube.clear();
				for(int i = 0;i<solvers[frame]->conflict.size();i++){
					cube.push(mkLit(var(solvers[frame]->conflict[i])-offset,sign(solvers[frame]->conflict[i])));
				}
				max = f+1;
			}
		}
	}
	if(max>depth())
		max = depth();


	LRef lr = solvers[max]->buildInterpolantClause(cube);
	interpolants[max].push(lr);
	//if and only if the cube was succesfully generalized, and we added notS to the previous time frames during the generalization, we _might_ need to add it to previous time frames
	if(opt_push_clauses_down){
		for(int i = max-1;i>0;i--){
			solvers[i]->cancelUntil(0);
			solvers[i]->addClause(cube);
		}
	}
}

/**
 * Check for new clauses that have been added in solvers (from time frame 1 to time frame 'to').
 */
void collectNewClauses(int to){
	static vec<Lit> gen;
	int lowest =to;
	while(solvers[--lowest]->interpolant.size());
	assert(lowest>=0);
	lowest++;
	for(int i = 1 ;i<to;i++){
		assert(solvers[i-1]->interpolant.size()==0);
		while(solvers[i]->interpolant.size()){
			LRef lr = solvers[i]->interpolant.last();
			solvers[i]->interpolant.pop();
			if(!lr.isLit()){
				if(opt_generalize_all){
					lr.copyTo(gen,solvers[i]->ca);
					addNewClause(i,gen, true);
				}else{
					interpolants[i].push(lr);
				}
			}else{
				interpolants[i].push(lr);
			}
		}
	}
}

/**
 * Main cube blocking procedure, using either the naive or the optimized sat modulo sat solver.
 */
bool modularBlockCube(vec<Lit> & assumptions){
		static vec<Lit> assign;
		static vec<Lit> cube;
		static vec<Lit> discard;
		static vec<Lit> gen;


		do{
			//Use a modular SAT solver to completely block the negated safety property at the last time frame.
			//This will have two side effects.
			//1) The SAT solver for each time frame will learn a set of clauses to strengthen the constraints on its inputs (stored in the interpolant vector), and
			//2) Any satisfying assignments to the input latches (ie, cubes) found by the solvers will be stored in Q.
			if(!opt_use_naive_modular_sat_solver){
				//Use the optimized, SAT modulo SAT solver
				if(solvers[depth()]->solve(assumptions, cube,&Q) ){
						return false;
					}
			}else{
				//Use the unoptimized, naive modular SAT solver
				if(naiveSolver->solve(depth(),assumptions,cube,&Q)){
					return false;
				}
			}
			//Collect (and inductively generalize) any new clauses learned by the SAT solver for any time frame in the modular SAT solver above, adding them to the interpolants
			collectNewClauses(depth());

			//Now, block all the cubes that have been found, starting with the earliest time frames
			while(opt_use_queue && Q.size() &&  Q.peekMin().frame <=depth()){

				TCube s = Q.removeMin();
				if(s.frame==0){
					return false;
				}

				if(isReset(*s.assignment))
					return false;//is this actually reachable? I'm not sure.

				int blocked_at = depth();
				if(!isBlocked(s.frame,*s.assignment, blocked_at))
				{
					//Note that we omit the extra 'semantic' check from the PDR paper.
					//This should probably be added back in.

					assert(!isReset(*s.assignment));//because if it was the reset state, we'd have already found a counter example
					assign.clear();
					for(int i = 0;i< s.assignment->size();i++){
						Lit in = (*(s.assignment))[i];
						assign.push(mkLit(var(in)+offset,sign(in))); //solvers[0]->fromSuper(in));
					}

					if(!opt_use_naive_modular_sat_solver && solvers[s.frame-1]->solve(assign,cube,&Q)){
						//valid counter example found
						return false;
					}else if (opt_use_naive_modular_sat_solver && naiveSolver->solve(s.frame-1,assign,cube,&Q)){
						//valid counter example found
						return false;
					}else{
						assert(solvers[s.frame-1]->okay());

						//assignment s.cube was blocked by the interpolant at s.frame
						//cube is already generalized by the solver
						for(int i = 0;i< cube.size();i++){
							Lit in =cube[i];
							cube[i]= mkLit(var(in)-offset,sign(in));
						}

						//ok, step through the new interpolants in each solver that were added in the current round and try to generalize each one
						collectNewClauses(s.frame);
						//In addition to collecting clauses found by the modular SAT solver internally, also add this outer conflict clause.
						addNewClause(s.frame, cube,false);

						//optionally add the proof obligation to the subsequent time frame
					    //If opt_allow_queue_next_forward, then if the frame is the last frame, we keep it in the queue for the next call to modularBlockCube
						if(opt_queue_forward && (opt_keep_cubes || s.frame < depth())){
						   s.frame++;
						   Q.insert(s.frame,s);

						}else{
							s.clear();//release vector
						}

					}
				}else{
					assert(blocked_at>=s.frame);
					//Keep syntactically blocked cubes to be tested at future time frames, instead of discarding them
					if( opt_queue_forward  && opt_forward_all && (opt_keep_all_cubes || blocked_at < depth())){
						//blocked_at is a small optimization; instead of just queueing these forward to s.frame+1,
						//we can detect the highest frame that the cube would be blocked at (which must be >= s.frame) cheaply in the isBlocked function, and enqueue the cube after that frame.
						//This avoids repeated isBlocked tests for the same cube.
					   s.frame=blocked_at+1;
					   Q.insert(s.frame,s);
					}else{
						s.clear();//release vector
					}

				}
			}
			collectNewClauses(solvers.size());

	}while(opt_use_queue && Q.size() && ( Q.peekMin().frame <=depth()));


	return true;

}

void listInts(){
	if(opt_list_interpolants){
		for(int i = 0;i<solvers.size();i++)
			std::cout<< " " << interpolants[i].size();
		std::cout<<"\n";
	}
	generalizer->printHistogram();
}


lbool run(int maximum_rounds=-1){
	double last_solve_start = cpuTime();

	offset=0;
	if(mgr->num_outputs!=1){
		std::cerr<<"Error: requires exactly 1 output, but this AIG has " << mgr->num_outputs << ".\n";
		exit(1);
	}

	vec<Var> in_subset_check;
	vec<Var> out_subset_check;
	solvers.clear();
	if(!opt_separate_ca){
		external_ca = new ClauseAllocator();
	}

	Solver *  last = new Solver(opt_separate_ca? NULL : external_ca);
	interpolants.push();
	last->external_interpolant=&interpolants;

	in_latches.clear();
	prepare(*last,mgr,in_latches);

	zero(*last,mgr,in_latches);

	negatedProperty =  unroll(*last,mgr, in_latches, out_latches);
	seen_subsume.growTo(in_latches.size()? ( in_latches.last()*2+3) : 0);

	for(int i = 0;i<last->interpolant.size();i++){
		interpolants[0].push(last->interpolant[i]);
	}
	last->interpolant.clear();

	last->frame=0;
	solvers.push(last);
	generalizer = new Generalize(solvers,mgr,interpolants,priority,in_latches,out_latches);
	generalizer->addTimeFrame();


	vec<Lit> assumptions;
	assumptions.push(negatedProperty);


	//Set up coi. There are two parts to this; a static cone of influence for the property variable (represented as a bitset), and a more complicated set up for efficiently finding the cone of influence of latch assignments in the circuit
	if(opt_coi){
		latch_coi = new ConeOfInfluence(mgr,in_latches);
		latch_coi->setupCOI();

		int from = mgr->outputs[0].lit;
		vec<Var> prop_coi;
		latch_coi->collectCOI(from, prop_coi );
		if(in_latches.size())
			property_coi.growTo(in_latches.last()+1);

		for(int i = 0;i<prop_coi.size();i++){
			Var v = prop_coi[i];
			property_coi.set(v);
		}

	}

	last->negatedProperty=negatedProperty;
	if(opt_coi){

		last->latch_coi=latch_coi;
		property_coi.copyTo(last->property_coi);
	}

#ifndef NDEBUG
		last->dbg_expect_answer=dbg_checkBMC(mgr,1);
#endif

	if(last->solve(assumptions)){
		return l_True;
	}

	last->max_frame=0;


	if(mgr->num_latches==0){
		std::cerr<<"Note: Circuit has no latches.\n";
		return l_False;
	}
	if(!last->okay()){
		std::cerr<<"Note: Problem proven unsat at first unrolling even without asserting property.\n";
		return l_False;
	}

	double solveTime=(cpuTime() - last_solve_start) ;

	assert(last->okay());
	assert(out_latches.size()>0);


	total_iterations=0;

	if(opt_sort_when_generalizing){
		priority.growTo(out_latches.size());
	}
	if(opt_ternary){
		ternary= new Ternary(mgr,priority.size()? &priority:NULL);
	}
	 if(opt_use_naive_modular_sat_solver){
		 naiveSolver = new NaiveSolver(solvers,generalizer,ternary,latch_coi,property_coi, in_latches,out_latches,negatedProperty);
	 }
	while(true){
		assert(dbg_inductiveInterpolant(mgr,solvers,interpolants));

		if(opt_print_depth){
			printf("u%d\n", total_iterations);
			fflush(stdout);
		}
		total_iterations++;
		if(maximum_rounds>=0 && total_iterations>maximum_rounds){
			return l_Undef;
		}

		//This is used for updating the 'priority' vector, which is a heuristic used for generalization and ternary simulation
		updateLatchCounts();


		total_solve_time+=solveTime;
		last_solve_start= cpuTime();

		//unroll a new iteration of the circuit in a new solver:
		if(opt_print_ca){
			printf("New round; ca size is %d\n",  solvers.last()->ca.size());
		}

		Solver * Sk= new Solver(opt_separate_ca? NULL : external_ca);
		interpolants.push();
		Sk->external_interpolant=&interpolants;

		in_latches.clear();
		out_latches.clear();

		prepare(*Sk,mgr,in_latches);

		negatedProperty =  unroll(*Sk,mgr, in_latches, out_latches);

		if(opt_add_negated_reset){
			static vec<Lit> c;
			c.clear();
			for(int i = 0;i<in_latches.size();i++){
				c.push(mkLit(in_latches[i],false));
			}
			c.push(~negatedProperty);
			Sk->addClause(c);//At least one input latch must be different from the reset state in any full solution
			//but this is true even if the negatedProperty is only set at a later stage...
		}

		Sk->negatedProperty=negatedProperty;
		if(opt_coi){
			property_coi.copyTo(Sk->property_coi);
			Sk->latch_coi=latch_coi;
		}
		if(opt_ternary){
			Sk->ternary=ternary;
		}
		solvers.push(Sk);
		generalizer->addTimeFrame();

		for(int i = 0;i<solvers.size();i++){
			solvers[i]->frame=i;
			solvers[i]->max_frame=depth();
		}

		assert(negatedProperty!=lit_Undef);
		assumptions.clear();
		assumptions.push(negatedProperty);

#ifndef NDEBUG
		vec<Lit> ignore;
		bool bmc_expect = dbg_checkBMC(mgr,total_iterations,in_latches,ignore);

		Sk->dbg_expect_answer=bmc_expect;
#endif

		offset = out_latches[0]-in_latches[0];

#ifndef NDEBUG
		Sk->dbg_property = negatedProperty;
#endif

		if(!opt_use_naive_modular_sat_solver)
			Sk->attachSubSolver(last,offset, out_latches[0], out_latches.last()+1);

		listInts();


		if( opt_prop_first){
			if(propagateClauses()){
				for(int i = 0;i<solvers.size();i++)
					printInterpolant(solvers[i],true);

				listInts();

				return l_False;
			}
			listInts();
		}

		if(opt_print_interpolants){
			for(int i = 0;i<solvers.size();i++){
				printInterpolant(solvers[i],true);
			}
		}

		//updateLatchCounts();
		bool result;

		result = !modularBlockCube(assumptions);

		solveTime = (cpuTime() - last_solve_start);

		if(!opt_prop_first){
			if(propagateClauses()){
				for(int i = 0;i<solvers.size();i++)
					printInterpolant(solvers[i],true);

				listInts();

				return l_False;
			}
			listInts();
		}

#ifndef NDEBUG
		if(bmc_expect)
				assert(result);
#endif
		if(result){
			if(opt_verb>0)
				std::cout<<"\nSolveLastTime: " << solveTime << "\n";
			return l_True;
		}else if (!Sk->okay()){
			std::cerr<<"Warning: main solver is unsat without assumptions. This usually indicates a bug!\n";
			exit(3);
			return l_False;
		}else{
			assert(dbg_constraints(*Sk, negatedProperty));

			if(opt_coi || opt_ternary){
				total_coi_blocked=0;
				total_assign_lits=0;
				total_ternary_blocked=0;
				for(int i = 0;i<solvers.size();i++){
					total_coi_blocked+=solvers[i]->num_coi_blocked;
					total_ternary_blocked+=solvers[i]->num_ternary_blocked;
					total_assign_lits+=solvers[i]->total_assign_lits;
				}
			}
			last=Sk;
		}
	}
	//Unreachable
	assert(false);
	return l_Undef;
}
};


#endif
