/*
 * Provides Cone of Influence detection
 * Cone.cpp
 *
 *  Created on: 2013-04-30
 *      Author: sam
 */

#include <iostream>
#include "Cone.h"
#include "Config.h"

#include "utils/System.h"




static int lit2lit (Lit l, int startVar, int last_in_latch)
{
  int v = var(l);
  int s = sign(l) ? -1 : 1;



  if (v){
	 if(v<=last_in_latch)
		 return v;
	 else
		 return (v-startVar)* s;
  }else
    return s;//ground literal
}

void ConeOfInfluence::markCone( int l){
	//traverse the aig from each output, marking nodes as either relevant or irrelevant.
	int v = aiger_lit2var(l);

	if(!seen[v]){
		seen[v]=true;
		if(aiger_and* a = aiger_is_and(mgr,aiger_strip(l))){
			int l0= a->rhs0;
			markCone(l0);

			int l1= a->rhs1;
			markCone(l1);
		}else if (aiger_is_input(mgr,aiger_strip(l))){
			//done
		}else if(aiger_symbol * s = aiger_is_latch(mgr,aiger_strip(l))){
			//done
		}

	}
}

void ConeOfInfluence::collectMarkedLatches(vec<Var> & coi){
	for(int i = 0;i<mgr->num_latches;i++){
			aiger_symbol& s = mgr->latches[i];
			int l= s.lit;
			  int res = aiger_lit2var (l);
			  if(seen[res]){
			  assert(i<in_latches.size());
			  Var v = in_latches[i];
			  coi.push(v);
		  }
	}
}

void ConeOfInfluence::collectCOI( int l,vec<Var> & coi){

	seen.clear();

	coi.clear();
	seen.growTo(mgr->maxvar+1);
	markCone(l);
	collectMarkedLatches(coi);
}
void ConeOfInfluence::collectCOI( const vec<Var>& from,vec<Var> & coi){

	seen.clear();

	coi.clear();
	seen.growTo(mgr->maxvar+1);
	int startVar = in_latches.last()+1;//this is fragile
	for(int i = 0;i<from.size();i++){
		int l = lit2lit(mkLit(from[i],false),startVar, in_latches.last());
		markCone(l);
	}
	collectMarkedLatches(coi);
}

/**
 * Initialize the cone of influence
 */
void ConeOfInfluence::setupCOI(){
	if(opt_coi && in_latches.size()){
		if(opt_verb>0)
			std::cout<<"Computing COI for each latch...";
		vec<Var> coi;
		double coiTime=(cpuTime() ) ;

		latch_coi.growTo(in_latches.last()+1);
		for(int i = 0;i<in_latches.size();i++){

			Var vfor = in_latches[i];
			int from = mgr->latches[i].next;
			collectCOI( from, coi );
			latch_coi[vfor].growTo(in_latches.last()+1);


			for(int j = 0;j<coi.size();j++){
				Var v = coi[j];
				latch_coi[vfor].set(v);
			}
			if(opt_print_coi){
				printf("Latch %d COI: ",vfor);
				for(int i = 0;i<in_latches.size();i++){
					if (latch_coi[vfor][in_latches[i]])
						printf("1");
					else
						printf("0");
				}
				printf("\n");
			}
		}
		if(opt_verb>0)
			std::cout<<"Done coi (" << (cpuTime() - coiTime) << " seconds)\n";
	}
}
